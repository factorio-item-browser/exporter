module gitlab.com/factorio-item-browser/exporter.git

go 1.23

require (
	github.com/andybalholm/brotli v1.1.1
	github.com/aws/aws-lambda-go v1.47.0
	github.com/aws/aws-sdk-go-v2 v1.32.5
	github.com/aws/aws-sdk-go-v2/config v1.28.5
	github.com/aws/aws-sdk-go-v2/service/eventbridge v1.35.6
	github.com/aws/aws-sdk-go-v2/service/s3 v1.69.0
	github.com/aws/smithy-go v1.22.1
	github.com/google/uuid v1.6.0
	github.com/lmittmann/tint v1.0.5
	github.com/spf13/afero v1.11.0
	github.com/stretchr/testify v1.10.0
	github.com/subosito/gotenv v1.6.0
	gitlab.com/blue-psyduck/soushinki.git v1.1.0
	gitlab.com/factorio-item-browser/export-data.git v1.0.0-rc.1
	gitlab.com/factorio-item-browser/factorio-icon-renderer.git v1.0.4
	gitlab.com/factorio-item-browser/factorio-mod-portal-client.git v1.0.1
	gitlab.com/factorio-item-browser/factorio-translator.git v1.0.0
	golang.org/x/sync v0.9.0
)

require (
	github.com/anthonynsimon/bild v0.14.0 // indirect
	github.com/aws/aws-sdk-go-v2/aws/protocol/eventstream v1.6.7 // indirect
	github.com/aws/aws-sdk-go-v2/credentials v1.17.46 // indirect
	github.com/aws/aws-sdk-go-v2/feature/ec2/imds v1.16.20 // indirect
	github.com/aws/aws-sdk-go-v2/internal/configsources v1.3.24 // indirect
	github.com/aws/aws-sdk-go-v2/internal/endpoints/v2 v2.6.24 // indirect
	github.com/aws/aws-sdk-go-v2/internal/ini v1.8.1 // indirect
	github.com/aws/aws-sdk-go-v2/internal/v4a v1.3.24 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/accept-encoding v1.12.1 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/checksum v1.4.5 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/presigned-url v1.12.5 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/s3shared v1.18.5 // indirect
	github.com/aws/aws-sdk-go-v2/service/sso v1.24.6 // indirect
	github.com/aws/aws-sdk-go-v2/service/ssooidc v1.28.5 // indirect
	github.com/aws/aws-sdk-go-v2/service/sts v1.33.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.5.2 // indirect
	golang.org/x/exp v0.0.0-20241108190413-2d47ceb2692f // indirect
	golang.org/x/text v0.20.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
