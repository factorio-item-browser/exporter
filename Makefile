#!make

BUILD_DIR  := build
BUILD_TIME := $(shell date -R)
GO_VERSION := 1.23
SRC_DIRS   := $(shell find . -maxdepth 1 -type d \( -name pkg -o -name internal \) -exec basename {} \; | xargs -I{} echo "./{}/..." | tr '\n' ' ')
SRC_DIRS_TEST := $$(go list $(SRC_DIRS) | grep -v mocks)

.PHONY: help
help: ## Show this help.
	@awk 'BEGIN {FS = ":.*##"; printf "Usage:\033[36m\033[0m\n"} /^[$$()% a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

all: clean vendor tools fix test lint ## Run all steps for testing the project.

.PHONY: tools
tools: ## Install go tools.
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
	go install github.com/mcubik/goverreport@latest
	go install github.com/vektra/mockery/v2@latest

.PHONY: generate
generate: ## Generates additional files.
	mockery --config .mockery.yaml

.PHONY: vendor
vendor: ## Build or update the vendor directory.
	go mod tidy -v
	go mod vendor -v

.PHONY: vendor-upgrade
vendor-upgrade: ## Upgrades all dependencies in the vendor directory.
	go get -t -u ./...

.PHONY: clean
clean: ## Clean the project.
	go clean -v
	rm -rf $(BUILD_DIR) vendor

.PHONY: fix
fix: ## Fix the coding style of the project.
	go fmt $(SRC_DIRS)

.PHONY: test
test: ## Test the project.
	go test -short -race -coverprofile coverage.out $(SRC_DIRS)
	goverreport -coverprofile=coverage.out
	@rm coverage.out

.PHONY: lint
lint: ## Run the linter on the project.
	golangci-lint run --timeout=5m --go=$(GO_VERSION)

.PHONY: build
build: cmd/* ## Builds the project.
	@for DIRECTORY in $^; do \
		NAME=`basename $$DIRECTORY`; \
		printf "\033[1;32mBuilding \033[1;93m$$NAME\033[0m\n"; \
		CGO_ENABLED=0 go build -ldflags "-s -w" -mod vendor -v -o $(BUILD_DIR)/bin/$$NAME $$DIRECTORY/main.go; \
		chmod +x $(BUILD_DIR)/bin/$$NAME; \
		printf "\033[1;32mDone building \033[1;93m$$NAME\033[0m\n"; \
	done; \
	printf "\033[1;32mCopying directory \033[1;93mlua\033[0m\n"; \
	cp -r lua $(BUILD_DIR)/; \
	printf "\033[1;32mCopying file \033[1;93m.env\033[0m\n"; \
	cp .env $(BUILD_DIR)/.env; \

.PHONY: build-image
build-image: ## Builds the docker image.
	@make build
	docker build --build-arg EXPORTER_FACTORIO_IMAGE=registry.gitlab.com/factorio-item-browser/exporter-factorio-image:latest -t fib-exporter .
