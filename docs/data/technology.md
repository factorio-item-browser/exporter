# Data: Technology

A `technology` is what eats away your science packs to unlock new recipes and abilities.

## Fields

| Name                 | Type                    | Default*      | Example                                 | Description                                                             |
|----------------------|-------------------------|---------------|-----------------------------------------|-------------------------------------------------------------------------|
| kind                 | "technology.v1"         | _(mandatory)_ | "technology.v1"                         | The kind of the data.                                                   |
| name                 | string                  | _(mandatory)_ | "spidertron"                            | The internal name of the technology, used in Factorio.                  | 
| labels               | object\<string, string> | {}            | {"en": "Spidertron"}                    | The name of the technology, translated to all available locales.        |
| descriptions         | object\<string, string> | {}            | {"en": "A versatile vehicle [...]"}     | The description of the technology, translated to all available locales. |
| icon                 | string                  | ""            | c542dbec-292b-8547-becf-f6e1583ef950"   | The id of the icon, used in the stylesheet.                             |
| prerequisites        | array\<string>          | []            | ["military-4", "exoskeleton-equipment"] | The names of the prerequisite technologies required to unlock this one. |
| researchIngredients  | array\<Ingredient>      | []            |                                         | The ingredients needed for one research cycle of the technology.        |
| researchCount        | integer                 | 0             | 250                                     | The number of research cycles needed for the technology.                |
| researchCountFormula | string                  | ""            | ""                                      | The formula for the research count for multi-level technologies.        |
| researchTime         | float                   | 0             | 30.                                     | The time required for one research cycle.                               |
| unlockedRecipes      | array\<string>          | []            | ["spidertron", "spidertron-remote"]     | The names of the recipes unlocked when researching this technology.     |
| level                | integer                 | 1             | 1                                       | The level of the technology.                                            |
| maxLevel             | integer                 | 1             | 1                                       | The maximal level of the technology.                                    |

_*) If the field is missing in the data, the default value must be assumed. Mandatory fields are always present._

### Ingredient:

| Name   | Type                        | Default*      | Example                   | Description                                           |
|--------|-----------------------------|---------------|---------------------------|-------------------------------------------------------|
| type   | "item", "fluid", "resource" | _(mandatory)_ | "item"                    | The type of the ingredient.                           |
| name   | string                      | _(mandatory)_ | "automation-science-pack" | The name of the ingredient.                           |
| amount | float                       | 1.            | 1.                        | The amount of the ingredient required for the recipe. |

## Examples

<details>
    <summary><b>Example:</b> spidertron</summary>

```json
{
  "kind": "technology.v1",
  "name": "spidertron",
  "labels": {
    "af": "Spidertron",
    "ar": "ةيتوبكنعلا ةبكرملا",
    "be": "Спайдартрон",
    "bg": "Паякотрон",
    "ca": "Aranyatron",
    "cs": "Spidertron",
    "da": "Eddertron",
    "de": "Spidertron",
    "el": "Spidertron",
    "en": "Spidertron",
    "eo": "Araneilo",
    "es-ES": "Mecanoaraña",
    "et": "Robo-ämblik",
    "eu": "Armiarmatron",
    "fa": "توبکابر",
    "fi": "Lukkibotti",
    "fr": "Spidertron",
    "ga-IE": "Spidertron",
    "he": "ןורטשיבכע",
    "hr": "Spidertron",
    "hu": "Spidertron",
    "id": "Spidertron",
    "is": "Vélkönguló",
    "it": "Spidertron",
    "ja": "スパイダートロン",
    "ka": "სპაიდერტრონი",
    "kk": "Өрмекшітрон",
    "ko": "스파이더트론",
    "lt": "Voratronas",
    "lv": "Zirnekļtrons",
    "nl": "Spidertron",
    "no": "Eddertron",
    "pl": "Spidertron",
    "pt-BR": "Spidertron",
    "pt-PT": "Aranhatron",
    "ro": "Păianjetron",
    "ru": "Паукотрон",
    "sk": "Pavúkotron",
    "sl": "Robopajek",
    "sr": "Паук-робот",
    "sv-SE": "Spidertron",
    "th": "สไปเดอร์ตรอน",
    "tr": "Spidertron",
    "uk": "Павукотрон",
    "vi": "Nhện máy",
    "zh-CN": "蜘蛛机甲",
    "zh-TW": "蜘蛛機甲"
  },
  "descriptions": {
    "af": "'n Veelsydige voertuig met die vermoë om oor rowwe terrein te vaar. Dit is toegerus met vinnig-vurende vuurpyllanseerders wat per hand of automaties beheer kan word. Kan per hand bestuur word of deur om die Spidertron afstandbeheer te gebruik.",
    "ar": ".توبكنعلا قيرط نع مكحتلا زاهج مادختساب وأ ًايودي دوقت نأ نكمي.يئاقلتلا وأ يوديلا فادهتسالا اهيف مكحتي قالطإلا ةعيرس خيراوص تافذاقب ةزهجم يهو.ةرعولا سيراضتلا زايتجا ىلع ةرداق ةعونتم ةبكرم",
    "be": "Універсальны транспартны сродак, здольны перамяшчаць перасечаную мясцовасць. Абсталяваны хуткастрэльнай ракетнай зброяй з ручным або аўтаматычным прыцэльваннем. Можа кіравацца ўручную альбо з дапамогай пульта кіравання.",
    "bg": "Универсално превозно средство, способно да минава през неравен терен. Оборудвано е с бързострелящи ракетни установки, управлявани чрез ръчно или автоматично насочване. Може да се управлява ръчно или с помощта на дистанционното на спайдъртрон.",
    "ca": "Un vehicle versàtil capaç de travessar terrenys complicats. Està equipat amb llançadors de míssils de cadència ràpida que apunten als objectius de manera automàtica o manual. Es pot conduir des del seu interior o amb amb un comandament a distància.",
    "cs": "Univerzální vozidlo schopné pohybovat se těžkým terénem. Je vybaven raketomety s rychlým odpalováním, které mohou být zamířeny manuálně nebo pomocí automatického zaměřování. Může být ovládán ručně nebo pomocí dálkového ovládání.",
    "da": "Et alsidigt køretøj, der kan bevæge sig over ujævnt terræn. Det er udstyret med hurtigtskydende raketkastere, der kan affyres automatisk eller manuelt. Kan styres manuelt eller ved hjælp af Eddertron fjernstyringen.",
    "de": "Ein vielseitig einsetzbares Fahrzeug, das unebenes Gelände durchqueren kann. Es ist mit schnellen Raketenwerfern ausgerüstet, die manuell oder automatisch abgefeuert werden können. Kann manuell gefahren oder mit der Spidertron-Fernbedienung gesteuert werden.",
    "el": "Ένα ευέλικτο όχημα ικανό να διασχίζει τραχύ έδαφος. Είναι εξοπλισμένο με μπαζούκας γρήγορης βολής με χειροκίνητη ή αυτόματη στόχευση. Μπορεί να οδηγηθεί χειροκίνητα ή χρησιμοποιώντας τον τηλεχειρισμό spidertron.",
    "en": "A versatile vehicle capable of traversing rough terrain. It's equipped with fast-firing rocket launchers controlled by manual or automatic targeting. Can be driven manually or using the spidertron remote.",
    "eo": "Lerta veturilo kapabla trapasi malglatan terenon. Ĝi estas ekipita per rapidpafaj raketlanĉiloj kontrolitaj per mana aŭ aŭtomata celigo. Oni stiras ĝin mane aŭ per teleregilo de araneilo.",
    "es-ES": "Un vehículo versátil capaz de atravesar terrenos difíciles. Está equipado con lanzacohetes de disparo rápido y controlados de forma manual o automática. Puede conducirse manualmente o utilizando el control remoto de la Mecanoaraña.",
    "et": "Mitmekülgne sõiduk, mis suudab läbida ebatasasel maastikul. See on varustatud kiirelt tulistavate raketiheitjatega, mida juhitakse käsitsi või automaatse sihtimisega. Saab juhtida käsitsi või kasutades spidertroni kaugjuhtimispulti.",
    "eu": "Lur zailak zeharkatzeko gai den ibilgailu trebea. Tiro azkarreko kohete-jaurtigailuak ditu, eskuz edo automatikoki kontrolatzen direnak. Eskuz edo armiarmatroiaren agintea erabiliz eragin daiteke.",
    "fa": ".درک یگدننار توبکابر تومیر زا هدافتسا اب ای اصخش ناوتیم ار اهنآ.دنوشیم لرتنک راکدوخ یریگ فده اب ای یتسد تروص هب هک عیرس یاه زادنا کشوم هب زهجم.روبعلا بعص یاه نیمز زا روبع هب رداق هراک همه هیلقن هلیسو کی",
    "fi": "Monipuolinen ajoneuvo, joka kykenee kulkemaan epätasaisessa maastossa. Varustettu nopeilla raketinheittimillä, jotka voi kohdistaa käsin tai automaattisesti. Voidaan ohjata käsin tai lukkibotti-kaukosäätimellä.",
    "fr": "Un véhicule polyvalent capable de traverser un terrain accidenté. Il est équipé de lance-roquettes à tir rapide contrôlés par ciblage manuel ou automatique. Peut être piloté manuellement ou à l'aide de la télécommande spidertron.",
    "he": ".ידועיי טלש תועצמאב וא תינדי וב גוהנל ןתינ.תיטמוטוא וא תינדי הטילשל םינתינה ירי יריהמ םיליט ירגשמב דייוצמ.םישק חטש יאווט חולצל לגוסמה ישומיש בר בכר ילכ",
    "hr": "Višenamjensko vozilo koje je sposobno prijelaza gruboga terena. Opremljeno je sa rafalnim raketnim bacaćima koje se mogu ručno kontrolirati ili na automatsko ciljanje. Može se ručno voziti ili uz pomoć daljinskog za robopauka.",
    "hu": "Sokoldalú jármű, amely képes az egyenetlen terepen való közlekedésre. Gyorstüzelő rakétavetőkkel van felszerelve, amelyek manuális és automatikus célzásra is képesek. Manuálisan vezethető és a Spidertron-távirányítóval is irányítható.",
    "id": "Sebuah kendaraan serbaguna yang mampu melintasi medan yang kasar. Kendaraan tersebut dilengkapi dengan peluncur roket berkecepatan tembak tinggi yang dapat dikendalikan secara manual atau melalui penargetan otomatis. Dapat dikemudikan secara manual atau dengan menggunakan remot spidertron.",
    "it": "Un veicolo versatile, capace di attraversare terreni impervi. E' equipaggiato con lanciarazzi a fuoco rapido, controllato da puntamento manuale o automatico. Può essere guidato manualmente oppure utilizzando il telecomando Spidertron.",
    "ja": "起伏の多い地形も踏破することができる汎用性の高い車両。手動または自動で照準される高速発射可能なロケットランチャーが装備されています。乗り込んで操作したり、スパイダートロンリモコンで操作できます。",
    "ka": "მრავალმხრივი მანქანა, რომელსაც შეუძლია უხეში რელიეფის გავლა. იგი აღჭურვილია სწრაფი სროლის სარაკეტო გამშვებით, რომელსაც აკონტროლებს ხელით ან ავტომატური დამიზნებით. შეიძლება მართოთ ხელით ან სპაიდერტრონი დისტანციური მართვის გამოყენებით.",
    "kk": "Тегіс емес жермен жүре алатын әмбебап көлік. Қолдан немесе автоматты түрде нысаналау арқылы басқарылатын, тез ататын зымыран атқыштармен жабдықталған. Қолдан немесе өрмекшітрон пультімен жүргізіледі.",
    "ko": "거친 지형을 횡단할 수 있는 다목적 차량입니다. 수동 또는 자동 목표 조준이 가능한 고속 연사 로켓 발사기가 장착되어 있습니다. 수동 또는 스파이더트론 조종 장치를 사용하여 움직일 수 있습니다.",
    "lt": "Įvairiapusė transporto priemonė galinti keliauti per nelygius paviršius. Turi pritaisytus greitai šaudančius raketsvaidžius valdomus ranka arba automatinio nutaikymo. Gali būti vairuojama paties arba su voratrono pulteliu.",
    "lv": "Daudzpusīgs transporta līdzeklis ar spēju pārvietoties pār nelīdzenu un grūti pārvaramu reljefu. Tas ir ekipēts ar ātri šaujošiem raķešu palaidējiem ar manuālu vai automātisku tēmēšanas iespēju. Var tikt vadīts manuāli vai arī izmantojot zirnekļtrona vadīšanas pulti.",
    "nl": "Een veelzijdig voertuig dat ruwe terrein kan doorkruizen. Het is uitgerust met snel vurende raketwerpers die handmatig of automatisch doelwit kunnen selecteren. Kan handmatig worden bestuurd of met behulp van de spidertron afstandsbediening.",
    "no": "Et allsidig fartøy som kan ferdes over ujevnt terreng. Den er utstyrt med hurtigavfyrende rakettkastere som er styrt ved manuell eller automatisk målsøking. Kan kjøres manuelt eller ved hjelp av en eddertron-fjernkontroll.",
    "pl": "Uniwersalny pojazd zdolny do przemierzania trudnego terenu. Jest wyposażony w szybkostrzelne wyrzutnie rakiet, zdolne do ręcznego lub automatycznego celowania. Może być sterowany ręcznie lub przy użyciu specjalnego kontrolera.",
    "pt-BR": "Veículo versátil capaz de atravessar terrenos desafiadores. Vem equipado com lançadores de foguetes de disparo rápido capazes de atirar manualmente ou automaticamente. Pode ser comandado manualmente ou usando o controle remoto do spidertron.",
    "pt-PT": "Um veículo versátil capaz de atravessar terrenos acidentados. É equipado com lançadores de foguetes de disparo rápido controlados por mira manual ou automática. Pode ser conduzido manualmente ou usando o controle remoto spidertron.",
    "ro": "Un vehicul versatil capabil să traverseze teren accidentat. Este echipat cu lansatoare de rachete cu lansare rapidă controlate prin țintire manuală sau automată. Poate fi condus manual sau folosind telecomanda păianjetron.",
    "ru": "Универсальное транспортное средство, способное передвигаться по пересечённой местности. Оно оснащено скорострельными ракетницами с ручным или автоматическим наведением. Управляется вручную или с помощью пульта управления.",
    "sk": "Viacúčelové vozidlo schopné prechádzať cez rôzny terén. Je vybavené rýchlopalnými raketometmi ovládanými manuálne alebo automatickým zameriavaním. Dá sa ovládať manuálne alebo použitím pavúkotron ovládačom",
    "sr": "Вишенаменско превозно средство које може да прелази преко стрмог и кршног терена. Опремљено је брзо-пуцајућим ракетним бацачима, контролише се ручним или аутоматским циљањем. Може бити вожено ручно или уз помоћ даљинског.",
    "sv-SE": "Ett mångsidigt fordon som kan färdas genom grov terräng. Den är utrustad med ett snabbskjutande raketgevär som siktas manuellt eller automatiskt. Kan köras manuellt eller med hjälp av spidertron-fjärrkontrollen.",
    "th": "ยานพาหนะเอนกประสงค์ที่สามารถข้ามภูมิประเทศที่ขรุขระได้ ติดตั้งเครื่องยิงจรวดที่ยิงเร็วซึ่งควบคุมโดยการกำหนดเป้าหมายแบบแมนนวลหรือแบบอัตโนมัติ สามารถขับเคลื่อนด้วยตนเองหรือใช้รีโมท spidertron",
    "tr": "Engebeli arazi şartlarında hareket edebilen çok yönlü bir araç. Elle veya otomatik hedefleme ile kullanılabilen hızlı ateş eden roketatarlar ile donatılmıştır. Bizzat sürülebilir ya da spidertron kumandası ile kontrol edilebilir.",
    "uk": "Універсальний транспортний засіб, здатний проходити пересічену місцевість. Він оснащений швидкострільними ракетними установками, придатними для ручного або автоматичного націлювання. Можна керувати вручну або за допомогою пульта керування павукотроном.",
    "vi": "Một phương tiện linh hoạt có thể đi qua địa hình hiểm trở. Được trang bị súng tên lửa tốc độ cao có thể được ngắm thủ công hoặc tự động. Có thể lái thủ công hoặc sử dụng điều khiển từ xa.",
    "zh-CN": "一种能够穿越复杂地形的多用途载具，其装备的高射速火箭筒能够手动瞄准或自动攻击，既能够手动驾驶，也可以通过遥控器远程驱动。",
    "zh-TW": "可跨越崎嶇地形的多功能載具。配備快速射擊的火箭發射器，火箭發射器可手動瞄準或自動瞄準。可手動駕駛或透過蜘蛛機甲遙控器來操作。"
  },
  "icon": "c542dbec-292b-8547-becf-f6e1583ef950",
  "prerequisites": [
    "military-4",
    "exoskeleton-equipment",
    "fusion-reactor-equipment",
    "rocketry",
    "rocket-control-unit",
    "effectivity-module-3"
  ],
  "researchIngredients": [
    {
      "type": "item",
      "name": "automation-science-pack"
    },
    {
      "type": "item",
      "name": "logistic-science-pack"
    },
    {
      "type": "item",
      "name": "military-science-pack"
    },
    {
      "type": "item",
      "name": "chemical-science-pack"
    },
    {
      "type": "item",
      "name": "production-science-pack"
    },
    {
      "type": "item",
      "name": "utility-science-pack"
    }
  ],
  "researchCount": 2500,
  "researchTime": 30,
  "unlockedRecipes": [
    "spidertron",
    "spidertron-remote"
  ]
}
```
</details>

<details>
    <summary><b>Example:</b> mining-productivity-4</summary>

```json
{
  "kind": "technology.v1",
  "name": "mining-productivity-4",
  "labels": {
    "af": "Mynbou produktiwiteit",
    "ar": "بيقنتلا ةيجاتنا",
    "be": "Прадукцыйнасць здабычы",
    "bg": "Продуктивност на рудодобива",
    "ca": "Productivitat minera",
    "cs": "Produktivita těžby",
    "da": "Minedriftsproduktivitet",
    "de": "Bergbau-Produktivität",
    "el": "Παραγωγικότητα εξόρυξης",
    "en": "Mining productivity",
    "eo": "Produktiveco de fosado",
    "es-ES": "Productividad minera",
    "et": "Kaevandaja tootlikkus",
    "eu": "Meatzatze produktibitatea",
    "fi": "Louhintatuottoisuus",
    "fr": "Productivité minière",
    "ga-IE": "Táirgiúlacht mianadóireachta",
    "he": "היירכ תונרצי",
    "hr": "Produktivnost rudarenja",
    "hu": "Bányászati termelékenység",
    "id": "Produktifitas penambangan",
    "is": "Námuvinnsluframleiðni",
    "it": "Produttività dell'estrazione",
    "ja": "掘削効率",
    "ka": "მაინინგის პროდუქტიულობა",
    "kk": "Қазу нәтижелілігі",
    "ko": "채취 생산성",
    "lt": "Kasybos produktyvumas",
    "lv": "Rakšanas produktivitāte",
    "nl": "Mijnproductiviteit",
    "no": "Produktivitet gruvedrift",
    "pl": "Wydajność wydobycia",
    "pt-BR": "Produtividade de mineração",
    "pt-PT": "Produtividade de extracção",
    "ro": "Productivitate de minerire",
    "ru": "Продуктивность добычи",
    "sk": "Produktivita ťažby",
    "sl": "Rudarska produktivnost",
    "sr": "Продуктивност рударења",
    "sv-SE": "Gruvdriftsproduktivitet",
    "th": "กำลังการผลิตของเหมืองแร่",
    "tr": "Sondaj üretkenliği",
    "uk": "Продуктивність видобутку",
    "vi": "Năng suất khai thác mỏ",
    "zh-CN": "采矿产能",
    "zh-TW": "開採產能"
  },
  "descriptions": {
    "af": "Verhoogde opbrengs vanaf alle mynbore en pomp donkies.",
    "ar": ".طفنلا تاخضمو بيقنتلا تارافح لك نم ىلعأ ةيجاتنإ",
    "be": "Павялічвае здабычу з бураў і нафтавых вышак.",
    "bg": "Увеличава добива на всички рудни установки и нефтени помпи.",
    "ca": "Augmenta la producció de totes les explotacions mineres i pous de bombeig de petroli.",
    "cs": "Zvyšuje výnos ze všech těžících zařízení a ropných čerpadel.",
    "da": "Forøget udbytte fra alle minebor og oliepumper.",
    "de": "Erhöht den Ertrag von allen Erzförderern und Rohöl-Förderpumpen.",
    "el": "Αυξάνει την απόδοση σε όλα τα τρυπάνια εξόρυξης και τις αντλίες πετρελαίου.",
    "en": "Increased yield from all mining drills and pumpjacks.",
    "eo": "Pliigas produktadon de ĉiuj fosmaŝinoj kaj naftpumpiloj.",
    "es-ES": "Mejora la productividad de todas las perforadoras y bombas extractoras.",
    "et": "Suurendatud saagikus kõikidest kaevanduspuuridest ja naftapumpadest.",
    "fi": "Parantaa kaikkien kaivosporien ja öljypumppujen tuottoisuutta.",
    "fr": "Augmente les rendements de toutes les foreuses électriques et de tous les chevalets de pompage.",
    "ga-IE": "Méadaigh táirgeacht as druilirí mhianadóireachta agus ola-thobair loine.",
    "he": ".חודיקה ילדגמו תונוכמ יגוס לכ לש הקופתה תלדגה",
    "hr": "Povećava prinos iz svih rudarskih bušilica i naftnih crpki.",
    "hu": "Megnöveli minden bányagép és olajkút hozamát.",
    "id": "Meningkatkan hasil dari semua bor penambangan dan pumpjacks.",
    "it": "Aumenta la resa di tutte le trivelle e pompe petrolifere.",
    "ja": "掘削機・油井から追加の資源を得られるようになります。",
    "ka": "გაზრდილი მოსავლიანობა ყველა სამთო წვრთნებიდან და ტუმბოებიდან.",
    "kk": "Барлық бұрғы мен мұнай мұнарасының табысы үлкейеді.",
    "ko": "모든 채광 드릴과 시추기의 생산량을 증가시킵니다.",
    "lt": "Padidėjęs derlius iš visų kasybos treniruočių ir siurblių.",
    "lv": "Paaugstināts ražīgums no visiem rakšanas urbjiem un naftas sūkņiem",
    "nl": "Verhoogde opbrengst van alle mijnboren en jaknikkers.",
    "no": "Økt utbytte fra alle gruveborer og pumper",
    "pl": "Zwiększona wydajność wszystkich wiertnic górniczych i kiwonów.",
    "pt-BR": "Aumento de produção de todas brocas de mineração e bombas de óleo.",
    "pt-PT": "Maior rendimento das brocas de mineração e bombas de extração.",
    "ro": "Randament crescut de la toate excavatoarele miniere şi sondele de pompare.",
    "ru": "Увеличенная добыча со всех буров и нефтяных вышек.",
    "sk": "Zvyšuje výťažok zo všetkých ťažobných zariadení a ropných čerpadiel.",
    "sl": "Povečan izkoristek vseh rudarskih vrtalnikov in pump.",
    "sr": "Повећан принос из свих бушилица и нафтних пумпа.",
    "sv-SE": "Ökad avkastning från alla gruv- och oljeborrar.",
    "th": "เพิ่มผลผลิตจาก แท่นขุดเจาะแร่ และ แท่นขุดเจาะน้ํามัน",
    "tr": "Tüm maden sondajlarının ve atbaşı pompaların verimini arttırır.",
    "uk": "Збільшений видобуток зі всі копалень та свердловин.",
    "vi": "Tăng năng suất cho các máy khoan đào và máy khai thác dầu",
    "zh-CN": "提高所有采矿机和抽油机的产量。",
    "zh-TW": "提高所有採礦機和抽油幫浦的產量。"
  },
  "icon": "0ef9adf4-18ef-b14e-f984-ccdf2f8ae394",
  "prerequisites": [
    "mining-productivity-3",
    "space-science-pack"
  ],
  "researchIngredients": [
    {
      "type": "item",
      "name": "automation-science-pack"
    },
    {
      "type": "item",
      "name": "logistic-science-pack"
    },
    {
      "type": "item",
      "name": "chemical-science-pack"
    },
    {
      "type": "item",
      "name": "production-science-pack"
    },
    {
      "type": "item",
      "name": "utility-science-pack"
    },
    {
      "type": "item",
      "name": "space-science-pack"
    }
  ],
  "researchCountFormula": "2500*(L - 3)",
  "researchTime": 60,
  "level": 4,
  "maxLevel": 4294967295
}
```
</details>