# Data: Machine

The machines which are able to transform a set of ingredients to a set of products, using the recipes. A special machine
is "character", which represents the character itself and its hand-crafting abilities.

The energy usage is provided as a number up to 1000, and a corresponding unit for it. Higher numbers will be converted
to their next unit to keep the actual number small.

## Fields

| Name               | Type                    | Default*      | Example                                             | Description                                                              |
|--------------------|-------------------------|---------------|-----------------------------------------------------|--------------------------------------------------------------------------|
| kind               | "machine.v1"            | _(mandatory)_ | "machine.v1"                                        | The kind of the data.                                                    |
| name               | string                  | _(mandatory)_ | "assembling-machine-1"                              | The internal name of the machine, used in Factorio.                      |
| labels             | object\<string, string> | {}            | {"en": "Assembling machine 1"}                      | The name of the machine, translated to all available locales.            |
| descriptions       | object\<string, string> | {}            | {}                                                  | The description of the machine, translated to all available locales.     |
| icon               | string                  | ""            | 12477e4d-4c3c-4499-8fe1-adacf4ff3a95"               | The id of the icon, used in the stylesheet.                              |
| craftingCategories | array\<string>          | []            | ["crafting", "basic-crafting", "advanced-crafting"] | The crafting categories supported by the machine to craft recipes.       |
| resourceCategories | array\<string>          | []            | []                                                  | The resource categories supported by the machines to mine resources.     |
| speed              | float                   | 1.            | 0.5                                                 | The speed of the machine.                                                |
| itemSlots          | integer                 | 255           | 255                                                 | The number of item slots available in the machine, or 255 for unlimited. |
| fluidInputSlots    | integer                 | 0             | 0                                                   | The number of fluid input slots available in the machine.                |
| fluidOutputSlots   | integer                 | 0             | 0                                                   | The number of fluid output slots available in the machine.               |
| moduleSlots        | integer                 | 0             | 0                                                   | The number of module slots available in the machine.                     |
| energyUsage        | float                   | 0.            | 75.                                                 | The energy used by the machine.                                          |
| energyUsageUnit    | string                  | ""            | W"                                                  | The unit for the energy usage.                                           |

_*) If the field is missing in the data, the default value must be assumed. Mandatory fields are always present._

## Examples

<details>
    <summary><b>Example:</b> assembling-machine-3</summary>

```json
{
  "kind": "machine.v1",
  "name": "assembling-machine-3",
  "labels": {
    "af": "Vervaardigings masjien 3",
    "ar": "3 عيمجتلا زاهج",
    "be": "Аўтамат зборкі 3",
    "bg": "Сглобяваща машина 3",
    "ca": "Màquina de muntatge 3",
    "cs": "Montovna 3",
    "da": "Fremstillingsmaskine 3",
    "de": "Montagemaschine 3",
    "el": "Μηχανή συναρμολόγησης 3",
    "en": "Assembling machine 3",
    "eo": "Muntilo 3",
    "es-ES": "Máquina de ensamblaje 3",
    "et": "Kokkupaneku masin 3",
    "eu": "Muntaketa makina 3",
    "fa": "۳ ژاتنوم نیشام",
    "fi": "Kasauskone 3",
    "fr": "Machine d'assemblage très rapide",
    "ga-IE": "Meaisín cóimeála 3",
    "he": "3 הבכרה תנוכמ",
    "hr": "Stroj za sastavljanje 3",
    "hu": "Összeszerelő gép 3",
    "id": "Mesin Perakitan 3",
    "is": "Samsetningarvél 3",
    "it": "Macchina assemblatrice 3",
    "ja": "組立機3",
    "ka": "აწყობის მანქანა 3",
    "kk": "Құрастыру мәшинесі 3",
    "ko": "조립 기계 3",
    "lt": "Surinkimo mašina 3",
    "lv": "Montāžas iekārta 3",
    "nl": "Montagemachine 3",
    "no": "Monteringsmaskin 3",
    "pl": "Automat montażowy 3",
    "pt-BR": "Máquina de montagem 3",
    "pt-PT": "Máquina de montagem 3",
    "ro": "Mașină de asamblare 3",
    "ru": "Сборочный автомат 3",
    "sk": "Montážny stroj 3",
    "sl": "Montažni stroj 3",
    "sq": "Makineri montuese 3",
    "sr": "Машина за склапање 3",
    "sv-SE": "Monteringsmaskin 3",
    "th": "เครื่องผลิต 3",
    "tr": "Montaj makinesi 3",
    "uk": "Складальний автомат 3",
    "vi": "Máy lắp ráp 3",
    "zh-CN": "组装机3型",
    "zh-TW": "組裝機Ⅲ"
  },
  "icon": "12477e4d-4c3c-4499-8fe1-adacf4ff3a95",
  "craftingCategories": [
    "basic-crafting",
    "crafting",
    "advanced-crafting",
    "crafting-with-fluid"
  ],
  "speed": 1.25,
  "fluidInputSlots": 1,
  "fluidOutputSlots": 1,
  "moduleSlots": 4,
  "energyUsage": 375,
  "energyUsageUnit": "kW"
}
```
</details>

<details>
    <summary><b>Example:</b> character</summary>

```json
{
  "kind": "machine.v1",
  "name": "character",
  "labels": {
    "af": "Karakter",
    "ar": "ةيصخشلا",
    "be": "Персанаж",
    "bg": "Герой",
    "ca": "Personatge",
    "cs": "Postava",
    "da": "Karakter",
    "de": "Charakter",
    "el": "Χαρακτήρας",
    "en": "Character",
    "eo": "Rolulo",
    "es-ES": "Personaje",
    "et": "Tegelane",
    "eu": "Pertsonaia",
    "fa": "رتکاراک",
    "fi": "Hahmo",
    "fil": "Tao",
    "fr": "Personnage",
    "ga-IE": "Carachtar",
    "he": "תומד",
    "hr": "Lik",
    "hu": "Karakter",
    "id": "Karakter",
    "is": "Persóna",
    "it": "Personaggio",
    "ja": "キャラクター",
    "ka": "პერსონაჟი",
    "kk": "Кейіпкер",
    "ko": "캐릭터",
    "lt": "Veikėjas",
    "lv": "Tēls",
    "nl": "Avatar",
    "no": "Karakter",
    "pl": "Postać",
    "pt-BR": "Personagem",
    "pt-PT": "Carácter",
    "ro": "Personaj",
    "ru": "Персонаж",
    "sk": "Postava",
    "sl": "Lik",
    "sr": "Карактер",
    "sv-SE": "Karaktär",
    "th": "ผู้เล่น",
    "tr": "Karakter",
    "uk": "Персонаж",
    "vi": "Nhân vật",
    "zh-CN": "玩家",
    "zh-TW": "人物"
  },
  "icon": "aafca6ba-0c05-7eb1-8758-d0367cadf4f5",
  "craftingCategories": [
    "crafting"
  ],
  "resourceCategories": [
    "basic-solid"
  ],
  "speed": 0.5
}
```
</details>

<details>
    <summary><b>Example:</b> electric-mining-drill</summary>

```json
{
  "kind": "machine.v1",
  "name": "electric-mining-drill",
  "labels": {
    "af": "Elektriese mynboor",
    "ar": "يئابرهكلا بيقنتلا رافح",
    "be": "Электрычны бур",
    "bg": "Електрическа рудна установка",
    "ca": "Explotació minera elèctrica",
    "cs": "Elektrické těžící zařízení",
    "da": "Elektrisk minebor",
    "de": "Elektrischer Erzförderer",
    "el": "Ηλεκτρικό τρυπάνι εξόρυξης",
    "en": "Electric mining drill",
    "eo": "Elektra fosmaŝino",
    "es-ES": "Perforadora Eléctrica",
    "et": "Elektriline kaevandamispuur",
    "eu": "Meatzaritzako zulatzaile elektrikoa",
    "fa": "یقرب راکندعم لیرد",
    "fi": "Sähköinen kaivospora",
    "fil": "Barenang de-kuryente na pang-mina",
    "fr": "Foreuse électrique",
    "fy-NL": "Elektryske myn boar",
    "ga-IE": "Druilire mianadóireachta leictreach",
    "he": "תילמשח חודיק תנוכמ",
    "hr": "Električna rudarska bušilica",
    "hu": "Elektromos bányagép",
    "id": "Bor tambang listrik",
    "is": "Rafknúinn námubor",
    "it": "Trivella elettrica",
    "ja": "電動掘削機",
    "ka": "ელექტრო ბურღი",
    "kk": "Электрлік бұрғы",
    "ko": "전기 채광 드릴",
    "lt": "Elektrinis kasybos grąžtas",
    "lv": "Elektriskais rakšanas urbis",
    "nl": "Elektrische mijnboor",
    "no": "Elektrisk gruvedrill",
    "pl": "Elektryczna wiertnica górnicza",
    "pt-BR": "Mineradora elétrica",
    "pt-PT": "Broca perfurante elétrica",
    "ro": "Foreză electrică",
    "ru": "Электрический бур",
    "sk": "Elektrické tažobné zariadenie",
    "sl": "Električen rudarski sveder",
    "sq": "Gërrmues elektrik",
    "sr": "Електрична бушилица",
    "sv-SE": "Elektrisk gruvborr",
    "th": "แท่นขุดเจาะไฟฟ้า",
    "tr": "Elektrikli maden sondajı",
    "uk": "Електрична кар'єрна установка",
    "vi": "Dàn khoan điện",
    "zh-CN": "电力采矿机",
    "zh-TW": "電能採礦機"
  },
  "icon": "9df28750-739a-e12e-07de-13d9667aefff",
  "resourceCategories": [
    "basic-solid"
  ],
  "speed": 0.5,
  "fluidInputSlots": 1,
  "fluidOutputSlots": 1,
  "moduleSlots": 3,
  "energyUsage": 90,
  "energyUsageUnit": "kW"
}
```
</details>
