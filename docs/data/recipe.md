# Data: Recipe

A `recipe` is the transformation of one group of items into another one using some kind of machine (including the
character itself). 

Recipes come in the following types:

* **crafting:** The crafting recipe is either processed in an assembling machine, or directly in hand (if possible).
  Most recipes are crafting recipes.
* **mining:** The mining recipes are used by mining drills and pumps, to mine some kind of ore or pump some kind of
  fluid from the ground.
* **rocket-launch:** The rocket-launch recipes involve launching a rocket. The ingredient must be placed in the rocket,
  and the products will be returned after the rocket was actually launched.

## Fields

| Name         | Type                    | Default*      | Example                                | Description                                                         |
|--------------|-------------------------|---------------|----------------------------------------|---------------------------------------------------------------------|
| kind         | "recipe.v1"             | _(mandatory)_ | "recipe.v1"                            | The kind of the data.                                               |
| type         | string                  | _(mandatory)_ | "crafting"                             | The type of the recipe.                                             |
| name         | string                  | _(mandatory)_ | "electronic-circuit"                   | The internal name of the recipe, used in Factorio.                  |
| labels       | object\<string, string> | {}            | {"en": "Electronic circuit"}           | The name of the recipe, translated to all available locales.        |
| descriptions | object\<string, string> | {}            | {}                                     | The description of the recipe, translated to all available locales. |
| icon         | string                  | ""            | "d231e7ea-4dfe-a1a1-1f16-9b9cc858f0f8" | The id of the icon, used in the stylesheet.                         |
| time         | float                   | 0             | 0.5                                    | The crafting time of the recipe, in seconds.                        |
| category     | string                  | ""            | "crafting"                             | The category of the recipe, defining what machines can produce it.  |
| ingredients  | array\<Ingredient>      | []            |                                        | The ingredients needed for the recipes, as array.                   |
| products     | array\<Product>         | []            |                                        | The products of the recipes, as array.                              |
| enabled      | boolean                 | false         | true                                   | Whether the recipe is enabled when starting a game.                 |
| hidden       | boolean                 | false         | false                                  | Whether the recipe is normally hidden from the player.              |

_*) If the field is missing in the data, the default value must be assumed. Mandatory fields are always present._

### Ingredient:

| Name   | Type                        | Default*      | Example      | Description                                           |
|--------|-----------------------------|---------------|--------------|-------------------------------------------------------|
| type   | "item", "fluid", "resource" | _(mandatory)_ | "item"       | The type of the ingredient.                           |
| name   | string                      | _(mandatory)_ | "iron-plate" | The name of the ingredient.                           |
| amount | float                       | 1.            | 1.           | The amount of the ingredient required for the recipe. |

### Product:

| Name        | Type                        | Default*      | Example      | Description                                                 |
|-------------|-----------------------------|---------------|--------------|-------------------------------------------------------------|
| type        | "item", "fluid", "resource" | _(mandatory)_ | "item"       | The type of the product.                                    |
| name        | string                      | _(mandatory)_ | "iron-plate" | The name of the product.                                    |
| amountMin   | float                       | 1.            | 1.           | The minimal amount of the product produced by the recipe.   |
| amountMax   | float                       | 1.            | 1.           | The maximal amount of the product produced by the recipe.   |
| probability | float                       | 1.            | 1.           | The probability that the product is produced by the recipe. |

## Examples

<details>
    <summary><b>Example:</b> crafting | electronic-circuit</summary>

```json
{
  "kind": "recipe.v1",
  "type": "crafting",
  "name": "electronic-circuit",
  "labels": {
    "af": "Elektroniese stroombaan",
    "ar": "ةينورتكلإلا رئاودلا",
    "be": "Мікрасхема",
    "bg": "Електронна схема",
    "ca": "Circuit electrònic",
    "cs": "Elektronický obvod",
    "da": "Elektronisk kredsløb",
    "de": "Elektronischer Schaltkreis",
    "el": "Ηλεκτρονικό κύκλωμα",
    "en": "Electronic circuit",
    "eo": "Elektronika cirkvito",
    "es-ES": "Circuito electrónico",
    "et": "Vooluring",
    "eu": "Zirkuitu elektronikoa",
    "fa": "یکیرتکلا رادم",
    "fi": "Sähköpiiri",
    "fr": "Circuit électronique",
    "ga-IE": "Ciorcád leictreonach",
    "he": "ינורטקלא לגעמ",
    "hr": "Električni sklop",
    "hu": "Elektromos áramkör",
    "id": "Sirkuit elektronik",
    "is": "Hringrásarborð",
    "it": "Circuito elettronico",
    "ja": "電子基板",
    "ka": "ელექტრონული წრე",
    "kk": "Электросхема",
    "ko": "전자 회로",
    "lt": "Mikroschema",
    "lv": "Mikroshēma",
    "nl": "Elektronisch circuit",
    "no": "Kretskort",
    "pl": "Układ elektroniczny",
    "pt-BR": "Circuito Eletrônico",
    "pt-PT": "Circuito eletrónico",
    "ro": "Circuit electronic",
    "ru": "Электросхема",
    "sk": "Elektronický obvod",
    "sl": "Elektronsko vezje",
    "sq": "Qark elektronik",
    "sr": "Електронско коло",
    "sv-SE": "Kretskort",
    "th": "แผงวงจรอิเล็กทรอนิกส์",
    "tr": "Elektronik devre",
    "uk": "Електронна схема",
    "vi": "Mạch điện tử",
    "zh-CN": "电路板",
    "zh-TW": "電子電路板"
  },
  "icon": "d231e7ea-4dfe-a1a1-1f16-9b9cc858f0f8",
  "time": 0.5,
  "category": "crafting",
  "ingredients": [
    {
      "type": "item",
      "name": "iron-plate"
    },
    {
      "type": "item",
      "name": "copper-cable",
      "amount": 3
    }
  ],
  "products": [
    {
      "type": "item",
      "name": "electronic-circuit"
    }
  ],
  "enabled": true
}
```
</details>

<details>
    <summary><b>Example:</b> mining | uranium-ore</summary>

```json
{
  "kind": "recipe.v1",
  "type": "mining",
  "name": "uranium-ore",
  "labels": {
    "af": "Uraan erts",
    "ar": "مويناروي ماخ",
    "be": "Уранавая руда",
    "bg": "Уранова руда",
    "ca": "Mineral d'urani",
    "cs": "Uranová ruda",
    "da": "Uranmalm",
    "de": "Uranerz",
    "el": "Μετάλλευμα ουρανίου",
    "en": "Uranium ore",
    "eo": "Urania erco",
    "es-ES": "Mineral de uranio",
    "et": "Uraanimaak",
    "eu": "Uranio-mea",
    "fa": "مویناروا گنس",
    "fi": "Uraanimalmi",
    "fil": "Hilaw na uranium",
    "fr": "Minerai d’uranium",
    "fy-NL": "Uranium erts",
    "ga-IE": "Mian úráiniaim",
    "he": "םוינרוא תרפע",
    "hr": "Uranova ruda",
    "hu": "Uránérc",
    "id": "Bijih uranium",
    "is": "Úrangrýti",
    "it": "Uranio grezzo",
    "ja": "ウラン鉱石",
    "ka": "ურანის მადანი",
    "kk": "Уран кені",
    "ko": "우라늄 광석",
    "lt": "Urano rūda",
    "lv": "Urāna rūda",
    "nl": "Uraniumerts",
    "no": "Uranmalm",
    "pl": "Ruda uranu",
    "pt-BR": "Minério de urânio",
    "pt-PT": "Minério de urânio",
    "ro": "Minereu de uraniu",
    "ru": "Урановая руда",
    "sk": "Uránová ruda",
    "sl": "Uranova ruda",
    "sq": "Xeheror urani",
    "sr": "Руда ураниума",
    "sv-SE": "Uranmalm",
    "th": "แร่ยูเรเนียม",
    "tr": "Uranyum cevheri",
    "uk": "Уранова руда",
    "vi": "Quặng Urani",
    "zh-CN": "铀矿",
    "zh-TW": "鈾礦"
  },
  "icon": "84181e70-4e72-1e8a-dbef-c3cba1b5e08c",
  "category": "basic-solid",
  "ingredients": [
    {
      "type": "resource",
      "name": "uranium-ore"
    },
    {
      "type": "fluid",
      "name": "sulfuric-acid",
      "amount": 10
    }
  ],
  "products": [
    {
      "type": "item",
      "name": "uranium-ore"
    }
  ]
}
```
</details>

<details>
    <summary><b>Example:</b> rocket-launch | satellite</summary>

```json
{
  "kind": "recipe.v1",
  "type": "rocket-launch",
  "name": "satellite",
  "labels": {
    "af": "Satelliet",
    "ar": "ةيعانصلا رامقألا",
    "be": "Спадарожнік",
    "bg": "Спътник",
    "ca": "Satèl·lit",
    "cs": "Satelit",
    "da": "Satellit",
    "de": "Satellit",
    "el": "Δορυφόρος",
    "en": "Satellite",
    "eo": "Satelito",
    "es-ES": "Satélite",
    "et": "Satelliit",
    "eu": "Satelitea",
    "fa": "هراوهام",
    "fi": "Satelliitti",
    "fr": "Satellite",
    "ga-IE": "Satailít",
    "he": "ןייוול",
    "hr": "Satelit",
    "hu": "Műhold",
    "id": "Satelit",
    "is": "Gervitungl",
    "it": "Satellite",
    "ja": "衛星",
    "ka": "თანამგზავრი",
    "kk": "Серік",
    "ko": "인공위성",
    "lt": "Palydovas",
    "lv": "Satelīts",
    "nl": "Sateliet",
    "no": "Satelitt",
    "pl": "Satelita",
    "pt-BR": "Satélite",
    "pt-PT": "Satélite",
    "ro": "Satelit",
    "ru": "Спутник",
    "sk": "Satelit",
    "sl": "Satelit",
    "sq": "Satelit",
    "sr": "Сателит",
    "sv-SE": "Satellit",
    "th": "ดาวเทียม",
    "tr": "Uydu",
    "uk": "Супутник",
    "vi": "Vệ tinh",
    "zh-CN": "卫星",
    "zh-TW": "衛星"
  },
  "descriptions": {
    "af": "Die satelliet moet in die vuurpyl geplaas word.",
    "ar": ".خوراصلا يف يعانصلا رمقلا عضو يغبني",
    "be": "Спадарожнік мусіць быць змешчаны ў ракеце.",
    "bg": "Спътникът трябва да се сложи в ракетата.",
    "ca": "Els satèl·lits s'han de posar dins del coet espacial.",
    "cs": "Satelit by měl být vložen do rakety.",
    "da": "Satellitten skal placeres i raketten.",
    "de": "Der Satellit sollte in die Rakete eingesetzt werden.",
    "el": "Ο δορυφόρος πρέπει να τοποθετηθεί μέσα στον πύραυλο.",
    "en": "The satellite should be put into the rocket.",
    "eo": "La satelito devas esti metita en la raketo.",
    "es-ES": "El satélite tiene que ser insertado en el cohete.",
    "et": "Sateliit peaks olema sisestatud raketti.",
    "eu": "Satelitea suziri batean jarri beharko zen.",
    "fa": ".دوش هداد رارق کشوم رد دیاب هراوهام",
    "fi": "Satelliitti tulee laittaa raketin sisään.",
    "fr": "Le satellite est à mettre dans la fusée.",
    "ga-IE": "Ba chóir leis an satailít a chur isteach sa roicéad.",
    "he": ".ליטה ךות לא ןייוולה תא סינכהל שי",
    "hr": "Satelit bi trebao biti postavljen u raketu.",
    "hu": "A műholdat a rakétában kell elhelyezni.",
    "id": "Satelit harus diletakkan ke dalam roket.",
    "it": "Il satellite deve essere inserito dentro il razzo.",
    "ja": "衛星はロケットに積んでください。",
    "ka": "სატელიტი რაკეტაში უნდა იყოს ჩასმული.",
    "kk": "Серік зымыранның ішіне салынуы керек.",
    "ko": "인공위성은 로켓 내부에 넣어져야 합니다.",
    "lt": "Palydovas turėtų būti įdėtas į raketą.",
    "lv": "Satelīts jāievieto raķetē.",
    "nl": "De satelliet moet in de raket geplaatst worden.",
    "no": "Satellitten skal settes i raketten.",
    "pl": "Satelita powinien być umieszczony w rakiecie.",
    "pt-BR": "O satélite deve ser colocado no foguete.",
    "pt-PT": "O satélite deve ser colocado no foguetão.",
    "ro": "Satelitul ar trebui pus în rachetă.",
    "ru": "Спутник должен быть помещён в ракету.",
    "sk": "Satelit by sa mal umiestniť do rakety.",
    "sl": "Satelit je treba dati v raketo.",
    "sq": "Sateliti duhet të vendoset në rraketë.",
    "sr": "Сателит би требало да се стави у ракету.",
    "sv-SE": "Satelliten bör sättas i raketen.",
    "th": "ดาวเทียมควรใส่ไว้ในจรวด",
    "tr": "Uydunun roketin içine koyulması gerekir.",
    "uk": "Супутник повинен бути поміщений в ракету.",
    "vi": "Vệ tinh cần được đặt vào trong tên lửa không gian.",
    "zh-CN": "卫星应装入火箭来发射入轨。",
    "zh-TW": "此衛星應放入火箭內。"
  },
  "icon": "15e80891-c2b9-48ac-c27d-3237d8de8e54",
  "ingredients": [
    {
      "type": "item",
      "name": "satellite"
    }
  ],
  "products": [
    {
      "type": "item",
      "name": "space-science-pack",
      "amountMin": 1000,
      "amountMax": 1000
    }
  ]
}
```
</details>
