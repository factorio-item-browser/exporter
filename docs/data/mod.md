# Data: Mod

The mods which have been loaded to create the data dump. The data for the mods are mostly taken from their info.json
file.

## Fields

| Name         | Type                    | Default*      | Example                                                                    | Description                                                      |
|--------------|-------------------------|---------------|----------------------------------------------------------------------------|------------------------------------------------------------------|
| kind         | "mod.v1"                | _(mandatory)_ | "mod.v1"                                                                   | The kind of the data.                                            |
| name         | string                  | _(mandatory)_ | "base"                                                                     | The name of the mod.                                             |
| labels       | object\<string, string> | {}            | {"en": "Base mod"}                                                         | The title of the mod, translated to all available locales.       |
| descriptions | object\<string, string> | {}            | {"en": "Basic mod with all the default game data and standard campaigns."} | The description of the mod, translated to all available locales. |
| icon         | string                  | ""            | d6fd5a46-5343-4414-f09f-fe2fffdae64a"                                      | The id of the icon, used in the stylesheet.                      |
| author       | string                  | ""            | Factorio team"                                                             | The author of the mod.                                           |
| version      | string                  | ""            | 1.1.92"                                                                    | The version of the mod.                                          |

_*) If the field is missing in the data, the default value must be assumed. Mandatory fields are always present._

## Examples

<details>
    <summary><b>Example:</b> base</summary>

```json
{
  "kind": "mod.v1",
  "name": "base",
  "labels": {
    "af": "Basis mod",
    "ar": "ةيساسألا دوملا",
    "be": "Базавы мод",
    "bg": "Основна модификация",
    "ca": "Mod base",
    "cs": "Základní verze",
    "da": "Grundspil",
    "de": "Basis-Mod",
    "el": "Βασικό mod",
    "en": "Base mod",
    "eo": "Baza modifaĵo",
    "es-ES": "Mod base",
    "et": "Põhi mod",
    "eu": "Oinarrizko mod-a",
    "fi": "Peruspeli",
    "fr": "Jeu de base",
    "fy-NL": "Basismod",
    "ga-IE": "Mod bunúsach",
    "he": "סיסבה קחשמ",
    "hr": "Bazna modifikacija",
    "hu": "Alapjáték",
    "id": "Mod dasar",
    "is": "Grunn leikbreyting",
    "it": "Mod base",
    "ja": "ベースMOD",
    "ka": "ფუძური მოდი",
    "kk": "Негізгі мод",
    "ko": "기본 모드",
    "lt": "Pagrindinė modifikacija",
    "lv": "Pamata modifikācija",
    "nl": "Basismod",
    "no": "Basemod",
    "pl": "Modyfikacja bazowa",
    "pt-BR": "Mod base",
    "pt-PT": "Mod Básico",
    "ro": "Mod de bază",
    "ru": "Базовый мод",
    "sk": "Základný Mod",
    "sl": "Osnovni mod",
    "sr": "Основни мод",
    "sv-SE": "Bas Mod",
    "th": "ม็อดพื้นฐาน",
    "tr": "Temel",
    "uk": "Базова модифікація",
    "vi": "Base mod",
    "zh-CN": "官方基础包",
    "zh-TW": "Base mod 基底模組"
  },
  "descriptions": {
    "en": "Basic mod with all the default game data and standard campaigns."
  },
  "icon": "d6fd5a46-5343-4414-f09f-fe2fffdae64a",
  "author": "Factorio team",
  "version": "1.1.92"
}
```
</details>