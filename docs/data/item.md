# Data: Item

An `item` is basically everything which can be part of recipes either as ingredient or result. 

The items come with these types:

* **item:** An actual item which you can hold in your hand, and drop on the ground.
* **fluid:** A fluid which gets pumped through pipes.
* **resource:** A resource to be mined by a mining drill or pump, often placed on the map as a deposit.

## Fields

| Name         | Type                        | Default*               | Example                                              | Description                                                       |
|--------------|-----------------------------|------------------------|------------------------------------------------------|-------------------------------------------------------------------|
| kind         | "item.v1"                   | _(mandatory)_          | "item.v1"                                            | The kind of the data.                                             |
| type         | "item", "fluid", "resource" | _(mandatory)_          | "item"                                               | The type of the item.                                             |
| name         | string                      | _(mandatory)_          | "copper-cable"                                       | The internal name of the item, used in Factorio.                  |
| labels       | object\<string, string>     | {}                     | {"en": "Copper cable"}                               | The name of the item, translated to all available locales.        |
| descriptions | object\<string, string>     | {}                     | {"en": "Can also be used to manually connect [...]"} | The description of the item, translated to all available locales. |
| icon         | string                      | ""                     | a211578c-5037-a1ee-242f-e692ebdeb7d6"                | The id of the icon, used in the stylesheet.                       |
| stackSize    | integer                     | 50 _(for type="item")_ | 200                                                  | The stack size of the item. Never set on fluids and ressources.   |
| hidden       | boolean                     | false                  | false                                                | Whether the item is normally hidden from the player.              |

_*) If the field is missing in the data, the default value must be assumed. Mandatory fields are always present._

## Examples

<details>
    <summary><b>Example:</b> item | copper-cable</summary>

```json
{
  "kind": "item.v1",
  "type": "item",
  "name": "copper-cable",
  "labels": {
    "af": "Koper kabel",
    "ar": "يساحن لبيك",
    "be": "Медны дрот",
    "bg": "Медна жица",
    "ca": "Cable de coure",
    "cs": "Měděný kabel",
    "da": "Kobberkabel",
    "de": "Kupferkabel",
    "el": "Καλώδιο χαλκού",
    "en": "Copper cable",
    "eo": "Kupra kablo",
    "es-ES": "Cable de cobre",
    "et": "Vaskkaabel",
    "eu": "Kobrezko kablea",
    "fa": "یسم میس",
    "fi": "Kuparikaapeli",
    "fil": "Tansong kable",
    "fr": "Câble en cuivre",
    "ga-IE": "Cábla copair",
    "he": "תשוחנ לבכ",
    "hr": "Bakreni kabel",
    "hu": "Rézkábel",
    "id": "Kabel tembaga",
    "is": "Kopar kapall",
    "it": "Cavo di rame",
    "ja": "銅線",
    "ka": "სპილენძის სადენი",
    "kk": "Мыс сым",
    "ko": "구리 전선",
    "lt": "Varinis kabelis",
    "lv": "Vara kabelis",
    "nl": "Koperdraad",
    "no": "Kobberkabel",
    "pl": "Miedziany drut",
    "pt-BR": "Cabo de cobre",
    "pt-PT": "Cabo cúprico",
    "ro": "Cablu de cupru",
    "ru": "Медный кабель",
    "sk": "Medený kábel",
    "sl": "Bakrena žica",
    "sq": "Kabëll bakri",
    "sr": "Бакарна жица",
    "sv-SE": "Kopparkabel",
    "th": "สายทองแดง",
    "tr": "Bakır kablo",
    "uk": "Мідний дріт",
    "vi": "Cáp đồng",
    "zh-CN": "铜线",
    "zh-TW": "銅電線"
  },
  "descriptions": {
    "af": "Kan gebruik word om elektriese kragpale en elektriese skakelaars te koppel of ontkoppel met [Bou].",
    "ar": ".[ءانب] ىلع طغضلاب ايودي ةقاطلا حيتافمو ةيئابرهكلا ةدمعألا عطقو طبرل مدختست",
    "be": "Таксама можа быць выкарыстаны для ручнога злучэння і адлучэння слупоў ЛЭП і вымыкальнікаў з дапамогай [Будаваць].",
    "bg": "Може същевременно да се ползва и за ръчно свързване и разкачане на електрически проводи и превключватели чрез [Построяване].",
    "ca": "També es pot utilitzar per connectar i desconnectar manualment pals elèctrics i interruptors amb [Construeix].",
    "cs": "Může být též použito pro manuální připojení a odpojení elektrických sloupů a vypínačů pomocí [Stavět].",
    "da": "Kan også bruges til manuelt at forbinde og frakoble elmaster og strømafbrydere med [Byg].",
    "de": "Kann auch durch [Bauwerk platzieren] zum manuellen Verbinden und Trennen von Strommasten und Stromschaltern verwendet werden.",
    "el": "Μπορεί να χρησιμοποιηθεί για να συνδέσει και να αποσυνδέσει ηλεκτρικούς πόλους και διακόπτες ρεύματος με [Κατασκευή].",
    "en": "Can also be used to manually connect and disconnect electric poles and power switches with [Build].",
    "eo": "Ankaŭ uzeblas por konekti aŭ malkonekti elektrajn fostojn kaj ŝaltilojn per [Konstru].",
    "es-ES": "También puede emplearse para conectar y desconectar polos eléctricos e interruptores manualmente, usando [Construir].",
    "et": "Saab kasutada ka elektripostide ja toitelülitite ühendamiseks ja lahti ühendamiseks kasutades [Ehita] nuppu.",
    "eu": "Poste elekrikoak eta etengailuak konektatzeko eta deskonektatzeko ere erabil daiteke eskuz, [Eraiki] erabiliz.",
    "fa": ".دوش هدافتسا [Build] اب قرب یاه دیلک و یقرب یاه لکد یتسد لاصتا عطق ای لاصتا یارب ناوتیم نینچمه",
    "fi": "Voidaan käyttää myös sähkötolppien ja virtakytkinten kytkemiseen tai katkaisemiseen käsin.\n[Rakenna]: kytke tai irroita",
    "fr": "Peut également être utilisé pour connecter et déconnecter manuellement les poteaux électriques et les interrupteurs d'alimentation avec [Construire].",
    "ga-IE": "Is féidir leis chomh maith a úsáid ar son ceangal nó neamhcheangal a dhéanamh go lámhoibríoch idir chuaillí leicteacha agus lasca cumhachta trí [Tógáil].",
    "he": ".[הנבמ תמקה] לע הציחלב םיקספמו למשח ידומע לש ינדי קותינו רוביחל םג שמשמ",
    "hr": "Također se može koristiti za spajanje i odspajanje dalekovoda i električkih sklopki s [Build].",
    "hu": "Villanyoszlopok és villanykapcsolók manuális csatlakoztatására és szétválasztására is használható a(z) [Építés] gombbal.",
    "id": "Dapat juga digunakan untuk menghubungkan dan memutus tiang listrik dan saklar daya secara manual dengan [Bangun].",
    "it": "Può anche essere utilizzato per connettere e disconnettere manualmente i pali elettrici e gli interruttori di alimentazione premendo [Costruisci].",
    "ja": "[オブジェクトを設置]を押すことで、電柱や電源スイッチを任意に接続したり接続を外したりするのにも使えます。",
    "ka": "ასევე შეიძლება გამოყენებულ იქნას ელექტრო ბოძებისა და დენის გადამრთველების ხელით დასაკავშირებლად და გამორთვაზე [ააშენე]-ით.",
    "kk": "[Құру] арқылы ЭБЖ тіректері немесе ажыратқыштар арасында сым тарту немесе үзу үшін де қолданылады.",
    "ko": "[설치]을 사용하여 수동으로 전신주와 전원 스위치를 연결하거나 연결을 끊을 수도 있습니다.",
    "lt": "Taip pat gali būti naudojama rankiniu būdu prijungti ir atjungti elektros stulpams ir maitinimo jungikliams su [Statyti].",
    "lv": "Var arī tikt izmantots, lai savienotu un atvienot elektrības stabus un slēdžus ar [Izveidot].",
    "nl": "Kan gebruikt worden voor het handmatig verbinden of loskoppelen van elektriciteitspalen en stroomschakelaars met [Bouw].",
    "no": "Kan også brukes til å koble til og fra strømstolper og strømbrytere med [Bygg].",
    "pl": "Może być również używany do ręcznego łączenia i rozłączenia słupów elektrycznych oraz przełączników zasilania z [Buduj].",
    "pt-BR": "Também pode ser usado para manualmente conectar e desconectar postes de eletricidade e interruptores com [Construir].",
    "pt-PT": "Pode também ser usado manualmente para ligar e desligar postes eléctricos de interruptores com [Construir].",
    "ro": "Poate fi folosit și pentru a conecta și deconecta stâlpii electrici și întrerupătoarele de putere cu [Construire].",
    "ru": "Также используется для протягивания или снятия электрических проводов вручную между опорами и выключателями питания нажатием [Строить].",
    "sk": "Možno ich tiež použiť na ručne pripojenie a odpojenie elektrických stĺpov a vypínačov pomocou [Budovať].",
    "sl": "Lahko se uporabi tudi za ročno povezavo ali odklop električnih drogov in stikal z [Zgradi].",
    "sr": "Такође се може користити да се ручно споје и раздвоје бандере и прекидачи за струју уз помоћ [Build].",
    "sv-SE": "Kan också användas för att manuellt ansluta eller koppla från elstolpar och strömbrytare med [Bygg].",
    "th": "สามารถใช้เชื่อมหรือตัดการเชื่อมต่อของเสาไฟฟ้ากับสวิตซ์ไฟด้วยตนเองโดยการกด [สร้าง]",
    "tr": "Elektrik direklerini ve devre kesicilerini [İnşa et] kullanarak el ile bağlamak için de kullanılabilir.",
    "uk": "Також використовується для протягування або зняття електричних проводів вручну між опорами і вимикачами живлення натисканням [Побудувати].",
    "vi": "Còn có thể được dùng để tự nối và cắt dây nối các cột điện và công tắc điện bằng nút [Xây].",
    "zh-CN": "按 [建造] 也可以用来手动连接或断开电线杆、电闸等设施之间的电线。",
    "zh-TW": "[建造]可拿來連接或切斷電線桿和電源開關。"
  },
  "icon": "a211578c-5037-a1ee-242f-e692ebdeb7d6",
  "stackSize": 200
}
```
</details>

<details>
    <summary><b>Example:</b> fluid | crude-oil</summary>

```json
{
  "kind": "item.v1",
  "type": "fluid",
  "name": "crude-oil",
  "labels": {
    "af": "Ru-olie",
    "ar": "ماخ طفن",
    "be": "Сырая нафта",
    "bg": "Необработен нефт",
    "ca": "Petroli cru",
    "cs": "Surová ropa",
    "da": "Råolie",
    "de": "Rohöl",
    "el": "Αργό πετρέλαιο",
    "en": "Crude oil",
    "eo": "Nafto",
    "es-ES": "Petróleo crudo",
    "et": "Toornafta",
    "eu": "Petrolio gordina",
    "fa": "ماخ تفن",
    "fi": "Raakaöljy",
    "fil": "Krudo",
    "fr": "Pétrole brut",
    "fy-NL": "Rûge oalje",
    "ga-IE": "Amhola",
    "he": "ימלוג טפנ",
    "hr": "Neobrađena nafta",
    "hu": "Nyersolaj",
    "id": "Minyak mentah",
    "is": "Hráolía",
    "it": "Greggio",
    "ja": "原油",
    "ka": "დაუმუშავებელი ნავთობი",
    "kk": "Шикі мұнай",
    "ko": "원유",
    "lt": "Neapdorota nafta",
    "lv": "Jēlnafta",
    "nl": "Ruwe olie",
    "no": "Råolje",
    "pl": "Ropa naftowa",
    "pt-BR": "Petróleo Bruto",
    "pt-PT": "Petróleo",
    "ro": "Țiței",
    "ru": "Сырая нефть",
    "sk": "Surová ropa",
    "sl": "Surova nafta",
    "sq": "Naftë",
    "sr": "Сирова Нафта",
    "sv-SE": "Råolja",
    "th": "น้ำมันดิบ",
    "tr": "Ham petrol",
    "uk": "Нафта",
    "vi": "Dầu thô",
    "zh-CN": "原油",
    "zh-TW": "原油"
  },
  "icon": "6a02e65e-4759-895c-3e0c-568ece48f151"
}
```
</details>

<details>
    <summary><b>Example:</b> resource | uranium-ore</summary>

```json
{
  "kind": "item.v1",
  "type": "resource",
  "name": "uranium-ore",
  "labels": {
    "af": "Uraan erts",
    "ar": "مويناروي ماخ",
    "be": "Уранавая руда",
    "bg": "Уранова руда",
    "ca": "Mineral d'urani",
    "cs": "Uranová ruda",
    "da": "Uranmalm",
    "de": "Uranerz",
    "el": "Μετάλλευμα ουρανίου",
    "en": "Uranium ore",
    "eo": "Urania erco",
    "es-ES": "Mineral de uranio",
    "et": "Uraanimaak",
    "eu": "Uranio-mea",
    "fa": "مویناروا گنس",
    "fi": "Uraanimalmi",
    "fil": "Hilaw na uranium",
    "fr": "Minerai d’uranium",
    "fy-NL": "Uranium erts",
    "ga-IE": "Mian úráiniaim",
    "he": "םוינרוא תרפע",
    "hr": "Uranova ruda",
    "hu": "Uránérc",
    "id": "Bijih uranium",
    "is": "Úrangrýti",
    "it": "Uranio grezzo",
    "ja": "ウラン鉱石",
    "ka": "ურანის მადანი",
    "kk": "Уран кені",
    "ko": "우라늄 광석",
    "lt": "Urano rūda",
    "lv": "Urāna rūda",
    "nl": "Uraniumerts",
    "no": "Uranmalm",
    "pl": "Ruda uranu",
    "pt-BR": "Minério de urânio",
    "pt-PT": "Minério de urânio",
    "ro": "Minereu de uraniu",
    "ru": "Урановая руда",
    "sk": "Uránová ruda",
    "sl": "Uranova ruda",
    "sq": "Xeheror urani",
    "sr": "Руда ураниума",
    "sv-SE": "Uranmalm",
    "th": "แร่ยูเรเนียม",
    "tr": "Uranyum cevheri",
    "uk": "Уранова руда",
    "vi": "Quặng Urani",
    "zh-CN": "铀矿",
    "zh-TW": "鈾礦"
  },
  "icon": "84181e70-4e72-1e8a-dbef-c3cba1b5e08c"
}
```
</details>
