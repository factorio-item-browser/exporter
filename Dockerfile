ARG EXPORTER_FACTORIO_IMAGE
FROM $EXPORTER_FACTORIO_IMAGE AS patched-factorio

FROM frolvlad/alpine-glibc:latest

RUN apk add --no-cache libwebp-dev

COPY --from=patched-factorio /opt/factorio /opt/factorio
COPY build /opt/exporter/

WORKDIR /opt/exporter
CMD ["bin/process"]
