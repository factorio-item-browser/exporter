package main

import (
	"context"
	"flag"
	"log/slog"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/lambda"
	awsconfig "github.com/aws/aws-sdk-go-v2/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/command"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/factorio"
	"gitlab.com/factorio-item-browser/exporter.git/internal/icon"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	"gitlab.com/factorio-item-browser/exporter.git/internal/middleware"
	"gitlab.com/factorio-item-browser/exporter.git/internal/prepare"
	"gitlab.com/factorio-item-browser/exporter.git/internal/step"
	"gitlab.com/factorio-item-browser/exporter.git/internal/storage"
	"gitlab.com/factorio-item-browser/exporter.git/internal/translate"
)

// Command: process -combination=test -mods=foo,bar
// Command: bin/process -combination="3e533361-8e5e-028d-af0c-2a4bb35d4f2c" -mods="Milestones,ModuleInserterSimplified,aai-containers,aai-industry,aai-signal-transmission,aai-vehicles-ironclad,alien-biomes,base,bullet-trails,combat-mechanics-overhaul,equipment-gantry,flib,grappling-gun,informatron,jetpack,robot_attrition,shield-projector,simhelper,space-exploration,space-exploration-graphics,space-exploration-graphics-2,space-exploration-graphics-3,space-exploration-graphics-4,space-exploration-graphics-5,space-exploration-menu-simulations,space-exploration-official-modpack,space-exploration-postprocess,textplates"
// Command: bin/process -combination="053763de-7c38-e156-689b-242acf65000a" -mods="base,elevated-rails,quality,space-age"

func main() {
	logger := log.New(config.ForLogger())

	// Initialize AWS config
	awsCfg, err := awsconfig.LoadDefaultConfig(context.Background(), awsconfig.WithRegion("eu-central-1"))
	if err != nil {
		logger.Error("aws config error", slog.String("error", err.Error()))
		os.Exit(1)
	}

	// Initialize dependencies
	eventPublisher := event.NewPublisher(config.ForEventPublisher(), awsCfg)
	executor := factorio.NewExecutor()
	translator := translate.NewTranslator()
	iconManager := icon.NewManager()
	iconRenderer := icon.NewRenderer(config.ForIconRenderer())
	modPortal := factorio.NewModPortal(config.ForFactorio())

	dataStorage := storage.NewDataStorage(config.ForStorage(), awsCfg)
	modStorage := storage.NewModStorage(config.ForStorage(), awsCfg)
	modDownloader := factorio.NewModDownloader(modPortal, modStorage, executor)

	preparers := []prepare.Preparer{
		prepare.NewDirectoryPreparer(),
		prepare.NewFactorioPreparer(config.ForDirectories()),
		prepare.NewDumperModPreparer(config.ForDirectories()),
		prepare.NewModListPreparer(),
	}

	processors := []dump.Processor{
		dump.NewErrorProcessor(),
		dump.NewIconProcessor(iconManager),
		dump.NewModProcessor(iconManager, translator),

		dump.NewFluidProcessor(iconManager, translator),
		dump.NewItemProcessor(iconManager, translator),
		dump.NewMachineProcessor(iconManager, translator),
		dump.NewRecipeProcessor(iconManager, translator),
		dump.NewResourceProcessor(iconManager, translator),
		dump.NewSurfaceProcessor(iconManager, translator),
		dump.NewTechnologyProcessor(iconManager, translator),
	}

	steps := []step.Step{
		step.NewPrepareStep(preparers),
		step.NewDownloadModsStep(config.ForFactorio(), modDownloader),
		step.NewRunFactorioStep(executor),
		step.NewProcessStep(processors),
		step.NewRenderIconsStep(iconManager, iconRenderer),
		step.NewUploadStep(dataStorage),
		step.NewCleanStep(),
	}

	cmd := command.NewProcessCommand(eventPublisher, instance.NewCreator(config.ForDirectories()), steps)

	if config.ForLambda().IsLambda {
		// We are running inside a lambda function.
		lambda.Start(log.NewMiddleware(logger, middleware.ExtractSqsMessage(cmd.Run)))
		return
	}

	var params command.ProcessCommandParams
	var modNamesList string
	flag.StringVar(&params.CombinationId, "combination", "", "The id of the combination to process.")
	flag.StringVar(&modNamesList, "mods", "", "The comma separated list of mods to process.")
	flag.Parse()
	params.ModNames = strings.Split(modNamesList, ",")

	ctx := log.NewContext(context.Background(), logger)
	err = cmd.Run(ctx, params)
	if err != nil {
		logger.Error("process error", slog.String("error", err.Error()))
		os.Exit(1)
	}
}
