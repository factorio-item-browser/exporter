--- Maps the prototype to a fluid.
--- @param prototype table
--- @return table|nil
local function mapFluid(prototype)
    if not prototype.valid or prototype.parameter then
        return nil
    end

    return {
        name = prototype.name,
        localised_name = prototype.localised_name,
        localised_description = prototype.localised_description,
        fuel_value = prototype.fuel_value,
        order = prototype.order,
        hidden = prototype.hidden,
    }
end

return mapFluid