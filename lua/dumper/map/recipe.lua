local inf = 1e308

--- Maps the prototype to a recipe.
--- @param prototype table
--- @return table|nil
local function mapRecipe(prototype)
    if not prototype.valid or prototype.parameter then
        return nil
    end

    local recipe = {
        name = prototype.name,
        localised_name = prototype.localised_name,
        localised_description = prototype.localised_description,
        category = prototype.category,
        time = prototype.energy,
        ingredients = {},
        products = {},
        surface_conditions = {},
        order = prototype.order,
        enabled = prototype.enabled,
        hidden = prototype.hidden,
    }

    for _, ingredient in pairs(prototype.ingredients) do
        table.insert(recipe.ingredients, {
            type = ingredient.type,
            name = ingredient.name,
            amount = ingredient.amount,
        })
    end

    for _, product in pairs(prototype.products) do
        table.insert(recipe.products, {
            type = product.type,
            name = product.name,
            amount = product.amount,
            amount_min = product.amount_min,
            amount_max = product.amount_max,
            probability = product.probability,
            extra_count_fraction = product.extra_count_fraction,
        })
    end

    if prototype.main_product then
        recipe.main_product = {
            type = prototype.main_product.type,
            name = prototype.main_product.name,
            amount = prototype.main_product.amount,
            amount_min = prototype.main_product.amount_min,
            amount_max = prototype.main_product.amount_max,
            probability = prototype.main_product.probability,
        }
    end

    if prototype.surface_conditions then
        for _, condition in pairs(prototype.surface_conditions) do
            local mapped_condition = {
                property = condition.property
            }
            if condition.min > -inf then
                mapped_condition.min = condition.min
            end
            if condition.max < inf then
                mapped_condition.max = condition.max
            end
            table.insert(recipe.surface_conditions, mapped_condition)
        end
    end

    return recipe
end

return mapRecipe