--- Maps an entity prototype to a machine, including mining drills.
--- @param prototype table The machine prototype to map.
--- @return table|nil The mapped data.
local function mapMachine(prototype)
    if not prototype.valid or prototype.parameter then
        return nil
    end

    -- Only allow the character called "character" and ignore all other pseudo-characters
    if prototype.type == "character" and prototype.name ~= "character" then
        return nil
    end

    local machine = {
        name = prototype.name,
        localised_name = prototype.localised_name,
        localised_description = prototype.localised_description,
        crafting_categories = prototype.crafting_categories,
        resource_categories = prototype.resource_categories,
        crafting_speed = prototype.get_crafting_speed(),
        mining_speed = prototype.mining_speed,
        energy_usage = prototype.energy_usage,
        ingredient_count = prototype.ingredient_count,
        module_inventory_size = prototype.module_inventory_size,
        order = prototype.order,
        hidden = prototype.hidden == true,
    }

    if prototype.burner_prototype then
        machine.fuel_categories = prototype.burner_prototype.fuel_categories
    end

    if prototype.fluidbox_prototypes then
        machine.fluid_boxes = {}
        for _, box in pairs(prototype.fluidbox_prototypes) do
            table.insert(machine.fluid_boxes, {
                production_type = box.production_type,
            })
        end
    end

    if not machine.crafting_categories and not machine.crafting_categories and not machine.fuel_categories then
        -- Machine does not craft, does not mine, and does not burn something. So it is of no interest for now.
        return nil
    end

    return machine
end

return mapMachine