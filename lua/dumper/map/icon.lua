--- Maps an icon from the prototype.
--- @param prototype table The prototype to map.
--- @return table|nil The mapped data, if there was anything to map.
local function mapIcon(prototype)
    if not prototype.icons and type(prototype.icon) ~= "string" then
        return nil
    end

    return {
        type = prototype.type,
        name = prototype.name,
        icon = prototype.icon,
        icons = prototype.icons,
        icon_size = prototype.icon_size,
        icon_mipmaps = prototype.icon_mipmaps,
    }
end

return mapIcon