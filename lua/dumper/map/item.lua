--- Maps the prototype to an item.
--- @param prototype table
--- @return table|nil
local function mapItem(prototype)
    if not prototype.valid or prototype.parameter then
        return nil
    end

    local item = {
        name = prototype.name,
        localised_name = prototype.localised_name,
        localised_description = prototype.localised_description,
        stack_size = prototype.stack_size,
        weight = prototype.weight,
        fuel_category = prototype.fuel_category,
        fuel_value = prototype.fuel_value,
        order = prototype.order,
        hidden = prototype.hidden == true,
    }

    if prototype.burnt_result then
        item.burnt_result_name = prototype.burnt_result.name
    end

    if prototype.spoil_result then
        item.spoil_result_name = prototype.spoil_result.name
        item.spoil_ticks = prototype.get_spoil_ticks()
    end

    if next(prototype.rocket_launch_products) ~= nil then
        item.rocket_launch_products = {}
        for _, product in pairs(prototype.rocket_launch_products) do
            table.insert(item.rocket_launch_products, {
                name = product.name,
                amount = product.amount,
                amount_min = product.amount_min,
                amount_max = product.amount_max,
                probability = product.probability,
            })
        end
    end

    return item
end

return mapItem