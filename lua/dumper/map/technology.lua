--- Maps a technology prototype.
--- @param prototype table The technology prototype to map.
--- @return table|nil The mapped data.
local function mapTechnology(prototype)
    if not prototype.valid or prototype.parameter then
        return nil
    end

    local technology = {
        name = prototype.name,
        localised_name = prototype.localised_name,
        localised_description = prototype.localised_description,
        prerequisites = {},
        research_unit_ingredients = {},
        research_unit_count = prototype.research_unit_count,
        research_unit_count_formula = prototype.research_unit_count_formula,
        research_unit_energy = prototype.research_unit_energy,
        unlocked_recipes = {},
        level = prototype.level,
        max_level = prototype.max_level,
        order = prototype.order,
        hidden = prototype.hidden,
    }

    for name in pairs(prototype.prerequisites) do
        table.insert(technology.prerequisites, name)
    end

    for _, ingredient in pairs(prototype.research_unit_ingredients) do
        table.insert(technology.research_unit_ingredients, {
            name = ingredient.name,
            amount = ingredient.amount,
        })
    end

    for _, effect in pairs(prototype.effects) do
        if effect.type == "unlock-recipe" then
            table.insert(technology.unlocked_recipes, effect.recipe)
        end
    end

    -- We have to already map the trigger because data differs depending on the trigger type.
    if prototype.research_trigger then
        technology.research_trigger = {
            type = prototype.research_trigger.type,
            amount = 1,
        }

        if prototype.research_trigger.type == "build-entity" then
            technology.research_trigger.name = prototype.research_trigger.entity.name
        elseif prototype.research_trigger.type == "capture-spawner" then
            technology.research_trigger.name = prototype.research_trigger.entity
        elseif prototype.research_trigger.type == "craft-fluid" then
            technology.research_trigger.name = prototype.research_trigger.fluid
            technology.research_trigger.amount = prototype.research_trigger.amount
        elseif prototype.research_trigger.type == "craft-item" then
            technology.research_trigger.name = prototype.research_trigger.item
            technology.research_trigger.amount = prototype.research_trigger.count
        elseif prototype.research_trigger.type == "mine-entity" then
            technology.research_trigger.name = prototype.research_trigger.entity
        elseif prototype.research_trigger.type == "send-item-to-orbit" then
            technology.research_trigger.name = prototype.research_trigger.item.name
        end
    end

    return technology
end

return mapTechnology