--- Maps the prototype to a resource.
--- @param prototype table
--- @return table|nil
local function mapResource(prototype)
    if not prototype.valid or prototype.parameter or prototype.type ~= "resource" then
        return nil
    end

    local resource = {
        name = prototype.name,
        localised_name = prototype.localised_name,
        localised_description = prototype.localised_description,
        resource_category = prototype.resource_category,
        mining_time = prototype.mineable_properties.mining_time,
        required_fluid = prototype.mineable_properties.required_fluid,
        fluid_amount = prototype.mineable_properties.fluid_amount,
        order = prototype.order,
        hidden = prototype.hidden,
    }

    if next(prototype.mineable_properties.products) ~= nil then
        resource.products = {}
        for _, product in pairs(prototype.mineable_properties.products) do
            table.insert(resource.products, {
                type = product.type,
                name = product.name,
                amount = product.amount,
                amount_min = product.amount_min,
                amount_max = product.amount_max,
                probability = product.probability,
            })
        end
    end

    return resource
end

return mapResource