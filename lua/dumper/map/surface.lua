--- Maps the prototype to a surface.
--- @param prototype table
--- @param default table The default values for the surface properties.
--- @return table|nil
local function mapSurface(prototype, defaults)
    if not prototype.valid or prototype.parameter then
        return nil
    end

    if not prototype.surface_properties then
        -- The surface is not one we can build on.
        return nil
    end

    local surface = {
        type = prototype.type,
        name = prototype.name,
        localised_name = prototype.localised_name,
        localised_description = prototype.localised_description,
        surface_properties = defaults or {},
        order = prototype.order,
        hidden = prototype.hidden,
    }

    for name, value in pairs(prototype.surface_properties) do
        surface.surface_properties[name] = value
    end

    return surface
end

return mapSurface