local map = {
    fluid        = require("map.fluid"),
    icon         = require("map.icon"),
    item         = require("map.item"),
    machine      = require("map.machine"),
    recipe       = require("map.recipe"),
    resource     = require("map.resource"),
    surface      = require("map.surface"),
    technology   = require("map.technology"),
}

return map