local dump = require("dump")
local map = require("map")

script.on_init(function()
    for _, item in pairs(prototypes.item) do
        dump.write("item", map.item(item))
    end

    for _, fluid in pairs(prototypes.fluid) do
        dump.write("fluid", map.fluid(fluid))
    end

    for _, entity in pairs(prototypes.entity) do
        dump.write("resource", map.resource(entity))
        dump.write("machine", map.machine(entity))
    end

    for _, recipe in pairs(prototypes.recipe) do
        dump.write("recipe", map.recipe(recipe))
    end

    for _, technology in pairs(prototypes.technology) do
        dump.write("technology", map.technology(technology))
    end

    local surface_defaults = {}
    for _, surface_property in pairs(prototypes.surface_property) do
        surface_defaults[surface_property.name] = surface_property.default_value
    end
    for _, spaceLocation in pairs(prototypes.space_location) do
        dump.write("surface", map.surface(spaceLocation, surface_defaults))
    end
    for _, surface in pairs(prototypes.surface) do
         dump.write("surface", map.surface(surface, surface_defaults))
    end
end)