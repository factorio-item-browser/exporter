package step

import (
	"context"
	"log/slog"

	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	"golang.org/x/sync/errgroup"
)

type modDownloader interface {
	DownloadMod(ctx context.Context, modName string) error
}

// DownloadModsStep is the step for downloading all the mods from either the local S3 bucket if the mod version stored
// there is already up-to-date, or from the Mod Portal if it isn't. In the latter case, the mod will get uploaded to
// the local S3 bucket to have the most recent version there.
type DownloadModsStep struct {
	modDownloader modDownloader
	workerCount   int
}

func NewDownloadModsStep(cfg config.Factorio, modDownloader modDownloader) *DownloadModsStep {
	return &DownloadModsStep{
		modDownloader: modDownloader,
		workerCount:   cfg.ModDownloadWorkerCount,
	}
}

func (s *DownloadModsStep) Name() StepName {
	return NameDownloadMods
}

func (s *DownloadModsStep) Run(ctx context.Context) error {
	fi := instance.FromContext(ctx)

	log.FromContext(ctx).Info(
		"downloading mods",
		slog.Int("count", len(fi.ModNames())),
		slog.Int("worker", s.workerCount),
	)

	group, ctx := errgroup.WithContext(ctx)
	group.SetLimit(s.workerCount)

	for _, modName := range fi.ModNames() {
		modName := modName
		group.Go(func() error {
			return s.downloadMod(ctx, modName)
		})
	}

	return group.Wait()
}

func (s *DownloadModsStep) downloadMod(ctx context.Context, modName string) error {
	select {
	case <-ctx.Done():
		return nil // A previous mod already errored, so silently ignore the current one.
	default:
	}

	err := s.modDownloader.DownloadMod(ctx, modName)
	if err != nil {
		return err
	}

	return nil
}

func (s *DownloadModsStep) Event(ctx context.Context) event.Event {
	return createStatusEvent(ctx, s.Name())
}
