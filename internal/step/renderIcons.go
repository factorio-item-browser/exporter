package step

import (
	"context"

	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/icon"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
)

type iconsProvider interface {
	UsedIcons(ctx context.Context) <-chan icon.Icon
}
type iconsRenderer interface {
	RenderIcons(ctx context.Context, icons <-chan icon.Icon, writer icon.ImageWriter)
}

type RenderIconsStep struct {
	iconsProvider iconsProvider
	iconsRenderer iconsRenderer
}

func NewRenderIconsStep(iconsProvider iconsProvider, iconsRenderer iconsRenderer) *RenderIconsStep {
	return &RenderIconsStep{
		iconsProvider: iconsProvider,
		iconsRenderer: iconsRenderer,
	}
}

func (s *RenderIconsStep) Name() StepName {
	return NameRenderIcons
}

func (s *RenderIconsStep) Run(ctx context.Context) error {
	fi := instance.FromContext(ctx)

	stylesheetFile, err := os.FileSystem.Create(fi.Path(instance.StylesheetFileName))
	if err != nil {
		return errors.NewLocalFileSystemError("stylesheet file create error", err)
	}
	cssWriter := icon.NewCssWriter(stylesheetFile)

	icons := s.iconsProvider.UsedIcons(ctx)
	s.iconsRenderer.RenderIcons(ctx, icons, cssWriter)

	return nil
}

func (s *RenderIconsStep) Event(ctx context.Context) event.Event {
	return createStatusEvent(ctx, s.Name())
}
