package step

import (
	"context"

	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/prepare"
)

// PrepareStep is the step for preparing the most basic things for the actual export, in kind-of mini-steps, called
// preparer. Each preparer is doing one small task.
type PrepareStep struct {
	preparers []prepare.Preparer
}

func NewPrepareStep(preparers []prepare.Preparer) *PrepareStep {
	return &PrepareStep{
		preparers: preparers,
	}
}

func (s *PrepareStep) Name() StepName {
	return NamePrepare
}

func (s *PrepareStep) Run(ctx context.Context) error {
	for _, preparer := range s.preparers {
		err := preparer.Prepare(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *PrepareStep) Event(ctx context.Context) event.Event {
	return createStatusEvent(ctx, s.Name())
}
