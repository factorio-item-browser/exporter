package step

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/exporter.git/internal/step/mocks"
)

func TestNewCleanStep(t *testing.T) {
	instance := NewCleanStep()

	assert.NotNil(t, instance)
}

func TestCleanStep_Name(t *testing.T) {
	expectedResult := NameClean

	instance := CleanStep{}
	result := instance.Name()

	assert.Equal(t, expectedResult, result)
}

func TestCleanStep_Run(t *testing.T) {
	tests := map[string]struct {
		removeError       error
		expectedErrorType error
	}{
		"happy path": {
			removeError: nil,
		},
		"with error": {
			removeError: fmt.Errorf("test error"),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

			fs := mocks.NewFs(t)
			fs.On("RemoveAll", "/root/instances/test").Once().Return(test.removeError)
			os.FileSystem = fs

			instance := CleanStep{}
			err := instance.Run(ctx)

			assert.Nil(t, err)
		})
	}
}

func TestCleanStep_Event(t *testing.T) {
	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)
	ctx = dump.NewMetaDataContext(ctx)

	metaData := dump.MetaDataFromContext(ctx)
	metaData.ModVersions = map[string]string{
		"abc": "1.2.3",
		"def": "4.5.6",
	}
	metaData.Numbers = map[string]uint64{
		"foo": 12,
		"bar": 23,
	}
	metaData.Files = map[string]dump.FileMetaData{
		"oof": {
			Size:           34,
			CompressedSize: 45,
		},
		"rab": {
			Size:           56,
			CompressedSize: 67,
		},
	}

	expectedEvent := event.SuccessEvent{
		CombinationID: "test",
		Mods: map[string]string{
			"abc": "1.2.3",
			"def": "4.5.6",
		},
		Numbers: map[string]uint64{
			"foo": 12,
			"bar": 23,
		},
		Files: map[string]event.SuccessEventFile{
			"oof": {
				Size:           34,
				CompressedSize: 45,
			},
			"rab": {
				Size:           56,
				CompressedSize: 67,
			},
		},
	}

	instance := CleanStep{}
	result := instance.Event(ctx)

	assert.IsType(t, event.SuccessEvent{}, result)

	successEvent := result.(event.SuccessEvent)
	assert.NotEmpty(t, successEvent.Timestamp)
	successEvent.Timestamp = ""
	assert.Equal(t, expectedEvent, successEvent)
}
