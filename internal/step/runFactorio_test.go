package step

import (
	"context"
	"fmt"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/exporter.git/internal/step/mocks"
)

func TestNewRunFactorioStep(t *testing.T) {
	runner := mocks.NewRunner(t)

	instance := NewRunFactorioStep(runner)

	assert.NotNil(t, instance)
	assert.Same(t, runner, instance.runner)
}

func TestRunFactorioStep_Name(t *testing.T) {
	expectedResult := NameRunFactorio

	instance := RunFactorioStep{}
	result := instance.Name()

	assert.Equal(t, expectedResult, result)
}

func TestRunFactorioStep_Run(t *testing.T) {
	exitCode := 42

	tests := map[string]struct {
		createError       error
		expectRunFactorio bool
		expectedErrorType error
		expectedExitCode  int
	}{
		"happy path": {
			createError:       nil,
			expectRunFactorio: true,
			expectedErrorType: nil,
			expectedExitCode:  exitCode,
		},
		"create error": {
			createError:       fmt.Errorf("test error"),
			expectRunFactorio: false,
			expectedErrorType: &errors.LocalFileSystemError{},
			expectedExitCode:  0,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 afero.Fs) {
				os.FileSystem = ref1
			}(os.FileSystem)

			ctx := context.Background()
			ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)
			ctx = dump.NewMetaDataContext(ctx)

			os.FileSystem = afero.NewMemMapFs()
			if test.createError != nil {
				fs := mocks.NewFs(t)
				fs.On("Create", "/root/instances/test/output.txt").Once().Return(nil, test.createError)
				os.FileSystem = fs
			}

			runner := mocks.NewRunner(t)
			if test.expectRunFactorio {
				runner.On("Run", ctx, mock.Anything).Once().Return(exitCode)
			}

			instance := RunFactorioStep{
				runner: runner,
			}

			err := instance.Run(ctx)
			meta := dump.MetaDataFromContext(ctx)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedExitCode, meta.ExitCode)
		})
	}
}

func TestRunFactorioStep_Event(t *testing.T) {
	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

	instance := RunFactorioStep{}
	result := instance.Event(ctx)

	assert.IsType(t, event.StatusEvent{}, result)
	assert.Equal(t, "test", result.(event.StatusEvent).CombinationID)
	assert.Equal(t, "run-factorio", result.(event.StatusEvent).Status)
	assert.NotEmpty(t, result.(event.StatusEvent).Timestamp)
}
