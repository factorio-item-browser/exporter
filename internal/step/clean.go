package step

import (
	"context"

	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
)

// CleanStep is the step to clean everything up after the export has finished. This is the last step of the process,
// and its ending event marks the end of the export.
type CleanStep struct {
}

func NewCleanStep() *CleanStep {
	return &CleanStep{}
}

func (s *CleanStep) Name() StepName {
	return NameClean
}

func (s *CleanStep) Run(ctx context.Context) error {
	fi := instance.FromContext(ctx)

	_ = os.FileSystem.RemoveAll(fi.Path(instance.Directory))
	return nil
}

func (s *CleanStep) Event(ctx context.Context) event.Event {
	fi := instance.FromContext(ctx)
	metaData := dump.MetaDataFromContext(ctx)

	files := make(map[string]event.SuccessEventFile, len(metaData.Files))
	for name, file := range metaData.Files {
		files[name] = event.SuccessEventFile{
			Size:           file.Size,
			CompressedSize: file.CompressedSize,
		}
	}

	return event.SuccessEvent{
		CombinationID: fi.CombinationId(),
		Timestamp:     currentTimestamp(),
		Mods:          metaData.ModVersions,
		Numbers:       metaData.Numbers,
		Files:         files,
	}
}
