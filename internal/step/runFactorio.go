package step

import (
	"context"
	"io"
	"log/slog"

	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
)

type runner interface {
	Run(ctx context.Context, writer io.Writer) int
}

// RunFactorioStep is the step which actually executes Factorio. This step simply runs the executable, everything else
// must be already set up. This step does not check for any errors, this must be done afterward by the data processors.
type RunFactorioStep struct {
	runner runner
}

func NewRunFactorioStep(runner runner) *RunFactorioStep {
	return &RunFactorioStep{
		runner: runner,
	}
}

func (s *RunFactorioStep) Name() StepName {
	return NameRunFactorio
}

func (s *RunFactorioStep) Run(ctx context.Context) error {
	fi := instance.FromContext(ctx)

	log.FromContext(ctx).Info("execute factorio", slog.String("directory", fi.Path(instance.FactorioDirectory)))

	outputFile, err := os.FileSystem.Create(fi.Path(instance.FactorioOutputFileName))
	if err != nil {
		return errors.NewLocalFileSystemError("unable to create output file", err)
	}
	defer func() {
		_ = outputFile.Close()
	}()

	// The actual error will be processed later when reading in the output file. For now, we only store the exit code.
	exitCode := s.runner.Run(ctx, outputFile)
	dump.MetaDataFromContext(ctx).SetExitCode(exitCode)
	return nil
}

func (s *RunFactorioStep) Event(ctx context.Context) event.Event {
	return createStatusEvent(ctx, s.Name())
}
