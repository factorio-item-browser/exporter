package step

import (
	"context"
	"log/slog"

	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
)

type ProcessDataStep struct {
	processors []dump.Processor
}

func NewProcessStep(processors []dump.Processor) *ProcessDataStep {
	return &ProcessDataStep{
		processors: processors,
	}
}

func (s *ProcessDataStep) Name() StepName {
	return NameProcessData
}

func (s *ProcessDataStep) Run(ctx context.Context) error {
	fi := instance.FromContext(ctx)
	fi.ResolveMods()

	inputFile, err := os.FileSystem.Open(fi.Path(instance.FactorioOutputFileName))
	if err != nil {
		return errors.NewLocalFileSystemError("open factorio output file error", err)
	}
	defer func() {
		_ = inputFile.Close()
	}()

	outputFile, err := os.FileSystem.Create(fi.Path(instance.DataOutputFileName))
	if err != nil {
		return errors.NewLocalFileSystemError("create data output file error", err)
	}
	defer func() {
		_ = outputFile.Close()
	}()

	dataWriter := dump.NewJsonLinesWriter(outputFile)
	dumpReader := dump.NewLineReader(inputFile)
	for line := range dumpReader.Read() {
		for _, processor := range s.processors {
			data := processor.Process(ctx, line)
			if data != nil {
				err = dataWriter.WriteData(data)
				if err != nil {
					log.FromContext(ctx).Error("data write error", slog.String("error", err.Error()))
				}
			}
		}
	}

	for _, processor := range s.processors {
		err = processor.Validate(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *ProcessDataStep) Event(ctx context.Context) event.Event {
	return createStatusEvent(ctx, s.Name())
}
