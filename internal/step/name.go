package step

type StepName string

const (
	NameClean        StepName = "clean"
	NameDownloadMods StepName = "download-mods"
	NamePrepare      StepName = "prepare"
	NameProcessData  StepName = "process-data"
	NameRenderIcons  StepName = "render-icons"
	NameRunFactorio  StepName = "run-factorio"
	NameUpload       StepName = "upload"
)
