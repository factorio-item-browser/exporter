package step

import (
	"context"

	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	"golang.org/x/sync/errgroup"
)

type uploader interface {
	UploadData(ctx context.Context, combinationId string, sourceFileName string) (dump.FileMetaData, error)
	UploadStylesheet(ctx context.Context, combinationId string, sourceFileName string) (dump.FileMetaData, error)
	UploadOutput(ctx context.Context, combinationId string, sourceFileName string) (dump.FileMetaData, error)
}

type UploadStep struct {
	uploader uploader
}

func NewUploadStep(uploader uploader) *UploadStep {
	return &UploadStep{
		uploader: uploader,
	}
}

func (s *UploadStep) Name() StepName {
	return NameUpload
}

func (s *UploadStep) Run(ctx context.Context) error {
	fi := instance.FromContext(ctx)
	metaData := dump.MetaDataFromContext(ctx)
	logger := log.FromContext(ctx)

	group, ctx := errgroup.WithContext(ctx)
	group.Go(func() error {
		logger.Info("upload data file")

		data, err := s.uploader.UploadData(ctx, fi.CombinationId(), fi.Path(instance.DataOutputFileName))
		metaData.AddFile("data", data)

		return err
	})
	group.Go(func() error {
		logger.Info("upload stylesheet file")

		data, err := s.uploader.UploadStylesheet(ctx, fi.CombinationId(), fi.Path(instance.StylesheetFileName))
		metaData.AddFile("stylesheet", data)

		return err
	})
	group.Go(func() error {
		logger.Info("upload Factorio output file")

		data, err := s.uploader.UploadOutput(ctx, fi.CombinationId(), fi.Path(instance.FactorioOutputFileName))
		metaData.AddFile("output", data)

		return err
	})
	return group.Wait()
}

func (s *UploadStep) Event(ctx context.Context) event.Event {
	return createStatusEvent(ctx, s.Name())
}
