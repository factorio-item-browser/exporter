package step

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/prepare"
	"gitlab.com/factorio-item-browser/exporter.git/internal/step/mocks"
)

func TestNewPrepareStep(t *testing.T) {
	preparers := []prepare.Preparer{
		mocks.NewPreparer(t),
		mocks.NewPreparer(t),
	}

	instance := NewPrepareStep(preparers)

	assert.NotNil(t, instance)
	assert.Equal(t, preparers, instance.preparers)
}

func TestPrepareStep_Name(t *testing.T) {
	expectedResult := NamePrepare

	instance := PrepareStep{}
	result := instance.Name()

	assert.Equal(t, expectedResult, result)
}

func TestPrepareStep_Run(t *testing.T) {
	ctx := context.Background()

	preparer1 := mocks.NewPreparer(t)
	preparer1.On("Prepare", ctx).Once().Return(nil)

	preparer2 := mocks.NewPreparer(t)
	preparer2.On("Prepare", ctx).Once().Return(nil)

	instance := PrepareStep{
		preparers: []prepare.Preparer{
			preparer1,
			preparer2,
		},
	}
	err := instance.Run(ctx)

	assert.Nil(t, err)
}

func TestPrepareStep_Run_withError(t *testing.T) {
	ctx := context.Background()
	prepareError := fmt.Errorf("test error")

	preparer1 := mocks.NewPreparer(t)
	preparer1.On("Prepare", ctx).Once().Return(prepareError)

	preparer2 := mocks.NewPreparer(t)

	instance := PrepareStep{
		preparers: []prepare.Preparer{
			preparer1,
			preparer2,
		},
	}
	err := instance.Run(ctx)

	assert.Equal(t, prepareError, err)
}

func TestPrepareStep_Event(t *testing.T) {
	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

	instance := PrepareStep{}
	result := instance.Event(ctx)

	assert.IsType(t, event.StatusEvent{}, result)
	assert.Equal(t, "test", result.(event.StatusEvent).CombinationID)
	assert.Equal(t, "prepare", result.(event.StatusEvent).Status)
	assert.NotEmpty(t, result.(event.StatusEvent).Timestamp)
}
