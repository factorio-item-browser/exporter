package step

import (
	"context"
	"time"

	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
)

// Step is the ability to be a step in the export process.
type Step interface {
	// Name returns the name of the step.
	Name() StepName
	// Run actually executes the step.
	Run(ctx context.Context) error
	// Event returns an event to mark the completion of the step.
	Event(ctx context.Context) event.Event
}

// createStatusEvent creates a status event representing the provided step.
func createStatusEvent(ctx context.Context, step StepName) event.StatusEvent {
	fi := instance.FromContext(ctx)

	return event.StatusEvent{
		CombinationID: fi.CombinationId(),
		Status:        string(step),
		Timestamp:     currentTimestamp(),
	}
}

// currentTimestamp returns the current timestamp, formatted for the events.
func currentTimestamp() string {
	return time.Now().UTC().Truncate(time.Millisecond).Format(time.RFC3339Nano)
}
