package step

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/step/mocks"
)

func TestNewUploadStep(t *testing.T) {
	uploader := mocks.NewUploader(t)

	instance := NewUploadStep(uploader)

	assert.NotNil(t, instance)
	assert.Same(t, uploader, instance.uploader)
}

func TestUploadStep_Name(t *testing.T) {
	expectedResult := NameUpload

	instance := UploadStep{}
	result := instance.Name()

	assert.Equal(t, expectedResult, result)
}

func TestUploadStep_Run(t *testing.T) {
	tests := map[string]struct {
		uploadError       error
		expectedErrorType error
		expectMetaData    bool
	}{
		"happy path": {
			uploadError:       nil,
			expectedErrorType: nil,
			expectMetaData:    true,
		},
		"with error": {
			uploadError:       &errors.StorageError{},
			expectedErrorType: &errors.StorageError{},
			expectMetaData:    false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)
			ctx = dump.NewMetaDataContext(ctx)

			metaData := dump.MetaDataFromContext(ctx)

			fileData := dump.FileMetaData{
				Size:           12,
				CompressedSize: 23,
			}
			fileOutput := dump.FileMetaData{
				Size:           34,
				CompressedSize: 45,
			}
			fileStylesheet := dump.FileMetaData{
				Size:           56,
				CompressedSize: 67,
			}

			expectedMetaData := dump.MetaData{
				ModVersions: make(map[string]string),
				Numbers:     make(map[string]uint64),
				Files: map[string]dump.FileMetaData{
					"data":       fileData,
					"output":     fileOutput,
					"stylesheet": fileStylesheet,
				},
			}

			uploader := mocks.NewUploader(t)
			uploader.
				On("UploadData", mock.Anything, "test", "/root/instances/test/data.jsonl").
				Once().
				Return(fileData, test.uploadError)
			uploader.
				On("UploadStylesheet", mock.Anything, "test", "/root/instances/test/stylesheet.css").
				Once().
				Return(fileStylesheet, test.uploadError)
			uploader.
				On("UploadOutput", mock.Anything, "test", "/root/instances/test/output.txt").
				Once().
				Return(fileOutput, test.uploadError)

			instance := UploadStep{
				uploader: uploader,
			}
			err := instance.Run(ctx)

			assert.IsType(t, test.expectedErrorType, err)
			if test.expectMetaData {
				assert.Equal(t, &expectedMetaData, metaData)
			}
		})
	}
}

func TestUploadStep_Event(t *testing.T) {
	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

	instance := UploadStep{}
	result := instance.Event(ctx)

	assert.IsType(t, event.StatusEvent{}, result)
	assert.Equal(t, "test", result.(event.StatusEvent).CombinationID)
	assert.Equal(t, "upload", result.(event.StatusEvent).Status)
	assert.NotEmpty(t, result.(event.StatusEvent).Timestamp)
}
