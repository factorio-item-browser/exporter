package step

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/step/mocks"
)

func TestNewModsStep(t *testing.T) {
	workerCount := 42
	cfg := config.Factorio{
		ModDownloadWorkerCount: workerCount,
	}

	modDownloader := mocks.NewModDownloader(t)

	instance := NewDownloadModsStep(cfg, modDownloader)

	assert.NotNil(t, instance)
	assert.Same(t, modDownloader, instance.modDownloader)
	assert.Equal(t, workerCount, instance.workerCount)
}

func TestDownloadModsStep_Name(t *testing.T) {
	expectedResult := NameDownloadMods

	instance := DownloadModsStep{}
	result := instance.Name()

	assert.Equal(t, expectedResult, result)
}

func TestDownloadModsStep_Run(t *testing.T) {
	instanceBuilder := instance2.NewMockedInstanceBuilder()
	instanceBuilder.WithMod("foo")
	instanceBuilder.WithMod("bar")
	instanceBuilder.WithMod("baz")

	ctx := context.Background()
	ctx = instanceBuilder.BuildContext(ctx)

	modDownloader := mocks.NewModDownloader(t)
	modDownloader.On("DownloadMod", mock.Anything, "foo").Once().Return(nil)
	modDownloader.On("DownloadMod", mock.Anything, "bar").Once().Return(nil)
	modDownloader.On("DownloadMod", mock.Anything, "baz").Once().Return(nil)

	instance := DownloadModsStep{
		modDownloader: modDownloader,
		workerCount:   16,
	}
	err := instance.Run(ctx)

	assert.Nil(t, err)
}

func TestDownloadModsStep_Run_withError(t *testing.T) {
	instanceBuilder := instance2.NewMockedInstanceBuilder()
	instanceBuilder.WithMod("foo")
	instanceBuilder.WithMod("bar")
	instanceBuilder.WithMod("baz")

	ctx := context.Background()
	ctx = instanceBuilder.BuildContext(ctx)

	modDownloader := mocks.NewModDownloader(t)
	modDownloader.On("DownloadMod", mock.Anything, "foo").Once().Return(nil)
	modDownloader.On("DownloadMod", mock.Anything, "bar").Once().Return(fmt.Errorf("test error"))

	instance := DownloadModsStep{
		modDownloader: modDownloader,
		workerCount:   1,
	}
	err := instance.Run(ctx)

	assert.IsType(t, fmt.Errorf("test error"), err)
}

func TestDownloadModsStep_Event(t *testing.T) {
	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

	instance := DownloadModsStep{}
	result := instance.Event(ctx)

	assert.IsType(t, event.StatusEvent{}, result)
	assert.Equal(t, "test", result.(event.StatusEvent).CombinationID)
	assert.Equal(t, "download-mods", result.(event.StatusEvent).Status)
	assert.NotEmpty(t, result.(event.StatusEvent).Timestamp)
}
