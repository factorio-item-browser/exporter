package step

import (
	"context"
	"fmt"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/icon"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/exporter.git/internal/step/mocks"
)

func TestNewRenderIconsStep(t *testing.T) {
	iconsProvider := mocks.NewIconsProvider(t)
	iconsRenderer := mocks.NewIconsRenderer(t)

	instance := NewRenderIconsStep(iconsProvider, iconsRenderer)

	assert.NotNil(t, instance)
	assert.Same(t, iconsProvider, instance.iconsProvider)
	assert.Same(t, iconsRenderer, instance.iconsRenderer)
}

func TestRenderIconsStep_Name(t *testing.T) {
	expectedResult := NameRenderIcons

	instance := RenderIconsStep{}
	result := instance.Name()

	assert.Equal(t, expectedResult, result)
}

func TestRenderIconsStep_Run(t *testing.T) {
	defer func(ref1 afero.Fs) {
		os.FileSystem = ref1
	}(os.FileSystem)
	os.FileSystem = afero.NewMemMapFs()

	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

	icons := make(<-chan icon.Icon)

	iconsProvider := mocks.NewIconsProvider(t)
	iconsProvider.On("UsedIcons", ctx).Once().Return(icons)

	iconsRenderer := mocks.NewIconsRenderer(t)
	iconsRenderer.On("RenderIcons", ctx, icons, mock.IsType(&icon.CssWriter{})).Once()

	instance := RenderIconsStep{
		iconsProvider: iconsProvider,
		iconsRenderer: iconsRenderer,
	}
	err := instance.Run(ctx)

	assert.Nil(t, err)

	exists, _ := afero.Exists(os.FileSystem, "/root/instances/test/stylesheet.css")
	assert.True(t, exists)
}

func TestRenderIconsStep_Run_withFileError(t *testing.T) {
	defer func(ref1 afero.Fs) {
		os.FileSystem = ref1
	}(os.FileSystem)

	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

	fs := mocks.NewFs(t)
	fs.On("Create", "/root/instances/test/stylesheet.css").Once().Return(nil, fmt.Errorf("test error"))
	os.FileSystem = fs

	instance := RenderIconsStep{}
	err := instance.Run(ctx)

	assert.IsType(t, &errors.LocalFileSystemError{}, err)
}

func TestRenderIconsStep_Event(t *testing.T) {
	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

	instance := RenderIconsStep{}
	result := instance.Event(ctx)

	assert.IsType(t, event.StatusEvent{}, result)
	assert.Equal(t, "test", result.(event.StatusEvent).CombinationID)
	assert.Equal(t, "render-icons", result.(event.StatusEvent).Status)
	assert.NotEmpty(t, result.(event.StatusEvent).Timestamp)
}
