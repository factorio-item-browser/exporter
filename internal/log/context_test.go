package log

import (
	"context"
	"log/slog"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
)

func TestNewContext(t *testing.T) {
	cfg := config.Logger{
		Format: "json",
	}
	logger := New(cfg).With(slog.String("foo", "bar"))

	ctx := context.Background()
	ctx2 := NewContext(ctx, logger)
	assert.Same(t, logger, FromContext(ctx2))

	// Default logger when not explicitly setting it
	assert.NotNil(t, FromContext(ctx))
}
