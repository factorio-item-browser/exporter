package log

import (
	"context"
	"log/slog"
)

// NewMiddleware creates a new middleware which injects the provided logger into the context of the handler.
func NewMiddleware[T any](
	logger *slog.Logger,
	handler func(ctx context.Context, payload T) error,
) func(ctx context.Context, event T) error {
	return func(ctx context.Context, event T) error {
		ctx = NewContext(ctx, logger)

		return handler(ctx, event)
	}
}
