package log

import (
	"log/slog"
	"os"

	"github.com/lmittmann/tint"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
)

// New creates a new logger instance using the provided format for its output.
func New(cfg config.Logger) *slog.Logger {
	var level slog.Level
	err := level.UnmarshalText([]byte(cfg.Level))
	if err != nil {
		level = slog.LevelInfo
	}

	var handler slog.Handler
	switch cfg.Format {
	case "text":
		handler = tint.NewHandler(os.Stdout, &tint.Options{Level: level})
	default:
		handler = slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: level})
	}

	return slog.New(handler)
}
