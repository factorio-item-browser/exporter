package log

import (
	"context"
	"log/slog"
	"os"
	"testing"

	"github.com/lmittmann/tint"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
)

func TestNew(t *testing.T) {
	tests := map[string]struct {
		format              string
		level               string
		expectedHandler     slog.Handler
		expectedInfoEnabled bool
	}{
		"json": {
			format:              "json",
			level:               "info",
			expectedHandler:     &slog.JSONHandler{},
			expectedInfoEnabled: true,
		},
		"text": {
			format:              "text",
			level:               "info",
			expectedHandler:     tint.NewHandler(os.Stdout, nil),
			expectedInfoEnabled: true,
		},
		"unknown": {
			format:              "unknown",
			level:               "info",
			expectedHandler:     &slog.JSONHandler{},
			expectedInfoEnabled: true,
		},
		"level warn": {
			format:              "json",
			level:               "warn",
			expectedHandler:     &slog.JSONHandler{},
			expectedInfoEnabled: false,
		},
		"level unknown": {
			format:              "json",
			level:               "unknown",
			expectedHandler:     &slog.JSONHandler{},
			expectedInfoEnabled: true,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			cfg := config.Logger{
				Format: test.format,
				Level:  test.level,
			}
			result := New(cfg)

			assert.NotNil(t, result)
			assert.IsType(t, test.expectedHandler, result.Handler())

			assert.True(t, result.Enabled(context.Background(), slog.LevelError))
			assert.Equal(t, test.expectedInfoEnabled, result.Enabled(context.Background(), slog.LevelInfo))
		})
	}
}
