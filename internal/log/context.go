package log

import (
	"context"
	"log/slog"

	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
)

type contextKey string

var keyLogger contextKey = "logger"

// NewContext creates a new context with the provided logger.
func NewContext(ctx context.Context, logger *slog.Logger) context.Context {
	return context.WithValue(ctx, keyLogger, logger)
}

// FromContext returns the logger from the provided context. If no logger has been provided, the default logger is
// returned.
func FromContext(ctx context.Context) *slog.Logger {
	if logger, ok := ctx.Value(keyLogger).(*slog.Logger); ok {
		return logger
	}

	return New(config.Logger{})
}

// NewContextWithValues creates a new context, adding the provided args to the logger already present in the provided
// context.
func NewContextWithValues(ctx context.Context, args ...any) context.Context {
	logger := FromContext(ctx)
	logger = logger.With(args...)

	return NewContext(ctx, logger)
}
