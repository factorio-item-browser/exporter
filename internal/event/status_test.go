package event

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStatusEvent_Type(t *testing.T) {
	instance := StatusEvent{}
	result := instance.Type()

	assert.Equal(t, "status", result)
}

func TestStatusEvent_Description(t *testing.T) {
	instance := StatusEvent{}
	result := instance.Description()

	assert.NotEmpty(t, result)
}
