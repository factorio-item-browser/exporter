package event

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSuccessEvent_Type(t *testing.T) {
	instance := SuccessEvent{}
	result := instance.Type()

	assert.Equal(t, "success", result)
}

func TestSuccessEvent_Description(t *testing.T) {
	instance := SuccessEvent{}
	result := instance.Description()

	assert.NotEmpty(t, result)
}
