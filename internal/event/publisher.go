package event

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/eventbridge"
	"github.com/aws/aws-sdk-go-v2/service/eventbridge/types"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
)

// Event is an event which can be published on the EventBridge.
type Event interface {
	Type() string
	Description() string
}

type bridge interface {
	PutEvents(ctx context.Context, params *eventbridge.PutEventsInput, optFns ...func(*eventbridge.Options)) (*eventbridge.PutEventsOutput, error)
}

// Publisher is able to publish any Event.
type Publisher struct {
	bridge       bridge
	sourceFormat string
}

func NewPublisher(cfg config.EventPublisher, awsConfig aws.Config) *Publisher {
	return &Publisher{
		bridge:       eventbridge.NewFromConfig(awsConfig),
		sourceFormat: cfg.SourceFormat,
	}
}

func (p *Publisher) PublishEvent(ctx context.Context, event Event) error {
	payload, _ := json.Marshal(event)

	input := eventbridge.PutEventsInput{
		Entries: []types.PutEventsRequestEntry{
			{
				Source:     aws.String(fmt.Sprintf(p.sourceFormat, event.Type())),
				Detail:     aws.String(string(payload)),
				DetailType: aws.String(event.Description()),
			},
		},
	}
	output, err := p.bridge.PutEvents(ctx, &input)
	if err != nil {
		return errors.NewPublishEventError(err)
	}
	if output.FailedEntryCount > 0 && len(output.Entries) > 0 && output.Entries[0].ErrorCode != nil &&
		output.Entries[0].ErrorMessage != nil {
		err = fmt.Errorf("[%s] %s", *output.Entries[0].ErrorCode, *output.Entries[0].ErrorMessage)
		return errors.NewPublishEventError(err)
	}

	return nil
}
