package event

import (
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
)

// ErrorEvent is the payload of the event when an error occurred during the export.
type ErrorEvent struct {
	CombinationID string           `json:"combinationID"`
	ErrorType     errors.ErrorType `json:"errorType"`
	ErrorMessage  string           `json:"errorMessage"`
	Timestamp     string           `json:"timestamp"`
}

func (e ErrorEvent) Type() string {
	return "error"
}

func (e ErrorEvent) Description() string {
	return "Announcing an error to the export of a combination."
}
