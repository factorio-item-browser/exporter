package event

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/eventbridge"
	"github.com/aws/aws-sdk-go-v2/service/eventbridge/types"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event/mocks"
)

func TestNewPublisher(t *testing.T) {
	sourceFormat := "fancy-format"

	instance := NewPublisher(config.EventPublisher{SourceFormat: sourceFormat}, aws.Config{})

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.bridge)
	assert.Equal(t, sourceFormat, instance.sourceFormat)
}

type testEvent struct {
	Foo string `json:"foo"`
}

func (e testEvent) Type() string {
	return "test-type"
}

func (e testEvent) Description() string {
	return "test-description"
}

func TestPublisher_PublishEvent(t *testing.T) {
	tests := map[string]struct {
		putResult         *eventbridge.PutEventsOutput
		putError          error
		expectedErrorType error
	}{
		"happy path": {
			putResult:         &eventbridge.PutEventsOutput{},
			putError:          nil,
			expectedErrorType: nil,
		},
		"failed event": {
			putResult: &eventbridge.PutEventsOutput{
				Entries: []types.PutEventsResultEntry{
					{
						ErrorCode:    aws.String("foo"),
						ErrorMessage: aws.String("bar"),
					},
				},
				FailedEntryCount: 1,
			},
			putError:          nil,
			expectedErrorType: &errors.PublishEventError{},
		},
		"put error": {
			putResult:         nil,
			putError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.PublishEventError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			event := testEvent{
				Foo: "bar",
			}

			expectedInput := eventbridge.PutEventsInput{
				Entries: []types.PutEventsRequestEntry{
					{
						Source:     aws.String("fancy-test-type-format"),
						Detail:     aws.String(`{"foo":"bar"}`),
						DetailType: aws.String("test-description"),
					},
				},
			}

			bridge := mocks.NewBridge(t)
			bridge.On("PutEvents", ctx, &expectedInput).Once().Return(test.putResult, test.putError)

			instance := Publisher{
				bridge:       bridge,
				sourceFormat: "fancy-%s-format",
			}
			err := instance.PublishEvent(ctx, event)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}
