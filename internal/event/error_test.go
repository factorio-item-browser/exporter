package event

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestErrorEvent_Type(t *testing.T) {
	instance := ErrorEvent{}
	result := instance.Type()

	assert.Equal(t, "error", result)
}

func TestErrorEvent_Description(t *testing.T) {
	instance := ErrorEvent{}
	result := instance.Description()

	assert.NotEmpty(t, result)
}
