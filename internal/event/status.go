package event

// StatusEvent is the payload of the event signaling a status update for the export, i.e. finishing a step.
type StatusEvent struct {
	CombinationID string `json:"combinationID"`
	Status        string `json:"status"`
	Timestamp     string `json:"timestamp"`
}

func (e StatusEvent) Type() string {
	return "status"
}

func (e StatusEvent) Description() string {
	return "Announcing a status update to the export of a combination."
}
