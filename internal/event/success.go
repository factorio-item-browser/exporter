package event

// SuccessEvent is the payload of the event when an export has been successfully finished, providing all the meta data
// about it.
type SuccessEvent struct {
	CombinationID string                      `json:"combinationID"`
	Timestamp     string                      `json:"timestamp"`
	Mods          map[string]string           `json:"mods"`
	Numbers       map[string]uint64           `json:"numbers"`
	Files         map[string]SuccessEventFile `json:"files"`
}

type SuccessEventFile struct {
	Size           uint64 `json:"size"`
	CompressedSize uint64 `json:"compressedSize"`
}

func (e SuccessEvent) Type() string {
	return "success"
}

func (e SuccessEvent) Description() string {
	return "Announcing the success of the combination export."
}
