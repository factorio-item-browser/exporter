package middleware

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"testing"
)

type payload struct {
	Foo string `json:"foo"`
}

func TestExtractSqsMessage(t *testing.T) {
	tests := map[string]struct {
		event             events.SQSEvent
		expectHandlerCall bool
		expectedPayload   payload
		handlerError      error
		expectedErrorType error
	}{
		"happy path": {
			event: events.SQSEvent{
				Records: []events.SQSMessage{
					{
						Body: `{"foo": "bar"}`,
					},
				},
			},
			expectHandlerCall: true,
			expectedPayload: payload{
				Foo: "bar",
			},
			handlerError:      nil,
			expectedErrorType: nil,
		},
		"handler error": {
			event: events.SQSEvent{
				Records: []events.SQSMessage{
					{
						Body: `{"foo": "bar"}`,
					},
				},
			},
			expectHandlerCall: true,
			expectedPayload: payload{
				Foo: "bar",
			},
			handlerError:      fmt.Errorf("test error"),
			expectedErrorType: fmt.Errorf("test error"),
		},
		"empty event": {
			event: events.SQSEvent{
				Records: []events.SQSMessage{},
			},
			expectHandlerCall: false,
			expectedPayload:   payload{},
			handlerError:      nil,
			expectedErrorType: nil,
		},
		"invalid payload": {
			event: events.SQSEvent{
				Records: []events.SQSMessage{
					{
						Body: `{"invalid`,
					},
				},
			},
			expectHandlerCall: false,
			expectedPayload:   payload{},
			handlerError:      nil,
			expectedErrorType: &json.SyntaxError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			handlerCalled := false
			handler := func(c context.Context, p payload) error {
				if !test.expectHandlerCall {
					assert.Fail(t, "handler was not expected to be called")
					return nil
				}

				handlerCalled = true
				assert.Equal(t, ctx, c)
				assert.Equal(t, test.expectedPayload, p)

				return test.handlerError
			}

			instance := ExtractSqsMessage(handler)
			err := instance(ctx, test.event)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectHandlerCall, handlerCalled)
		})
	}
}
