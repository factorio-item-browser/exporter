package middleware

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
)

// ExtractSqsMessage will extract the message from the first record of the incoming SQS event, and pass it to the
// provided handler. If the incoming SQS event has multiple records, only the first will be processed.
func ExtractSqsMessage[T any](
	handler func(ctx context.Context, payload T) error,
) func(ctx context.Context, event events.SQSEvent) error {
	return func(ctx context.Context, event events.SQSEvent) error {
		if len(event.Records) == 0 {
			return nil
		}

		var payload T
		err := json.Unmarshal([]byte(event.Records[0].Body), &payload)
		if err != nil {
			return err
		}

		return handler(ctx, payload)
	}
}
