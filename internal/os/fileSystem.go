package os

import "github.com/spf13/afero"

var FileSystem = afero.NewOsFs()
