package command

import (
	"context"
	"log/slog"
	"strings"
	"time"

	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	"gitlab.com/factorio-item-browser/exporter.git/internal/step"
)

type eventPublisher interface {
	PublishEvent(ctx context.Context, event event.Event) error
}

type ProcessCommandParams struct {
	CombinationId string   `json:"combinationId"`
	ModNames      []string `json:"modNames"`
}

type ProcessCommand struct {
	eventPublisher  eventPublisher
	instanceCreator instance.Creator
	steps           []step.Step
}

func NewProcessCommand(
	eventPublisher eventPublisher,
	instanceCreator instance.Creator,
	steps []step.Step,
) *ProcessCommand {
	return &ProcessCommand{
		eventPublisher:  eventPublisher,
		instanceCreator: instanceCreator,
		steps:           steps,
	}
}

func (c *ProcessCommand) Run(ctx context.Context, params ProcessCommandParams) error {
	ctx = log.NewContextWithValues(ctx, slog.String("combination", params.CombinationId))

	logger := log.FromContext(ctx)
	logger.Info("request", slog.String("mods", strings.Join(params.ModNames, ",")))

	if params.CombinationId == "" || len(params.ModNames) == 0 {
		logger.Error("missing parameters")
		return nil
	}

	ctx = instance.NewContext(ctx, c.instanceCreator(params.CombinationId, params.ModNames))
	ctx = dump.NewMetaDataContext(ctx)

	for _, s := range c.steps {
		err := c.runStep(ctx, s)
		if err != nil {
			c.publishError(ctx, err)
			return err
		}
	}

	return nil
}

func (c *ProcessCommand) runStep(ctx context.Context, step step.Step) error {
	ctx = log.NewContextWithValues(ctx, slog.String("step", string(step.Name())))

	start := time.Now()
	log.FromContext(ctx).Info("start step")
	err := step.Run(ctx)
	log.FromContext(ctx).Info("finish step", slog.Duration("time", time.Since(start)))
	if err != nil {
		return err
	}

	if ev := step.Event(ctx); ev != nil {
		err = c.eventPublisher.PublishEvent(ctx, ev)
		if err != nil {
			log.FromContext(ctx).Error("publish event error", slog.String("error", err.Error()))
		}
	}

	return nil
}

type errorTyper interface {
	Type() errors.ErrorType
}

// publishError will publish an event for the provided error.
func (c *ProcessCommand) publishError(ctx context.Context, err error) {
	fi := instance.FromContext(ctx)

	errorEvent := event.ErrorEvent{
		CombinationID: fi.CombinationId(),
		ErrorType:     errors.TypeUnknown,
		ErrorMessage:  err.Error(),
		Timestamp:     time.Now().UTC().Truncate(time.Millisecond).Format(time.RFC3339Nano),
	}

	if et, ok := err.(errorTyper); ok {
		errorEvent.ErrorType = et.Type()
	}

	err = c.eventPublisher.PublishEvent(ctx, errorEvent)
	if err != nil {
		log.FromContext(ctx).Error("publish event error", slog.String("error", err.Error()))
	}
}
