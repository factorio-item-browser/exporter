package command

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/exporter.git/internal/command/mocks"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/event"
	finstance "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/step"
)

func TestNewProcessCommand(t *testing.T) {
	eventPublisher := mocks.NewEventPublisher(t)
	instanceCreator := finstance.NewCreator(config.Directories{Instances: "/root/instances"})
	steps := []step.Step{
		mocks.NewStep(t),
		mocks.NewStep(t),
	}

	instance := NewProcessCommand(eventPublisher, instanceCreator, steps)

	assert.NotNil(t, instance)
	assert.Same(t, eventPublisher, instance.eventPublisher)
	assert.Equal(t, steps, instance.steps)
}

func TestProcessCommand_Run(t *testing.T) {
	contextMatcher := func(ctx context.Context) bool {
		fi := finstance.FromContext(ctx)

		assert.Equal(t, "test", fi.CombinationId())
		assert.Equal(t, []string{"foo", "bar"}, fi.ModNames())
		assert.NotNil(t, dump.MetaDataFromContext(ctx))

		return true
	}

	event1 := mocks.NewEvent(t)
	event3 := mocks.NewEvent(t)

	step1 := mocks.NewStep(t)
	step1.On("Name").Maybe().Return(step.StepName("step1"))
	step1.On("Run", mock.MatchedBy(contextMatcher)).Once().Return(nil)
	step1.On("Event", mock.MatchedBy(contextMatcher)).Once().Return(event1)

	step2 := mocks.NewStep(t)
	step2.On("Name").Maybe().Return(step.StepName("step2"))
	step2.On("Run", mock.MatchedBy(contextMatcher)).Once().Return(nil)
	step2.On("Event", mock.MatchedBy(contextMatcher)).Once().Return(nil)

	step3 := mocks.NewStep(t)
	step3.On("Name").Maybe().Return(step.StepName("step3"))
	step3.On("Run", mock.MatchedBy(contextMatcher)).Once().Return(nil)
	step3.On("Event", mock.MatchedBy(contextMatcher)).Once().Return(event3)

	eventPublisher := mocks.NewEventPublisher(t)
	eventPublisher.On("PublishEvent", mock.MatchedBy(contextMatcher), event1).Once().Return(nil)
	eventPublisher.On("PublishEvent", mock.MatchedBy(contextMatcher), event3).Once().Return(nil)

	instanceCreator := finstance.NewCreator(config.Directories{Instances: "/root/instances"})

	steps := []step.Step{step1, step2, step3}
	params := ProcessCommandParams{
		CombinationId: "test",
		ModNames:      []string{"foo", "bar"},
	}

	instance := ProcessCommand{
		eventPublisher:  eventPublisher,
		instanceCreator: instanceCreator,
		steps:           steps,
	}
	err := instance.Run(context.Background(), params)

	assert.Nil(t, err)
}

func TestProcessCommand_Run_withMissingParameters(t *testing.T) {
	tests := map[string]struct {
		params ProcessCommandParams
	}{
		"missing combination id": {
			params: ProcessCommandParams{
				CombinationId: "",
				ModNames:      []string{"foo", "bar"},
			},
		},
		"missing mod names": {
			params: ProcessCommandParams{
				CombinationId: "test",
				ModNames:      []string{},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := ProcessCommand{
				steps: []step.Step{
					mocks.NewStep(t),
					mocks.NewStep(t),
					mocks.NewStep(t),
				},
			}
			err := instance.Run(context.Background(), test.params)

			assert.Nil(t, err)
		})
	}

}

type testError struct {
}

func (testError) Type() errors.ErrorType {
	return "test type"
}

func (testError) Error() string {
	return "test error"
}

func TestProcessCommand_Run_withError(t *testing.T) {
	runError := testError{}

	event1 := mocks.NewEvent(t)
	errorEvent := event.ErrorEvent{
		CombinationID: "test",
		ErrorType:     "test type",
		ErrorMessage:  "test error",
	}

	step1 := mocks.NewStep(t)
	step1.On("Name").Maybe().Return(step.StepName("step1"))
	step1.On("Run", mock.Anything).Once().Return(nil)
	step1.On("Event", mock.Anything).Once().Return(event1)

	step2 := mocks.NewStep(t)
	step2.On("Name").Maybe().Return(step.StepName("step2"))
	step2.On("Run", mock.Anything).Once().Return(runError)

	step3 := mocks.NewStep(t)
	step3.On("Name").Maybe().Return(step.StepName("step3"))

	eventPublisher := mocks.NewEventPublisher(t)
	eventPublisher.On("PublishEvent", mock.Anything, event1).Once().Return(fmt.Errorf("test error"))
	eventPublisher.On("PublishEvent", mock.Anything, mock.MatchedBy(func(e event.ErrorEvent) bool {
		assert.NotEmpty(t, e.Timestamp)
		e.Timestamp = ""

		return assert.Equal(t, errorEvent, e)
	})).Once().Return(fmt.Errorf("test error"))

	instanceCreator := finstance.NewCreator(config.Directories{Instances: "/root/instances"})

	steps := []step.Step{step1, step2, step3}
	params := ProcessCommandParams{
		CombinationId: "test",
		ModNames:      []string{"foo", "bar"},
	}

	instance := ProcessCommand{
		eventPublisher:  eventPublisher,
		instanceCreator: instanceCreator,
		steps:           steps,
	}
	err := instance.Run(context.Background(), params)

	assert.Equal(t, runError, err)
}
