package instance

import (
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance/mocks"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/loader"
)

func TestNew(t *testing.T) {
	directory := "/test/instances"
	modNames := []string{"foo", "bar"}

	instance := New(directory, "comb", modNames)

	assert.Equal(t, "comb", instance.CombinationId())
	assert.Equal(t, modNames, instance.ModNames())

	assert.Equal(t, "/test/instances/comb", instance.Path(Directory))
	assert.Equal(t, "/test/instances/comb/factorio", instance.Path(FactorioDirectory))
	assert.Equal(t, "/test/instances/comb/factorio/data", instance.Path(FactorioDataDirectory))
	assert.Equal(t, "/test/instances/comb/factorio/mods", instance.Path(ModsDirectory))
	assert.Equal(t, "/test/instances/comb/output.txt", instance.Path(FactorioOutputFileName))
	assert.Equal(t, "/test/instances/comb/data.jsonl", instance.Path(DataOutputFileName))
	assert.Equal(t, "/test/instances/comb/stylesheet.css", instance.Path(StylesheetFileName))
}

func TestInstance_ResolveMods(t *testing.T) {
	os.FileSystem = afero.NewMemMapFs()
	_ = afero.WriteFile(os.FileSystem, "/test/mods/abc/info.json", []byte("abc"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/mods/def_1.2.3/info.json", []byte("def"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/mods/ghi.zip", []byte("ghi"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/mods/jkl_4.5.6.zip", []byte("jkl"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/factorio/data/base/info.json", []byte("base"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/factorio/data/core/info.json", []byte("core"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/factorio/data/changelog.txt", []byte("ignore"), 0644)

	expectedMods := map[string]mod{
		"abc": {
			isBundled: false,
			path:      "/test/mods/abc",
			loader:    &loader.ModDirectoryLoader{},
		},
		"def": {
			isBundled: false,
			path:      "/test/mods/def_1.2.3",
			loader:    &loader.ModDirectoryLoader{},
		},
		"ghi": {
			isBundled: false,
			path:      "/test/mods/ghi.zip",
			loader:    &loader.ModArchiveLoader{},
		},
		"jkl": {
			isBundled: false,
			path:      "/test/mods/jkl_4.5.6.zip",
			loader:    &loader.ModArchiveLoader{},
		},
		"core": {
			isBundled: true,
			path:      "/test/factorio/data/core",
			loader:    &loader.ModDirectoryLoader{},
		},
		"base": {
			isBundled: true,
			path:      "/test/factorio/data/base",
			loader:    &loader.ModDirectoryLoader{},
		},
	}

	instance := Instance{
		combinationId: "test",
		paths: map[Path]string{
			FactorioDataDirectory: "/test/factorio/data",
			ModsDirectory:         "/test/mods",
		},
	}
	instance.ResolveMods()

	assert.Len(t, instance.mods, len(expectedMods))
	for name, expectedMod := range expectedMods {
		mod, ok := instance.mods[name]
		assert.True(t, ok)
		assert.Equal(t, expectedMod.isBundled, mod.isBundled)
		assert.Equal(t, expectedMod.path, mod.path)
		assert.IsType(t, expectedMod.loader, mod.loader)
	}
}

func TestInstance_BundledModNames(t *testing.T) {
	mods := map[string]mod{
		"foo": {
			isBundled: true,
		},
		"bar": {
			isBundled: false,
		},
		"baz": {
			isBundled: true,
		},
	}
	expectedResult := []string{"foo", "baz"}

	instance := Instance{
		mods: mods,
	}
	result := instance.BundledModNames()

	assert.ElementsMatch(t, expectedResult, result)
}

func TestInstance_BundledModNames_withModResolving(t *testing.T) {
	os.FileSystem = afero.NewMemMapFs()
	_ = afero.WriteFile(os.FileSystem, "/test/mods/ignored/info.json", []byte("abc"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/mods/ignored.zip", []byte("ghi"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/factorio/data/base/info.json", []byte("base"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/factorio/data/core/info.json", []byte("core"), 0644)
	_ = afero.WriteFile(os.FileSystem, "/test/factorio/data/changelog.txt", []byte("ignore"), 0644)

	expectedMods := map[string]mod{
		"core": {
			isBundled: true,
			path:      "/test/factorio/data/core",
			loader:    &loader.ModDirectoryLoader{},
		},
		"base": {
			isBundled: true,
			path:      "/test/factorio/data/base",
			loader:    &loader.ModDirectoryLoader{},
		},
	}
	expectedResult := []string{"core", "base"}

	instance := Instance{
		combinationId: "test",
		paths: map[Path]string{
			FactorioDataDirectory: "/test/factorio/data",
			ModsDirectory:         "/test/mods",
		},
		mods: make(map[string]mod),
	}
	result := instance.BundledModNames()

	assert.ElementsMatch(t, expectedResult, result)
	assert.Len(t, instance.mods, len(expectedMods))
	for name, expectedMod := range expectedMods {
		mod, ok := instance.mods[name]
		assert.True(t, ok)
		assert.Equal(t, expectedMod.isBundled, mod.isBundled)
		assert.Equal(t, expectedMod.path, mod.path)
		assert.IsType(t, expectedMod.loader, mod.loader)
	}
}

func TestInstance_ModPath(t *testing.T) {
	tests := map[string]struct {
		mods              map[string]mod
		modName           string
		expectedErrorType error
		expectedResult    string
	}{
		"happy path": {
			mods: map[string]mod{
				"foo": {
					path: "some/path",
				},
			},
			modName:           "foo",
			expectedErrorType: nil,
			expectedResult:    "some/path",
		},
		"unknown mod": {
			mods:              nil,
			modName:           "foo",
			expectedErrorType: &errors.UnknownModError{},
			expectedResult:    "",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := Instance{
				mods: test.mods,
			}
			result, err := instance.ModPath(test.modName)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestInstance_ReadModFile(t *testing.T) {
	content := io.NopCloser(strings.NewReader("fancy content"))

	tests := map[string]struct {
		modName           string
		expectLoad        bool
		loaderResult      io.ReadCloser
		loaderError       error
		expectedErrorType error
		expectedResult    io.ReadCloser
	}{
		"happy path": {
			modName:           "foo",
			expectLoad:        true,
			loaderResult:      content,
			loaderError:       nil,
			expectedErrorType: nil,
			expectedResult:    content,
		},
		"unknown mod": {
			modName:           "unknown",
			expectLoad:        false,
			expectedErrorType: &errors.UnknownModError{},
			expectedResult:    nil,
		},
		"loader error": {
			modName:           "foo",
			expectLoad:        true,
			loaderResult:      nil,
			loaderError:       fmt.Errorf("test error"),
			expectedErrorType: &errors.ReadModFileError{},
			expectedResult:    nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			fileName := "fancy.txt"

			loader := mocks.NewModLoader(t)
			if test.expectLoad {
				loader.On("Load", fileName).Once().Return(test.loaderResult, test.loaderError)
			}

			mods := map[string]mod{
				"foo": {
					loader: loader,
				},
			}

			instance := Instance{
				mods: mods,
			}
			result, err := instance.ReadModFile(test.modName, fileName)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestInstance_ModInfo(t *testing.T) {
	tests := map[string]struct {
		loaderResult      io.ReadCloser
		loaderError       error
		expectedErrorType error
		expectedResult    ModInfo
	}{
		"happy path": {
			loaderResult: io.NopCloser(strings.NewReader(`{
				"name": "foo",
				"title": "Foo",
				"description": "The foo of all mods.",
				"author": "factorio-item-browser",
				"version": "1.2.3",
				"factorio_version": "1.0",
				"dependencies": []
			}`)),
			loaderError:       nil,
			expectedErrorType: nil,
			expectedResult: ModInfo{
				Name:            "foo",
				Title:           "Foo",
				Description:     "The foo of all mods.",
				Author:          "factorio-item-browser",
				Version:         "1.2.3",
				FactorioVersion: "1.0",
				Dependencies:    []string{},
			},
		},
		"loader error": {
			loaderResult:      nil,
			loaderError:       &errors.ReadModFileError{},
			expectedErrorType: &errors.ReadModFileError{},
			expectedResult:    ModInfo{},
		},
		"invalid json": {
			loaderResult:      io.NopCloser(strings.NewReader(`{"invalid`)),
			loaderError:       nil,
			expectedErrorType: &errors.ReadModFileError{},
			expectedResult:    ModInfo{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			modName := "fancy"

			modLoader := mocks.NewModLoader(t)
			modLoader.On("Load", "info.json").Once().Return(test.loaderResult, test.loaderError)

			mods := map[string]mod{
				"fancy": {
					loader: modLoader,
				},
			}

			instance := Instance{
				mods: mods,
			}
			result, err := instance.ModInfo(modName)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}
