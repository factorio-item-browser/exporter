package instance

import (
	"encoding/json"
	"fmt"
	"io"
	"maps"
	"regexp"
	"slices"
	"strings"

	"github.com/spf13/afero"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/loader"
)

type Path int

const (
	Directory Path = iota
	FactorioDirectory
	FactorioDataDirectory
	ModsDirectory
	FactorioOutputFileName
	DataOutputFileName
	StylesheetFileName
)

var (
	regexpModVersion = regexp.MustCompile(`_\d+\.\d+\.\d+$`)
)

type modLoader interface {
	Load(path string) (io.ReadCloser, error)
}

type mod struct {
	isBundled bool
	path      string
	loader    modLoader
}

// Instance represents the instance of Factorio which eventually will get executed to dump all the data from the game.
type Instance struct {
	combinationId string
	modNames      []string
	mods          map[string]mod
	paths         map[Path]string
}

// New creates a new instance of Factorio. The instance must actually be set up by copying the files to its directory.
func New(instancesDirectory string, combinationId string, modNames []string) *Instance {
	directory := fmt.Sprintf("%s/%s", instancesDirectory, combinationId)

	return &Instance{
		combinationId: combinationId,
		modNames:      slices.Clone(modNames),
		mods:          make(map[string]mod),
		paths: map[Path]string{
			Directory:              directory,
			FactorioDirectory:      fmt.Sprintf("%s/factorio", directory),
			FactorioDataDirectory:  fmt.Sprintf("%s/factorio/data", directory),
			ModsDirectory:          fmt.Sprintf("%s/factorio/mods", directory),
			FactorioOutputFileName: fmt.Sprintf("%s/output.txt", directory),
			DataOutputFileName:     fmt.Sprintf("%s/data.jsonl", directory),
			StylesheetFileName:     fmt.Sprintf("%s/stylesheet.css", directory),
		},
	}
}

// ResolveMods will resolve all mods which are available in the Factorio data or mods directory.
func (i *Instance) ResolveMods() {
	i.mods = make(map[string]mod)
	maps.Copy(i.mods, i.resolveModFiles(ModsDirectory, false))
	maps.Copy(i.mods, i.resolveModFiles(FactorioDataDirectory, true))
}

func (i *Instance) resolveModFiles(path Path, isBundled bool) map[string]mod {
	files, _ := afero.Glob(os.FileSystem, fmt.Sprintf("%s/*", i.paths[path]))
	mods := make(map[string]mod, len(files))

	for _, file := range files {
		isArchive := false
		modName := file[len(i.paths[path])+1:]
		if strings.HasSuffix(modName, ".zip") {
			modName = modName[:len(modName)-4]
			isArchive = true
		} else if stat, err := os.FileSystem.Stat(file); err != nil || !stat.IsDir() {
			continue
		}

		match := regexpModVersion.FindString(modName)
		modName = modName[:len(modName)-len(match)]
		fileMod := mod{
			isBundled: isBundled,
			path:      file,
		}

		if isArchive {
			fileMod.loader = loader.NewModArchiveLoader(file)
		} else {
			fileMod.loader = loader.NewModDirectoryLoader(file)
		}

		mods[modName] = fileMod
	}

	return mods
}

// CombinationId is the Combination ID of the instance.
func (i *Instance) CombinationId() string {
	return i.combinationId
}

// ModNames are the list of mods to load in the instance.
func (i *Instance) ModNames() []string {
	return slices.Clone(i.modNames)
}

// BundledModNames are all mods which come bundled with Factorio.
func (i *Instance) BundledModNames() []string {
	if len(i.mods) == 0 {
		i.mods = i.resolveModFiles(FactorioDataDirectory, true)
	}

	result := make([]string, 0)
	for modName, mod := range i.mods {
		if mod.isBundled {
			result = append(result, modName)
		}
	}
	return result
}

// Path returns the provided path in the context of the instance.
func (i *Instance) Path(path Path) string {
	return i.paths[path]
}

// ModPath returns the path to the mod with the provided name. The path either points to the archive file, or the
// mod directory.
func (i *Instance) ModPath(modName string) (string, error) {
	mod, ok := i.mods[modName]
	if !ok {
		return "", errors.NewUnknownModError(modName)
	}

	return mod.path, nil
}

// ReadModFile reads a file from a mod. If the file does not exist, an error will be returned instead.
func (i *Instance) ReadModFile(modName string, fileName string) (io.ReadCloser, error) {
	mod, ok := i.mods[modName]
	if !ok {
		return nil, errors.NewUnknownModError(modName)
	}

	file, err := mod.loader.Load(fileName)
	if err != nil {
		return nil, errors.NewReadModFileError(modName, fileName, err)
	}
	return file, nil
}

// ModInfo returns the info of the mod with the provided name.
func (i *Instance) ModInfo(modName string) (ModInfo, error) {
	var result ModInfo

	file, err := i.ReadModFile(modName, "info.json")
	if err != nil {
		return result, err
	}
	defer func() {
		_ = file.Close()
	}()

	err = json.NewDecoder(file).Decode(&result)
	if err != nil {
		return result, errors.NewReadModFileError(modName, "info.json", fmt.Errorf("invalid JSON file: %s", err))
	}
	return result, nil
}
