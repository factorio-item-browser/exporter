package instance

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewContext(t *testing.T) {
	instance := &Instance{
		combinationId: "test",
	}

	ctx := context.Background()
	ctx2 := NewContext(ctx, instance)
	assert.Same(t, instance, FromContext(ctx2))
}
