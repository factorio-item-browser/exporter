package instance

import (
	"context"
	"fmt"
	"io"
	"strings"
)

type MockedMod struct {
	files map[string]string
}

func (m *MockedMod) WithFile(path string, content string) *MockedMod {
	m.files[path] = content
	return m
}

func (m *MockedMod) Load(path string) (io.ReadCloser, error) {
	content, ok := m.files[path]
	if !ok {
		return nil, fmt.Errorf("%s: file not known", path)
	}

	return io.NopCloser(strings.NewReader(content)), nil
}

type MockedInstanceBuilder struct {
	instance *Instance
}

// NewMockedInstanceBuilder creates a new builder for a mocked instance.
// The base directory of the instance will be "/root/instances/test".
func NewMockedInstanceBuilder() *MockedInstanceBuilder {
	return &MockedInstanceBuilder{
		instance: New("/root/instances", "test", []string{}),
	}
}

func (b *MockedInstanceBuilder) WithMod(modName string) *MockedMod {
	newMod := MockedMod{
		files: make(map[string]string),
	}

	b.instance.modNames = append(b.instance.modNames, modName)
	b.instance.mods[modName] = mod{
		path:   "/root/instances/test/factorio/mods/" + modName,
		loader: &newMod,
	}

	return &newMod
}

func (b *MockedInstanceBuilder) WithBundledMod(modName string, includeInModNames bool) *MockedMod {
	newMod := MockedMod{
		files: make(map[string]string),
	}

	if includeInModNames {
		b.instance.modNames = append(b.instance.modNames, modName)
	}

	b.instance.mods[modName] = mod{
		isBundled: true,
		path:      "/root/instances/test/factorio/mods/" + modName,
		loader:    &newMod,
	}

	return &newMod
}

func (b *MockedInstanceBuilder) BuildContext(ctx context.Context) context.Context {
	return NewContext(ctx, b.instance)
}
