package instance

type ModInfo struct {
	Name            string   `json:"name"`
	Title           string   `json:"title"`
	Description     string   `json:"description"`
	Author          string   `json:"author"`
	Version         string   `json:"version"`
	FactorioVersion string   `json:"factorio_version"`
	Dependencies    []string `json:"dependencies"`
}

type ModList struct {
	Mods []ModListMod `json:"mods"`
}

type ModListMod struct {
	Name    string `json:"name"`
	Enabled bool   `json:"enabled"`
}
