package instance

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
)

func TestNewCreator(t *testing.T) {
	directory := "/root/instances"
	combinationId := "test"
	modNames := []string{"base", "foo"}
	expectedDirectory := "/root/instances/test"

	cfg := config.Directories{
		Instances: directory,
	}

	creator := NewCreator(cfg)
	instance := creator(combinationId, modNames)

	assert.Equal(t, modNames, instance.ModNames())
	assert.Equal(t, expectedDirectory, instance.Path(Directory))
}
