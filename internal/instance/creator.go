package instance

import "gitlab.com/factorio-item-browser/exporter.git/internal/config"

type Creator = func(combinationId string, modNames []string) *Instance

func NewCreator(cfg config.Directories) Creator {
	return func(combinationId string, modNames []string) *Instance {
		return New(cfg.Instances, combinationId, modNames)
	}
}
