package instance

import (
	"context"
)

type contextKey string

var keyInstance contextKey = "instance"

// NewContext creates a new context with the provided instance.
func NewContext(ctx context.Context, instance *Instance) context.Context {
	return context.WithValue(ctx, keyInstance, instance)
}

// FromContext returns the instance from the provided context. THe instance must be set into the context beforehand.
func FromContext(ctx context.Context) *Instance {
	return ctx.Value(keyInstance).(*Instance)
}
