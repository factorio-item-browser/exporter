package prepare

import (
	"context"
	"fmt"
	realos "os"
	"os/exec"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
)

func TestNewFactorioPreparer(t *testing.T) {
	directoryFactorio := "/root/factorio"
	cfg := config.Directories{
		Factorio: directoryFactorio,
	}

	instance := NewFactorioPreparer(cfg)

	assert.NotNil(t, instance)
	assert.Equal(t, directoryFactorio, instance.directoryFactorio)
}

func TestFactorioPreparer_Prepare(t *testing.T) {
	tests := map[string]struct {
		commandExitCode   int
		expectedErrorType error
	}{
		"happy path": {
			commandExitCode:   0,
			expectedErrorType: nil,
		},
		"command error": {
			commandExitCode:   1,
			expectedErrorType: &errors.LocalFileSystemError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 func(string, ...string) *exec.Cmd) {
				execCommand = ref1
			}(execCommand)

			directoryFactorio := "/opt/factorio"

			instanceBuilder := instance2.NewMockedInstanceBuilder()
			instanceBuilder.WithMod("base")

			ctx := context.Background()
			ctx = instanceBuilder.BuildContext(ctx)

			expectedCommand := "cp"
			expectedArgs := []string{"-r", directoryFactorio, "/root/instances/test"}

			execCommand = func(command string, args ...string) *exec.Cmd {
				assert.Equal(t, expectedCommand, command)
				assert.ElementsMatch(t, expectedArgs, args)

				fakeArgs := []string{"-test.run=TestFactorioPreparer_Command", "--", command}
				fakeArgs = append(fakeArgs, args...)
				cmd := exec.Command(realos.Args[0], fakeArgs...)
				cmd.Env = []string{fmt.Sprintf("GO_TEST_PROCESS=%d", test.commandExitCode)}
				return cmd
			}

			instance := FactorioPreparer{
				directoryFactorio: directoryFactorio,
			}
			err := instance.Prepare(ctx)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}

func TestFactorioPreparer_Command(t *testing.T) {
	if realos.Getenv("GO_TEST_PROCESS") == "" {
		return
	}

	exitCode, _ := strconv.ParseInt(realos.Getenv("GO_TEST_PROCESS"), 10, 64)
	realos.Exit(int(exitCode))
}
