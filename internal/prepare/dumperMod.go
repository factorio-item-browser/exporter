package prepare

import (
	"context"
	"fmt"
	"log/slog"

	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
)

// DumperModPreparer will prepare the dumper mod for the export by copying all files to the instance, and create
// an info.json file having all mods of the export as dependencies.
type DumperModPreparer struct {
	directoryExporter string
}

func NewDumperModPreparer(cfg config.Directories) *DumperModPreparer {
	return &DumperModPreparer{
		directoryExporter: cfg.Exporter,
	}
}

func (p *DumperModPreparer) Prepare(ctx context.Context) error {
	fi := instance.FromContext(ctx)
	fi.BundledModNames() // Implicitly resolve bundled mods.

	log.FromContext(ctx).Info("preparing dumper mod", slog.Int("mod-count", len(fi.ModNames())))

	sourceDirectory := fmt.Sprintf("%s/lua/dumper", p.directoryExporter)
	dumperDirectory := fmt.Sprintf("%s/dumper", fi.Path(instance.ModsDirectory))

	copyCommand := execCommand("cp", "-r", sourceDirectory, dumperDirectory)
	err := copyCommand.Run()
	if err != nil {
		return errors.NewLocalFileSystemError("unable to copy dumper mod", err)
	}

	baseModInfo, _ := fi.ModInfo("base")

	modInfo := instance.ModInfo{
		Name:            "dumper",
		Title:           "Factorio Item Browser Dumper",
		Author:          "Factorio Item Browser",
		Version:         "1.0.0",
		FactorioVersion: baseModInfo.Version,
		Dependencies:    fi.ModNames(),
	}

	err = writeJsonFile(ctx, fmt.Sprintf("%s/info.json", dumperDirectory), modInfo)
	if err != nil {
		return errors.NewLocalFileSystemError("unable to write info.json file of dumper mod", err)
	}
	return nil
}
