package prepare

import (
	"context"
	"log/slog"

	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
)

// DirectoryPreparer will prepare all required directories for the export.
type DirectoryPreparer struct {
}

func NewDirectoryPreparer() *DirectoryPreparer {
	return &DirectoryPreparer{}
}

func (p *DirectoryPreparer) Prepare(ctx context.Context) error {
	fi := instance.FromContext(ctx)

	log.FromContext(ctx).Info("preparing instance directories", slog.String("directory", fi.Path(instance.Directory)))

	err := os.FileSystem.RemoveAll(fi.Path(instance.Directory))
	if err != nil {
		return errors.NewLocalFileSystemError("unable to remove existing directory", err)
	}

	err = os.FileSystem.MkdirAll(fi.Path(instance.Directory), 0755)
	if err != nil {
		return errors.NewLocalFileSystemError("unable to create instance directory", err)
	}

	err = os.FileSystem.MkdirAll(fi.Path(instance.ModsDirectory), 0755)
	if err != nil {
		return errors.NewLocalFileSystemError("unable to create mods directory", err)
	}

	return nil
}
