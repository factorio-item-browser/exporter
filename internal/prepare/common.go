package prepare

import (
	"context"
	"encoding/json"
	"os/exec"

	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
)

var execCommand = exec.Command

type Preparer interface {
	Prepare(ctx context.Context) error
}

// writeJsonFile will encode the provided data as JSON, and will write it to the specified fileName.
func writeJsonFile(_ context.Context, fileName string, data any) error {
	fp, err := os.FileSystem.Create(fileName)
	if err != nil {
		return err
	}
	defer func() {
		_ = fp.Close()
	}()

	return json.NewEncoder(fp).Encode(data)
}
