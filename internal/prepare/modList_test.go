package prepare

import (
	"context"
	"encoding/json"
	"fmt"
	"slices"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/exporter.git/internal/prepare/mocks"
)

func TestNewModListPreparer(t *testing.T) {
	instance := NewModListPreparer()

	assert.NotNil(t, instance)
}

func TestModListPreparer_Prepare(t *testing.T) {
	tests := map[string]struct {
		modNames          []string
		expectedErrorType error
		expectedModList   instance2.ModList
	}{
		"with base": {
			modNames:          []string{"base", "foo", "bar"},
			expectedErrorType: nil,
			expectedModList: instance2.ModList{
				Mods: []instance2.ModListMod{
					{
						Name:    "base",
						Enabled: true,
					},
					{
						Name:    "foo",
						Enabled: true,
					},
					{
						Name:    "bar",
						Enabled: true,
					},
					{
						Name:    "dumper",
						Enabled: true,
					},
				},
			},
		},
		"without base": {
			modNames:          []string{"foo", "bar"},
			expectedErrorType: nil,
			expectedModList: instance2.ModList{
				Mods: []instance2.ModListMod{
					{
						Name:    "foo",
						Enabled: true,
					},
					{
						Name:    "bar",
						Enabled: true,
					},
					{
						Name:    "base",
						Enabled: false,
					},
					{
						Name:    "dumper",
						Enabled: true,
					},
				},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 afero.Fs) {
				os.FileSystem = ref1
			}(os.FileSystem)
			os.FileSystem = afero.NewMemMapFs()

			instanceBuilder := instance2.NewMockedInstanceBuilder()
			instanceBuilder.WithMod("foo")
			instanceBuilder.WithMod("bar")
			instanceBuilder.WithBundledMod("base", slices.Contains(test.modNames, "base"))
			instanceBuilder.WithBundledMod("core", slices.Contains(test.modNames, "core"))
			ctx := instanceBuilder.BuildContext(context.Background())

			instance := ModListPreparer{}
			err := instance.Prepare(ctx)

			assert.IsType(t, test.expectedErrorType, err)

			var modList instance2.ModList
			content, _ := afero.ReadFile(os.FileSystem, "/root/instances/test/factorio/mods/mod-list.json")
			_ = json.Unmarshal(content, &modList)
			assert.ElementsMatch(t, test.expectedModList.Mods, modList.Mods)
		})
	}
}

func TestModListPreparer_Prepare_withError(t *testing.T) {
	defer func(ref1 afero.Fs) {
		os.FileSystem = ref1
	}(os.FileSystem)

	instanceBuilder := instance2.NewMockedInstanceBuilder()
	instanceBuilder.WithMod("foo")

	ctx := context.Background()
	ctx = instanceBuilder.BuildContext(ctx)

	fs := mocks.NewFs(t)
	fs.On("Create", mock.Anything).Return(nil, fmt.Errorf("test error"))
	os.FileSystem = fs

	instance := ModListPreparer{}
	err := instance.Prepare(ctx)

	assert.IsType(t, &errors.LocalFileSystemError{}, err)
}
