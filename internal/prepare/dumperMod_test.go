package prepare

import (
	"context"
	"encoding/json"
	"fmt"
	realos "os"
	"os/exec"
	"strconv"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/exporter.git/internal/prepare/mocks"
)

func TestNewDumperModPreparer(t *testing.T) {
	directoryExporter := "/root/exporter"
	cfg := config.Directories{
		Exporter: directoryExporter,
	}

	instance := NewDumperModPreparer(cfg)

	assert.NotNil(t, instance)
	assert.Equal(t, directoryExporter, instance.directoryExporter)
}

func TestDumperModPreparer_Prepare(t *testing.T) {
	tests := map[string]struct {
		commandExitCode   int
		withWriteError    bool
		expectedErrorType error
		expectModInfo     bool
	}{
		"happy path": {
			commandExitCode:   0,
			withWriteError:    false,
			expectedErrorType: nil,
			expectModInfo:     true,
		},
		"copy error": {
			commandExitCode:   1,
			withWriteError:    false,
			expectedErrorType: &errors.LocalFileSystemError{},
			expectModInfo:     false,
		},
		"write error": {
			commandExitCode:   0,
			withWriteError:    true,
			expectedErrorType: &errors.LocalFileSystemError{},
			expectModInfo:     false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 func(string, ...string) *exec.Cmd, ref2 afero.Fs) {
				execCommand = ref1
				os.FileSystem = ref2
			}(execCommand, os.FileSystem)

			directoryExporter := "/opt/exporter"
			modNames := []string{"foo", "bar"}

			instanceBuilder := instance2.NewMockedInstanceBuilder()
			instanceBuilder.WithMod("foo")
			instanceBuilder.WithMod("bar")

			ctx := context.Background()
			ctx = instanceBuilder.BuildContext(ctx)

			expectedCommand := "cp"
			expectedArgs := []string{"-r", "/opt/exporter/lua/dumper", "/root/instances/test/factorio/mods/dumper"}

			execCommand = func(command string, args ...string) *exec.Cmd {
				assert.Equal(t, expectedCommand, command)
				assert.ElementsMatch(t, expectedArgs, args)

				fakeArgs := []string{"-test.run=TestDumperModPreparer_Command", "--", command}
				fakeArgs = append(fakeArgs, args...)
				cmd := exec.Command(realos.Args[0], fakeArgs...)
				cmd.Env = []string{fmt.Sprintf("GO_TEST_PROCESS=%d", test.commandExitCode)}
				return cmd
			}

			os.FileSystem = afero.NewMemMapFs()
			if test.withWriteError {
				fs := mocks.NewFs(t)
				fs.On("Create", "/root/instances/test/factorio/mods/dumper/info.json").Return(nil, fmt.Errorf("test error"))
				os.FileSystem = fs
			}

			instance := DumperModPreparer{
				directoryExporter: directoryExporter,
			}
			err := instance.Prepare(ctx)

			assert.IsType(t, test.expectedErrorType, err)

			if test.expectModInfo {
				var modInfo instance2.ModInfo
				content, _ := afero.ReadFile(os.FileSystem, "/root/instances/test/factorio/mods/dumper/info.json")
				_ = json.Unmarshal(content, &modInfo)
				assert.Equal(t, "dumper", modInfo.Name)
				assert.Equal(t, modNames, modInfo.Dependencies)
			}
		})
	}
}

func TestDumperModPreparer_Command(t *testing.T) {
	if realos.Getenv("GO_TEST_PROCESS") == "" {
		return
	}

	exitCode, _ := strconv.ParseInt(realos.Getenv("GO_TEST_PROCESS"), 10, 64)
	realos.Exit(int(exitCode))
}
