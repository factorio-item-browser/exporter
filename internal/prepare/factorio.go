package prepare

import (
	"context"

	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
)

type FactorioPreparer struct {
	directoryFactorio string
}

func NewFactorioPreparer(cfg config.Directories) *FactorioPreparer {
	return &FactorioPreparer{
		directoryFactorio: cfg.Factorio,
	}
}

func (p *FactorioPreparer) Prepare(ctx context.Context) error {
	fi := instance.FromContext(ctx)

	copyCommand := execCommand("cp", "-r", p.directoryFactorio, fi.Path(instance.Directory))
	err := copyCommand.Run()
	if err != nil {
		return errors.NewLocalFileSystemError("unable to copy Factorio installation", err)
	}

	return nil
}
