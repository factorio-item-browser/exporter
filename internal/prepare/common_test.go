package prepare

import (
	"context"
	"fmt"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/exporter.git/internal/prepare/mocks"
)

func TestWriteJsonFile(t *testing.T) {
	os.FileSystem = afero.NewMemMapFs()

	fileName := "test/foo.json"
	data := struct {
		Foo string `json:"foo"`
	}{
		Foo: "bar",
	}
	expectedContent := `{"foo":"bar"}
`

	err := writeJsonFile(context.Background(), fileName, data)

	assert.Nil(t, err)

	content, _ := afero.ReadFile(os.FileSystem, fileName)
	assert.Equal(t, expectedContent, string(content))
}

func TestWriteJsonFile_WithError(t *testing.T) {
	fileName := "test/foo.json"
	data := struct {
		Foo string `json:"foo"`
	}{
		Foo: "bar",
	}
	createError := fmt.Errorf("test error")

	fs := mocks.NewFs(t)
	fs.On("Create", fileName).Once().Return(nil, createError)
	os.FileSystem = fs

	err := writeJsonFile(context.Background(), fileName, data)

	assert.Equal(t, createError, err)
}
