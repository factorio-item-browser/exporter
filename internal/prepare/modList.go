package prepare

import (
	"context"
	"fmt"
	"slices"

	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
)

// ModListPreparer will prepare the mod-list.json file to enable all the required mods for the export.
type ModListPreparer struct {
}

func NewModListPreparer() *ModListPreparer {
	return &ModListPreparer{}
}

func (p *ModListPreparer) Prepare(ctx context.Context) error {
	fi := instance.FromContext(ctx)

	bundledModNames := fi.BundledModNames()
	seenBundledMods := make(map[string]struct{}, len(bundledModNames))

	modList := instance.ModList{
		Mods: make([]instance.ModListMod, 0, len(fi.ModNames())+1),
	}

	for _, modName := range fi.ModNames() {
		modList.Mods = append(modList.Mods, instance.ModListMod{
			Name:    modName,
			Enabled: true,
		})

		if slices.Contains(bundledModNames, modName) {
			seenBundledMods[modName] = struct{}{}
		}
	}

	for _, modName := range bundledModNames {
		if modName == "core" {
			continue
		}

		if _, ok := seenBundledMods[modName]; !ok {
			// We did not see a bundled mod within the enabled mods, so we have to explicitly disable it.
			modList.Mods = append(modList.Mods, instance.ModListMod{
				Name:    modName,
				Enabled: false,
			})
		}
	}

	modList.Mods = append(modList.Mods, instance.ModListMod{
		Name:    "dumper",
		Enabled: true,
	})
	err := writeJsonFile(ctx, fmt.Sprintf("%s/mod-list.json", fi.Path(instance.ModsDirectory)), modList)
	if err != nil {
		return errors.NewLocalFileSystemError("unable to write modList.json file", err)
	}

	return nil
}
