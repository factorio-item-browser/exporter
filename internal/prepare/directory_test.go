package prepare

import (
	"context"
	"fmt"
	"io/fs"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/exporter.git/internal/prepare/mocks"
)

func TestNewDirectoryPreparer(t *testing.T) {
	instance := NewDirectoryPreparer()

	assert.NotNil(t, instance)
}

func TestDirectoryPreparer_Prepare(t *testing.T) {
	tests := map[string]struct {
		errorRemove       error
		errorMake1        error
		errorMake2        error
		expectedErrorType error
	}{
		"happy path": {
			errorRemove:       nil,
			errorMake1:        nil,
			errorMake2:        nil,
			expectedErrorType: nil,
		},
		"remove error": {
			errorRemove:       fmt.Errorf("test error"),
			errorMake1:        nil,
			errorMake2:        nil,
			expectedErrorType: &errors.LocalFileSystemError{},
		},
		"make1 error": {
			errorRemove:       nil,
			errorMake1:        fmt.Errorf("test error"),
			errorMake2:        nil,
			expectedErrorType: &errors.LocalFileSystemError{},
		},
		"make2 error": {
			errorRemove:       nil,
			errorMake1:        nil,
			errorMake2:        fmt.Errorf("test error"),
			expectedErrorType: &errors.LocalFileSystemError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

			fileSystem := mocks.NewFs(t)
			fileSystem.On("RemoveAll", "/root/instances/test").Once().Return(test.errorRemove)
			fileSystem.On("MkdirAll", "/root/instances/test", fs.FileMode(0755)).Maybe().Return(test.errorMake1)
			fileSystem.On("MkdirAll", "/root/instances/test/factorio/mods", fs.FileMode(0755)).Maybe().Return(test.errorMake2)
			os.FileSystem = fileSystem

			instance := DirectoryPreparer{}
			err := instance.Prepare(ctx)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}
