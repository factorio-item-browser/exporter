package dump

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump/mocks"
)

func TestNewTechnologyProcessor(t *testing.T) {
	hashProvider := mocks.NewHashProvider(t)
	translator := mocks.NewTranslator(t)

	instance := NewTechnologyProcessor(hashProvider, translator)

	assert.NotNil(t, instance)
	assert.Same(t, hashProvider, instance.hashProvider)
	assert.Same(t, translator, instance.translator)
}

func TestTechnologyProcessor_Process(t *testing.T) {
	localisedLabel := []any{"label", float64(42)}
	localisedDescription := []any{"description", float64(1337)}
	labels := exportdata.Translations{"en": "abc"}
	descriptions := exportdata.Translations{"en": "def"}

	tests := map[string]struct {
		entity                       any
		expectTranslation            bool
		expectHash                   bool
		iconHash                     string
		expectedResult               any
		expectedNumberOfTechnologies uint64
	}{
		"full": {
			entity: Technology{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				Prerequisites:        []string{"foo", "bar"},
				ResearchUnitIngredients: []TechnologyIngredient{
					{
						Name:   "blue",
						Amount: 1.2,
					},
					{
						Name:   "red",
						Amount: 3.4,
					},
				},
				ResearchUnitCount:        42,
				ResearchUnitCountFormula: "42+n",
				ResearchUnitEnergy:       802.2,
				ResearchTrigger: &TechnologyTrigger{
					Type:   "item",
					Name:   "foo",
					Amount: 5.6,
				},
				UnlockedRecipes: []string{"fancy-recipe", "raw-recipe"},
				Level:           21,
				MaxLevel:        1337,
				Order:           "x[fancy]",
				Hidden:          false,
			},
			expectTranslation: true,
			expectHash:        true,
			iconHash:          "fancy-hash",
			expectedResult: exportdata.TechnologyV1{
				Name:          "fancy",
				Labels:        labels,
				Descriptions:  descriptions,
				Icon:          "fancy-hash",
				Prerequisites: []string{"foo", "bar"},
				Ingredients: []exportdata.TechnologyV1Ingredient{
					{
						Name:   "blue",
						Amount: 1.2,
					},
					{
						Name:   "red",
						Amount: 3.4,
					},
				},
				Trigger: &exportdata.TechnologyV1Trigger{
					Type:   "item",
					Name:   "foo",
					Amount: 5.6,
				},
				Count:           42,
				CountFormula:    "42+n",
				Time:            13.37,
				UnlockedRecipes: []string{"fancy-recipe", "raw-recipe"},
				Level:           21,
				MaxLevel:        1337,
				Order:           "x[fancy]",
				Hidden:          false,
			},
			expectedNumberOfTechnologies: 43,
		},
		"minimal": {
			entity: Technology{
				Name:                     "fancy",
				LocalisedName:            localisedLabel,
				LocalisedDescription:     localisedDescription,
				Prerequisites:            []string{},
				ResearchUnitIngredients:  []TechnologyIngredient{},
				ResearchUnitCount:        0,
				ResearchUnitCountFormula: "",
				ResearchUnitEnergy:       0,
				ResearchTrigger:          nil,
				UnlockedRecipes:          []string{},
				Level:                    1,
				MaxLevel:                 1,
				Order:                    "",
				Hidden:                   true,
			},
			expectTranslation: true,
			expectHash:        true,
			iconHash:          "",
			expectedResult: exportdata.TechnologyV1{
				Name:            "fancy",
				Labels:          labels,
				Descriptions:    descriptions,
				Prerequisites:   make([]string, 0),
				Ingredients:     nil,
				UnlockedRecipes: make([]string, 0),
				Level:           1,
				MaxLevel:        1,
				Hidden:          true,
			},
			expectedNumberOfTechnologies: 43,
		},
		"no entity": {
			entity:                       nil,
			expectTranslation:            false,
			expectHash:                   false,
			iconHash:                     "",
			expectedResult:               nil,
			expectedNumberOfTechnologies: 42,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = NewMetaDataContext(ctx)

			metaData := MetaDataFromContext(ctx)
			metaData.Numbers["technology"] = 42

			line := Line{
				Content: []byte("fancy content"),
				Entity:  test.entity,
			}

			translator := mocks.NewTranslator(t)
			if test.expectTranslation {
				translator.On("Translate", ctx, localisedLabel).Once().Return(labels)
				translator.On("Translate", ctx, localisedDescription).Once().Return(descriptions)
			}

			hashProvider := mocks.NewHashProvider(t)
			if test.expectHash {
				hashProvider.On("HashForEntity", ctx, "technology", "fancy").Once().Return(test.iconHash)
			}

			instance := TechnologyProcessor{
				hashProvider: hashProvider,
				translator:   translator,
			}

			result := instance.Process(ctx, line)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedNumberOfTechnologies, metaData.Numbers["technology"])
		})
	}
}

func TestTechnologyProcessor_Validate(t *testing.T) {
	ctx := context.Background()

	instance := TechnologyProcessor{}
	err := instance.Validate(ctx)

	assert.Nil(t, err)
}
