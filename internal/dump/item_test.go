package dump

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump/mocks"
)

func TestNewItemProcessor(t *testing.T) {
	hashProvider := mocks.NewHashProvider(t)
	translator := mocks.NewTranslator(t)

	instance := NewItemProcessor(hashProvider, translator)

	assert.NotNil(t, instance)
	assert.Same(t, hashProvider, instance.hashProvider)
	assert.Same(t, translator, instance.translator)
}

func TestItemProcessor_Process(t *testing.T) {
	localisedLabel := []any{"label", float64(42)}
	localisedDescription := []any{"description", float64(1337)}
	labels := exportdata.Translations{"en": "abc"}
	descriptions := exportdata.Translations{"en": "def"}

	tests := map[string]struct {
		entity                any
		expectTranslation     bool
		expectHash            bool
		expectedResult        any
		expectedNumberOfItems uint64
	}{
		"full item": {
			entity: Item{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				StackSize:            21,
				Weight:               42000,
				FuelCategory:         "fancy-fuel",
				FuelValue:            1337,
				BurntResultName:      "not-so-fancy",
				SpoilResultName:      "spoilage",
				SpoilTicks:           432000,
				RocketLaunchProducts: []RocketLaunchProduct{
					{
						Name:        "with-amount",
						Amount:      10,
						Probability: 0.75,
					},
					{
						Name:        "with-min-max",
						AmountMin:   2,
						AmountMax:   5,
						Probability: 0.25,
					},
				},
				Order:  "x[fancy]",
				Hidden: false,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.ItemV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				StackSize:    21,
				Weight:       42,
				Burns: &exportdata.ItemV1Burn{
					Category:   "fancy-fuel",
					Value:      1337,
					ResultName: "not-so-fancy",
				},
				Spoils: &exportdata.ItemV1Spoil{
					ResultName: "spoilage",
					Time:       7200,
				},
				RocketLaunchProducts: []exportdata.ItemV1RocketLaunchProduct{
					{
						Name:        "with-amount",
						AmountMin:   10,
						AmountMax:   10,
						Probability: 0.75,
					},
					{
						Name:        "with-min-max",
						AmountMin:   2,
						AmountMax:   5,
						Probability: 0.25,
					},
				},
				Order:  "x[fancy]",
				Hidden: false,
			},
			expectedNumberOfItems: 43,
		},
		"minimal item": {
			entity: Item{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				StackSize:            21,
				Weight:               42000,
				Order:                "x[fancy]",
				Hidden:               true,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.ItemV1{
				Name:                 "fancy",
				Labels:               labels,
				Descriptions:         descriptions,
				Icon:                 "fancy-hash",
				StackSize:            21,
				Weight:               42,
				Burns:                nil,
				Spoils:               nil,
				RocketLaunchProducts: make([]exportdata.ItemV1RocketLaunchProduct, 0),
				Order:                "x[fancy]",
				Hidden:               true,
			},
			expectedNumberOfItems: 43,
		},
		"no entity": {
			entity:                nil,
			expectTranslation:     false,
			expectHash:            false,
			expectedResult:        nil,
			expectedNumberOfItems: 42,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = NewMetaDataContext(ctx)

			metaData := MetaDataFromContext(ctx)
			metaData.Numbers["item"] = 42

			line := Line{
				Content: []byte("fancy content"),
				Entity:  test.entity,
			}

			translator := mocks.NewTranslator(t)
			if test.expectTranslation {
				translator.On("Translate", ctx, localisedLabel).Once().Return(labels)
				translator.On("Translate", ctx, localisedDescription).Once().Return(descriptions)
			}

			hashProvider := mocks.NewHashProvider(t)
			if test.expectHash {
				hashProvider.On("HashForEntity", ctx, "item", "fancy").Once().Return("fancy-hash")
			}

			instance := ItemProcessor{
				hashProvider: hashProvider,
				translator:   translator,
			}

			result := instance.Process(ctx, line)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedNumberOfItems, metaData.Numbers["item"])
		})
	}
}

func TestItemProcessor_Validate(t *testing.T) {
	ctx := context.Background()

	instance := ItemProcessor{}
	err := instance.Validate(ctx)

	assert.Nil(t, err)
}
