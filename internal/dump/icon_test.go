package dump

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump/mocks"
	"gitlab.com/factorio-item-browser/exporter.git/internal/icon"
)

func TestNewIconProcessor(t *testing.T) {
	iconAdder := mocks.NewIconAdder(t)

	instance := NewIconProcessor(iconAdder)

	assert.NotNil(t, instance)
	assert.Same(t, iconAdder, instance.iconAdder)
}

func TestIconProcessor_Process(t *testing.T) {
	dumpIcon := icon.Icon{
		Type: "foo",
		Name: "bar",
	}

	tests := map[string]struct {
		entity     any
		expectIcon bool
	}{
		"with icon": {
			entity:     dumpIcon,
			expectIcon: true,
		},
		"without icon": {
			entity:     Item{},
			expectIcon: false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			line := Line{
				Entity: test.entity,
			}

			iconAdder := mocks.NewIconAdder(t)
			if test.expectIcon {
				iconAdder.On("Add", ctx, dumpIcon).Once()
			}

			instance := IconProcessor{
				iconAdder: iconAdder,
			}
			result := instance.Process(ctx, line)

			assert.Nil(t, result)
		})
	}
}

func TestIconProcessor_Validate(t *testing.T) {
	ctx := context.Background()

	instance := IconProcessor{}
	err := instance.Validate(ctx)

	assert.Nil(t, err)
}
