package dump

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump/mocks"
)

func TestNewFluidProcessor(t *testing.T) {
	hashProvider := mocks.NewHashProvider(t)
	translator := mocks.NewTranslator(t)

	instance := NewFluidProcessor(hashProvider, translator)

	assert.NotNil(t, instance)
	assert.Same(t, hashProvider, instance.hashProvider)
	assert.Same(t, translator, instance.translator)
}

func TestFluidProcessor_Process(t *testing.T) {
	localisedLabel := []any{"label", float64(42)}
	localisedDescription := []any{"description", float64(1337)}
	labels := exportdata.Translations{"en": "abc"}
	descriptions := exportdata.Translations{"en": "def"}

	tests := map[string]struct {
		entity                 any
		expectTranslation      bool
		expectHash             bool
		expectedResult         any
		expectedNumberOfFluids uint64
	}{
		"full fluid": {
			entity: Fluid{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				FuelValue:            1337,
				Order:                "x[fancy]",
				Hidden:               false,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.FluidV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Burns: &exportdata.FluidV1Burn{
					Value: 1337,
				},
				Order:  "x[fancy]",
				Hidden: false,
			},
			expectedNumberOfFluids: 43,
		},
		"minimal fluid": {
			entity: Fluid{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				FuelValue:            0,
				Order:                "x[fancy]",
				Hidden:               false,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.FluidV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Burns:        nil,
				Order:        "x[fancy]",
				Hidden:       false,
			},
			expectedNumberOfFluids: 43,
		},
		"no entity": {
			entity:                 nil,
			expectTranslation:      false,
			expectHash:             false,
			expectedResult:         nil,
			expectedNumberOfFluids: 42,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = NewMetaDataContext(ctx)

			metaData := MetaDataFromContext(ctx)
			metaData.Numbers["fluid"] = 42

			line := Line{
				Content: []byte("fancy content"),
				Entity:  test.entity,
			}

			translator := mocks.NewTranslator(t)
			if test.expectTranslation {
				translator.On("Translate", ctx, localisedLabel).Once().Return(labels)
				translator.On("Translate", ctx, localisedDescription).Once().Return(descriptions)
			}

			hashProvider := mocks.NewHashProvider(t)
			if test.expectHash {
				hashProvider.On("HashForEntity", ctx, "fluid", "fancy").Once().Return("fancy-hash")
			}

			instance := FluidProcessor{
				hashProvider: hashProvider,
				translator:   translator,
			}

			result := instance.Process(ctx, line)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedNumberOfFluids, metaData.Numbers["fluid"])
		})
	}
}

func TestFluidProcessor_Validate(t *testing.T) {
	ctx := context.Background()

	instance := FluidProcessor{}
	err := instance.Validate(ctx)

	assert.Nil(t, err)
}
