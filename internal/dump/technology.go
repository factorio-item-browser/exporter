package dump

import (
	"context"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
)

type Technology struct {
	Name                     string                 `json:"name"`
	LocalisedName            any                    `json:"localised_name"`
	LocalisedDescription     any                    `json:"localised_description"`
	Prerequisites            []string               `json:"prerequisites"`
	ResearchUnitIngredients  []TechnologyIngredient `json:"research_unit_ingredients"`
	ResearchUnitCount        int64                  `json:"research_unit_count"`
	ResearchUnitCountFormula string                 `json:"research_unit_count_formula"`
	ResearchUnitEnergy       float64                `json:"research_unit_energy"`
	ResearchTrigger          *TechnologyTrigger     `json:"research_trigger"`
	UnlockedRecipes          []string               `json:"unlocked_recipes"`
	Level                    int64                  `json:"level"`
	MaxLevel                 int64                  `json:"max_level"`
	Order                    string                 `json:"order"`
	Hidden                   bool                   `json:"hidden"`
}

type TechnologyIngredient struct {
	Name   string  `json:"name"`
	Amount float64 `json:"amount"`
}

type TechnologyTrigger struct {
	Type   string  `json:"type"`
	Name   string  `json:"name"`
	Amount float64 `json:"amount"`
}

type TechnologyProcessor struct {
	hashProvider hashProvider
	translator   translator
}

func NewTechnologyProcessor(hashProvider hashProvider, translator translator) *TechnologyProcessor {
	return &TechnologyProcessor{
		hashProvider: hashProvider,
		translator:   translator,
	}
}

func (p *TechnologyProcessor) Process(ctx context.Context, line Line) any {
	technology, ok := line.Entity.(Technology)
	if !ok {
		return nil
	}

	exportTechnology := exportdata.TechnologyV1{
		Name:            technology.Name,
		Labels:          p.translator.Translate(ctx, technology.LocalisedName),
		Descriptions:    p.translator.Translate(ctx, technology.LocalisedDescription),
		Icon:            p.hashProvider.HashForEntity(ctx, "technology", technology.Name),
		Prerequisites:   technology.Prerequisites,
		Count:           technology.ResearchUnitCount,
		CountFormula:    technology.ResearchUnitCountFormula,
		Time:            roundFloat(technology.ResearchUnitEnergy / 60),
		UnlockedRecipes: technology.UnlockedRecipes,
		Level:           technology.Level,
		MaxLevel:        technology.MaxLevel,
		Order:           technology.Order,
		Hidden:          technology.Hidden,
	}

	if technology.ResearchTrigger != nil {
		exportTechnology.Trigger = &exportdata.TechnologyV1Trigger{
			Type:   technology.ResearchTrigger.Type,
			Name:   technology.ResearchTrigger.Name,
			Amount: roundFloat(technology.ResearchTrigger.Amount),
		}
	}

	if len(technology.ResearchUnitIngredients) > 0 {
		exportTechnology.Ingredients = make([]exportdata.TechnologyV1Ingredient, 0, len(technology.ResearchUnitIngredients))
		for _, ingredient := range technology.ResearchUnitIngredients {
			exportTechnology.Ingredients = append(exportTechnology.Ingredients, exportdata.TechnologyV1Ingredient{
				Name:   ingredient.Name,
				Amount: ingredient.Amount,
			})
		}
	}

	MetaDataFromContext(ctx).IncreaseNumber("technology")
	return exportTechnology
}

func (p *TechnologyProcessor) Validate(_ context.Context) error {
	return nil
}
