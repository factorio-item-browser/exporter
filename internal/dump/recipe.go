package dump

import (
	"context"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
)

type Recipe struct {
	Name                 string                   `json:"name"`
	LocalisedName        any                      `json:"localised_name"`
	LocalisedDescription any                      `json:"localised_description"`
	Category             string                   `json:"category"`
	Time                 float64                  `json:"time"`
	Ingredients          []RecipeIngredient       `json:"ingredients"`
	Products             []RecipeProduct          `json:"products"`
	MainProduct          *RecipeProduct           `json:"main_product"`
	SurfaceConditions    []RecipeSurfaceCondition `json:"surface_conditions"`
	Order                string                   `json:"order"`
	Enabled              bool                     `json:"enabled"`
	Hidden               bool                     `json:"hidden"`
}

type RecipeIngredient struct {
	Type   string  `json:"type"`
	Name   string  `json:"name"`
	Amount float64 `json:"amount"`
}

type RecipeProduct struct {
	Type               string  `json:"type"`
	Name               string  `json:"name"`
	Amount             float64 `json:"amount"`
	AmountMin          float64 `json:"amount_min"`
	AmountMax          float64 `json:"amount_max"`
	Probability        float64 `json:"probability"`
	ExtraCountFraction float64 `json:"extra_count_fraction"`
}

type RecipeSurfaceCondition struct {
	Property string   `json:"property"`
	Min      *float64 `json:"min"`
	Max      *float64 `json:"max"`
}

type RecipeProcessor struct {
	hashProvider hashProvider
	translator   translator
}

func NewRecipeProcessor(hashProvider hashProvider, translator translator) *RecipeProcessor {
	return &RecipeProcessor{
		hashProvider: hashProvider,
		translator:   translator,
	}
}

func (p *RecipeProcessor) Process(ctx context.Context, line Line) any {
	recipe, ok := line.Entity.(Recipe)
	if !ok {
		return nil
	}

	exportRecipe := exportdata.RecipeV1{
		Name:         recipe.Name,
		Labels:       p.translator.Translate(ctx, recipe.LocalisedName),
		Descriptions: p.translator.Translate(ctx, recipe.LocalisedDescription),
		Icon:         p.hashProvider.HashForEntity(ctx, "recipe", recipe.Name),
		Category:     recipe.Category,
		Time:         roundFloat(recipe.Time),
		Ingredients:  make([]exportdata.RecipeV1Ingredient, 0, len(recipe.Ingredients)),
		Products:     make([]exportdata.RecipeV1Product, 0, len(recipe.Products)),
		Conditions:   make(map[string]exportdata.RecipeV1Condition, len(recipe.SurfaceConditions)),
		Order:        recipe.Order,
		Enabled:      recipe.Enabled,
		Hidden:       recipe.Hidden,
	}

	for _, ingredient := range recipe.Ingredients {
		exportRecipe.Ingredients = append(exportRecipe.Ingredients, exportdata.RecipeV1Ingredient{
			Type:   ingredient.Type,
			Name:   ingredient.Name,
			Amount: roundFloat(ingredient.Amount),
		})
	}

	for _, product := range recipe.Products {
		mappedProduct := exportdata.RecipeV1Product{
			Type:        product.Type,
			Name:        product.Name,
			AmountMin:   roundFloat(product.AmountMin),
			AmountMax:   roundFloat(product.AmountMax),
			Probability: roundFloat(product.Probability),
			Extra:       roundFloat(product.ExtraCountFraction),
		}
		if product.Amount > 0 {
			mappedProduct.AmountMin = roundFloat(product.Amount)
			mappedProduct.AmountMax = roundFloat(product.Amount)
		}
		exportRecipe.Products = append(exportRecipe.Products, mappedProduct)
	}

	if exportRecipe.Icon == "" && recipe.MainProduct != nil {
		exportRecipe.Icon = p.hashProvider.HashForEntity(ctx, recipe.MainProduct.Type, recipe.MainProduct.Name)
	}
	if exportRecipe.Icon == "" && len(exportRecipe.Products) > 0 {
		exportRecipe.Icon =
			p.hashProvider.HashForEntity(ctx, exportRecipe.Products[0].Type, exportRecipe.Products[0].Name)
	}

	for _, condition := range recipe.SurfaceConditions {
		exportCondition := exportdata.RecipeV1Condition{
			Min: -exportdata.Infinity,
			Max: exportdata.Infinity,
		}

		if condition.Min != nil {
			exportCondition.Min = *condition.Min
		}
		if condition.Max != nil {
			exportCondition.Max = *condition.Max
		}

		exportRecipe.Conditions[condition.Property] = exportCondition
	}

	MetaDataFromContext(ctx).IncreaseNumber("recipe")
	return exportRecipe
}

func (p *RecipeProcessor) Validate(_ context.Context) error {
	return nil
}
