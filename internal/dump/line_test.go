package dump

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/icon"
)

func TestNewLineReader(t *testing.T) {
	buffer := bytes.NewBuffer(nil)

	instance := NewLineReader(buffer)

	assert.NotNil(t, instance)
	assert.Same(t, buffer, instance.reader)
}

func TestLineReader_Read(t *testing.T) {
	defer func(ref1 func(*LineReader, []byte) Line) {
		readLine = ref1
	}(readLine)

	content := "[abc def]\n{ghi jkl}\n"
	line1 := Line{Content: []byte("[abc def]")}
	line2 := Line{Content: []byte("{ghi jkl}")}
	expectedContents := map[string]Line{
		"[abc def]": line1,
		"{ghi jkl}": line2,
	}

	reader := strings.NewReader(content)

	instance := LineReader{
		reader: reader,
	}

	readLine = func(i *LineReader, c []byte) Line {
		assert.Same(t, &instance, i)

		line, ok := expectedContents[string(c)]
		if !ok {
			assert.Fail(t, fmt.Sprintf(`unexpected call to readLine with content "%s"`, string(c)))
		}

		delete(expectedContents, string(c))
		return line
	}

	result := instance.Read()

	assert.Equal(t, line1, <-result)
	assert.Equal(t, line2, <-result)

	_, ok := <-result
	assert.False(t, ok)

	assert.Empty(t, expectedContents)
}

func TestLineReader_readLine(t *testing.T) {
	tests := map[string]struct {
		content        string
		expectedEntity any
	}{
		"fluid": {
			content: `>DUMP>fluid>{"name":"fancy"}<`,
			expectedEntity: Fluid{
				Name: "fancy",
			},
		},
		"icon": {
			content: `>DUMP>icon>{"type":"icon","name":"fancy"}<`,
			expectedEntity: icon.Icon{
				Type: "icon",
				Name: "fancy",
			},
		},
		"item": {
			content: `>DUMP>item>{"name":"fancy"}<`,
			expectedEntity: Item{
				Name: "fancy",
			},
		},
		"machine": {
			content: `>DUMP>machine>{"name":"fancy"}<`,
			expectedEntity: Machine{
				Name: "fancy",
			},
		},
		"recipe": {
			content: `>DUMP>recipe>{"name":"fancy"}<`,
			expectedEntity: Recipe{
				Name: "fancy",
			},
		},
		"resource": {
			content: `>DUMP>resource>{"name":"fancy"}<`,
			expectedEntity: Resource{
				Name: "fancy",
			},
		},
		"surface": {
			content: `>DUMP>surface>{"name":"fancy"}<`,
			expectedEntity: Surface{
				Name: "fancy",
			},
		},
		"technology": {
			content: `>DUMP>technology>{"name":"fancy"}<`,
			expectedEntity: Technology{
				Name: "fancy",
			},
		},
		"no dump": {
			content:        "some fancy line",
			expectedEntity: nil,
		},
		"wrong structure": {
			content:        ">DUMP>test>invalid",
			expectedEntity: nil,
		},
		"invalid JSON": {
			content:        `>DUMP>test>{"invalid<`,
			expectedEntity: nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			expectedResult := Line{
				Content: []byte(test.content),
				Entity:  test.expectedEntity,
			}

			instance := LineReader{}
			result := instance.readLine([]byte(test.content))

			assert.Equal(t, expectedResult, result)
		})
	}
}
