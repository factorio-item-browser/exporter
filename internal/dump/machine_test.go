package dump

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump/mocks"
)

func TestNewMachineProcessor(t *testing.T) {
	hashProvider := mocks.NewHashProvider(t)
	translator := mocks.NewTranslator(t)

	instance := NewMachineProcessor(hashProvider, translator)

	assert.NotNil(t, instance)
	assert.Same(t, hashProvider, instance.hashProvider)
	assert.Same(t, translator, instance.translator)
}

func TestMachineProcessor_Process(t *testing.T) {
	localisedLabel := []any{"label", float64(42)}
	localisedDescription := []any{"description", float64(1337)}
	labels := exportdata.Translations{"en": "abc"}
	descriptions := exportdata.Translations{"en": "def"}

	tests := map[string]struct {
		entity                   any
		expectTranslation        bool
		expectHash               bool
		expectedResult           any
		expectedNumberOfMachines uint64
	}{
		"full": {
			entity: Machine{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				ModuleInventorySize:  12,
				EnergyUsage:          22.2833333,
				Order:                "x[fancy]",
				Hidden:               false,
				CraftingCategories: map[string]bool{
					"crafting-1": true,
					"invalid":    false,
				},
				CraftingSpeed:   4.2,
				IngredientCount: 21,
				FluidBoxes: []MachineFluidBox{
					{ProductionType: "input"},
					{ProductionType: "input"},
					{ProductionType: "output"},
					{ProductionType: "input-output"},
				},
				ResourceCategories: map[string]bool{
					"resource-1": true,
					"invalid":    false,
				},
				MiningSpeed: 73.31,
				FuelCategories: map[string]bool{
					"fuel-1":  true,
					"invalid": false,
				},
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.MachineV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				ModuleSlots:  12,
				EnergyUsage:  1337,
				Crafting: &exportdata.MachineV1Crafting{
					Categories:       []string{"crafting-1"},
					Speed:            4.2,
					ItemSlots:        21,
					FluidInputSlots:  3,
					FluidOutputSlots: 2,
				},
				Mining: &exportdata.MachineV1Mining{
					Categories: []string{"resource-1"},
					Speed:      73.31,
				},
				Burning: &exportdata.MachineV1Burning{
					Categories: []string{"fuel-1"},
				},
				Order:  "x[fancy]",
				Hidden: false,
			},
			expectedNumberOfMachines: 43,
		},
		"minimal": {
			entity: Machine{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				ModuleInventorySize:  12,
				EnergyUsage:          22.2833333,
				Order:                "",
				Hidden:               true,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.MachineV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				ModuleSlots:  12,
				EnergyUsage:  1337,
				Crafting:     nil,
				Mining:       nil,
				Burning:      nil,
				Order:        "",
				Hidden:       true,
			},
			expectedNumberOfMachines: 43,
		},
		"character": {
			entity: Machine{
				Name:                 "character",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				ModuleInventorySize:  0,
				EnergyUsage:          0,
				Order:                "x[fancy]",
				Hidden:               false,
				CraftingCategories: map[string]bool{
					"crafting-1": true,
				},
				CraftingSpeed:   0,
				IngredientCount: 0,
				ResourceCategories: map[string]bool{
					"resource-1": true,
				},
				MiningSpeed: 0.5,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.MachineV1{
				Name:         "character",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				ModuleSlots:  0,
				EnergyUsage:  0,
				Crafting: &exportdata.MachineV1Crafting{
					Categories: []string{"crafting-1"},
					Speed:      1,
					ItemSlots:  255,
				},
				Mining: &exportdata.MachineV1Mining{
					Categories: []string{"resource-1"},
					Speed:      0.5,
				},
				Burning: nil,
				Order:   "x[fancy]",
				Hidden:  false,
			},
			expectedNumberOfMachines: 43,
		},
		"no entity": {
			entity:                   nil,
			expectTranslation:        false,
			expectHash:               false,
			expectedResult:           nil,
			expectedNumberOfMachines: 42,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = NewMetaDataContext(ctx)

			metaData := MetaDataFromContext(ctx)
			metaData.Numbers["machine"] = 42

			line := Line{
				Content: []byte("fancy content"),
				Entity:  test.entity,
			}

			translator := mocks.NewTranslator(t)
			if test.expectTranslation {
				translator.On("Translate", ctx, localisedLabel).Once().Return(labels)
				translator.On("Translate", ctx, localisedDescription).Once().Return(descriptions)
			}

			hashProvider := mocks.NewHashProvider(t)
			if test.expectHash {
				hashProvider.On("HashForEntity", ctx, "machine", test.entity.(Machine).Name).Once().Return("fancy-hash")
			}

			instance := MachineProcessor{
				hashProvider: hashProvider,
				translator:   translator,
			}

			result := instance.Process(ctx, line)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedNumberOfMachines, metaData.Numbers["machine"])
		})
	}
}

func TestMachineProcessor_Validate(t *testing.T) {
	ctx := context.Background()

	instance := MachineProcessor{}
	err := instance.Validate(ctx)

	assert.Nil(t, err)
}
