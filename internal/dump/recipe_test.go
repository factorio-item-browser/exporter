package dump

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump/mocks"
)

func TestNewRecipeProcessor(t *testing.T) {
	hashProvider := mocks.NewHashProvider(t)
	translator := mocks.NewTranslator(t)

	instance := NewRecipeProcessor(hashProvider, translator)

	assert.NotNil(t, instance)
	assert.Same(t, hashProvider, instance.hashProvider)
	assert.Same(t, translator, instance.translator)
}

func TestRecipeProcessor_Process(t *testing.T) {
	valueMin := 2.1
	valueMax := 4.2

	type hashCall struct {
		expectedType string
		expectedName string
		hash         string
	}

	localisedLabel := []any{"label", float64(42)}
	localisedDescription := []any{"description", float64(1337)}
	labels := exportdata.Translations{"en": "abc"}
	descriptions := exportdata.Translations{"en": "def"}

	tests := map[string]struct {
		entity                  any
		expectTranslation       bool
		expectedHashCalls       []hashCall
		expectedResult          any
		expectedNumberOfRecipes uint64
	}{
		"full": {
			entity: Recipe{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				Category:             "foo",
				Time:                 13.37,
				Ingredients: []RecipeIngredient{
					{
						Type:   "item",
						Name:   "thing",
						Amount: 1.2,
					},
					{
						Type:   "fluid",
						Name:   "waste",
						Amount: 2.3,
					},
				},
				Products: []RecipeProduct{
					{
						Type:               "item",
						Name:               "with-amount",
						Amount:             3.4,
						Probability:        4.5,
						ExtraCountFraction: 5.6,
					},
					{
						Type:               "item",
						Name:               "with-min-max",
						AmountMin:          6.7,
						AmountMax:          7.8,
						Probability:        8.9,
						ExtraCountFraction: 9.0,
					},
				},
				MainProduct: nil,
				SurfaceConditions: []RecipeSurfaceCondition{
					{
						Property: "values",
						Min:      &valueMin,
						Max:      &valueMax,
					},
					{
						Property: "infinity",
					},
				},
				Order:   "x[fancy]",
				Enabled: false,
				Hidden:  true,
			},
			expectTranslation: true,
			expectedHashCalls: []hashCall{
				{
					expectedType: "recipe",
					expectedName: "fancy",
					hash:         "fancy-hash",
				},
			},
			expectedResult: exportdata.RecipeV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Category:     "foo",
				Time:         13.37,
				Ingredients: []exportdata.RecipeV1Ingredient{
					{
						Type:   "item",
						Name:   "thing",
						Amount: 1.2,
					},
					{
						Type:   "fluid",
						Name:   "waste",
						Amount: 2.3,
					},
				},
				Products: []exportdata.RecipeV1Product{
					{
						Type:        "item",
						Name:        "with-amount",
						AmountMin:   3.4,
						AmountMax:   3.4,
						Probability: 4.5,
						Extra:       5.6,
					},
					{
						Type:        "item",
						Name:        "with-min-max",
						AmountMin:   6.7,
						AmountMax:   7.8,
						Probability: 8.9,
						Extra:       9.0,
					},
				},
				Conditions: map[string]exportdata.RecipeV1Condition{
					"values": {
						Min: valueMin,
						Max: valueMax,
					},
					"infinity": {
						Min: -exportdata.Infinity,
						Max: exportdata.Infinity,
					},
				},
				Order:   "x[fancy]",
				Enabled: false,
				Hidden:  true,
			},
			expectedNumberOfRecipes: 43,
		},
		"with main product icon": {
			entity: Recipe{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				Category:             "foo",
				Time:                 13.37,
				Ingredients: []RecipeIngredient{
					{
						Type:   "item",
						Name:   "thing",
						Amount: 1.2,
					},
					{
						Type:   "fluid",
						Name:   "waste",
						Amount: 2.3,
					},
				},
				Products: []RecipeProduct{
					{
						Type:               "item",
						Name:               "with-amount",
						Amount:             3.4,
						Probability:        4.5,
						ExtraCountFraction: 5.6,
					},
					{
						Type:               "item",
						Name:               "with-min-max",
						AmountMin:          6.7,
						AmountMax:          7.8,
						Probability:        8.9,
						ExtraCountFraction: 9.0,
					},
				},
				MainProduct: &RecipeProduct{
					Type:               "item",
					Name:               "with-min-max",
					AmountMin:          6.7,
					AmountMax:          7.8,
					Probability:        8.9,
					ExtraCountFraction: 9.0,
				},
				Order:   "x[fancy]",
				Enabled: false,
				Hidden:  true,
			},
			expectTranslation: true,
			expectedHashCalls: []hashCall{
				{
					expectedType: "recipe",
					expectedName: "fancy",
					hash:         "",
				},
				{
					expectedType: "item",
					expectedName: "with-min-max",
					hash:         "fancy-hash",
				},
			},
			expectedResult: exportdata.RecipeV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Category:     "foo",
				Time:         13.37,
				Ingredients: []exportdata.RecipeV1Ingredient{
					{
						Type:   "item",
						Name:   "thing",
						Amount: 1.2,
					},
					{
						Type:   "fluid",
						Name:   "waste",
						Amount: 2.3,
					},
				},
				Products: []exportdata.RecipeV1Product{
					{
						Type:        "item",
						Name:        "with-amount",
						AmountMin:   3.4,
						AmountMax:   3.4,
						Probability: 4.5,
						Extra:       5.6,
					},
					{
						Type:        "item",
						Name:        "with-min-max",
						AmountMin:   6.7,
						AmountMax:   7.8,
						Probability: 8.9,
						Extra:       9.0,
					},
				},
				Conditions: make(map[string]exportdata.RecipeV1Condition),
				Order:      "x[fancy]",
				Enabled:    false,
				Hidden:     true,
			},
			expectedNumberOfRecipes: 43,
		},
		"with first product icon": {
			entity: Recipe{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				Category:             "foo",
				Time:                 13.37,
				Ingredients: []RecipeIngredient{
					{
						Type:   "item",
						Name:   "thing",
						Amount: 1.2,
					},
					{
						Type:   "fluid",
						Name:   "waste",
						Amount: 2.3,
					},
				},
				Products: []RecipeProduct{
					{
						Type:               "item",
						Name:               "with-amount",
						Amount:             3.4,
						Probability:        4.5,
						ExtraCountFraction: 5.6,
					},
					{
						Type:               "item",
						Name:               "with-min-max",
						AmountMin:          6.7,
						AmountMax:          7.8,
						Probability:        8.9,
						ExtraCountFraction: 9.0,
					},
				},
				MainProduct: &RecipeProduct{
					Type:               "item",
					Name:               "with-min-max",
					AmountMin:          6.7,
					AmountMax:          7.8,
					Probability:        8.9,
					ExtraCountFraction: 9.0,
				},
				Order:   "x[fancy]",
				Enabled: false,
				Hidden:  true,
			},
			expectTranslation: true,
			expectedHashCalls: []hashCall{
				{
					expectedType: "recipe",
					expectedName: "fancy",
					hash:         "",
				},
				{
					expectedType: "item",
					expectedName: "with-min-max",
					hash:         "",
				},
				{
					expectedType: "item",
					expectedName: "with-amount",
					hash:         "fancy-hash",
				},
			},
			expectedResult: exportdata.RecipeV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Category:     "foo",
				Time:         13.37,
				Ingredients: []exportdata.RecipeV1Ingredient{
					{
						Type:   "item",
						Name:   "thing",
						Amount: 1.2,
					},
					{
						Type:   "fluid",
						Name:   "waste",
						Amount: 2.3,
					},
				},
				Products: []exportdata.RecipeV1Product{
					{
						Type:        "item",
						Name:        "with-amount",
						AmountMin:   3.4,
						AmountMax:   3.4,
						Probability: 4.5,
						Extra:       5.6,
					},
					{
						Type:        "item",
						Name:        "with-min-max",
						AmountMin:   6.7,
						AmountMax:   7.8,
						Probability: 8.9,
						Extra:       9.0,
					},
				},
				Conditions: make(map[string]exportdata.RecipeV1Condition),
				Order:      "x[fancy]",
				Enabled:    false,
				Hidden:     true,
			},
			expectedNumberOfRecipes: 43,
		},
		"no entity": {
			entity:                  nil,
			expectTranslation:       false,
			expectedHashCalls:       []hashCall{},
			expectedResult:          nil,
			expectedNumberOfRecipes: 42,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = NewMetaDataContext(ctx)

			metaData := MetaDataFromContext(ctx)
			metaData.Numbers["recipe"] = 42

			line := Line{
				Content: []byte("fancy content"),
				Entity:  test.entity,
			}

			translator := mocks.NewTranslator(t)
			if test.expectTranslation {
				translator.On("Translate", ctx, localisedLabel).Once().Return(labels)
				translator.On("Translate", ctx, localisedDescription).Once().Return(descriptions)
			}

			hashProvider := mocks.NewHashProvider(t)
			for _, call := range test.expectedHashCalls {
				hashProvider.On("HashForEntity", ctx, call.expectedType, call.expectedName).Once().Return(call.hash)
			}

			instance := RecipeProcessor{
				hashProvider: hashProvider,
				translator:   translator,
			}

			result := instance.Process(ctx, line)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedNumberOfRecipes, metaData.Numbers["recipe"])
		})
	}
}

func TestRecipeProcessor_Validate(t *testing.T) {
	ctx := context.Background()

	instance := RecipeProcessor{}
	err := instance.Validate(ctx)

	assert.Nil(t, err)
}
