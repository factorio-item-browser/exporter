package dump

import (
	"context"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
)

type Resource struct {
	Name                 string            `json:"name"`
	LocalisedName        any               `json:"localised_name"`
	LocalisedDescription any               `json:"localised_description"`
	ResourceCategory     string            `json:"resource_category"`
	MiningTime           float64           `json:"mining_time"`
	RequiredFluid        string            `json:"required_fluid"`
	FluidAmount          float64           `json:"fluid_amount"`
	Products             []ResourceProduct `json:"products"`
	Order                string            `json:"order"`
	Hidden               bool              `json:"hidden"`
}

type ResourceProduct struct {
	Type        string  `json:"type"`
	Name        string  `json:"name"`
	Amount      float64 `json:"amount"`
	AmountMin   float64 `json:"amount_min"`
	AmountMax   float64 `json:"amount_max"`
	Probability float64 `json:"probability"`
}

type ResourceProcessor struct {
	hashProvider hashProvider
	translator   translator
}

func NewResourceProcessor(hashProvider hashProvider, translator translator) *ResourceProcessor {
	return &ResourceProcessor{
		hashProvider: hashProvider,
		translator:   translator,
	}
}

func (p *ResourceProcessor) Process(ctx context.Context, line Line) any {
	resource, ok := line.Entity.(Resource)
	if !ok {
		return nil
	}

	exportResource := exportdata.ResourceV1{
		Name:         resource.Name,
		Labels:       p.translator.Translate(ctx, resource.LocalisedName),
		Descriptions: p.translator.Translate(ctx, resource.LocalisedDescription),
		Icon:         p.hashProvider.HashForEntity(ctx, "resource", resource.Name),
		Category:     resource.ResourceCategory,
		Time:         roundFloat(resource.MiningTime),
		Products:     make([]exportdata.ResourceV1Product, 0, len(resource.Products)),
		Order:        resource.Order,
		Hidden:       resource.Hidden,
	}

	if resource.FluidAmount > 0 {
		exportResource.Fluid = &exportdata.ResourceV1Fluid{
			Name:   resource.RequiredFluid,
			Amount: roundFloat(resource.FluidAmount),
		}
	}

	for _, product := range resource.Products {
		mappedProduct := exportdata.ResourceV1Product{
			Type:        product.Type,
			Name:        product.Name,
			AmountMin:   roundFloat(product.AmountMin),
			AmountMax:   roundFloat(product.AmountMax),
			Probability: roundFloat(product.Probability),
		}
		if product.Amount > 0 {
			mappedProduct.AmountMin = roundFloat(product.Amount)
			mappedProduct.AmountMax = roundFloat(product.Amount)
		}
		exportResource.Products = append(exportResource.Products, mappedProduct)
	}

	MetaDataFromContext(ctx).IncreaseNumber("resource")
	return exportResource
}

func (p *ResourceProcessor) Validate(_ context.Context) error {
	return nil
}
