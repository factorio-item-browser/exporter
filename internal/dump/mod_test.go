package dump

import (
	"bytes"
	"context"
	"fmt"
	"image"
	"image/png"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump/mocks"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/icon"
	instancetest "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	renderer "gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
)

func TestNewModProcessor(t *testing.T) {
	iconManager := mocks.NewIconManager(t)
	translator := mocks.NewFullTranslator(t)

	instance := NewModProcessor(iconManager, translator)

	assert.NotNil(t, instance)
	assert.Same(t, iconManager, instance.iconManager)
	assert.Same(t, translator, instance.translator)
	assert.NotNil(t, instance.modVersions)
}

func TestModProcessor_Process(t *testing.T) {
	labels := exportdata.Translations{"en": "abc"}
	descriptions := exportdata.Translations{"en": "def"}

	tests := map[string]struct {
		line                     string
		loadedCore               bool
		infoJson                 string
		expectLoadCore           bool
		expectLoadMod            bool
		expectThumbnailIcon      bool
		expectedResult           any
		expectedMetaDataVersions map[string]string
		expectedNumberOfMods     uint64
	}{
		"with mod": {
			line:                "  0.1337 Checksum of foo: 1337",
			loadedCore:          true,
			infoJson:            `{"name":"foo","title":"Mod of Foo","version":"1.2.3"}`,
			expectLoadCore:      false,
			expectLoadMod:       true,
			expectThumbnailIcon: true,
			expectedResult: exportdata.ModV1{
				Name:         "foo",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Version:      "1.2.3",
			},
			expectedMetaDataVersions: map[string]string{
				"bar": "3.2.1",
				"foo": "1.2.3",
			},
			expectedNumberOfMods: 43,
		},
		"load core": {
			line:                "fancy line without mod",
			loadedCore:          false,
			infoJson:            "",
			expectLoadCore:      true,
			expectLoadMod:       false,
			expectThumbnailIcon: false,
			expectedResult:      nil,
			expectedMetaDataVersions: map[string]string{
				"bar": "3.2.1",
			},
			expectedNumberOfMods: 42,
		},
		"loaded core": {
			line:                "fancy line without mod",
			loadedCore:          true,
			infoJson:            "",
			expectLoadCore:      false,
			expectLoadMod:       false,
			expectThumbnailIcon: false,
			expectedResult:      nil,
			expectedMetaDataVersions: map[string]string{
				"bar": "3.2.1",
			},
			expectedNumberOfMods: 42,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instanceBuilder := instancetest.NewMockedInstanceBuilder()
			instanceBuilder.WithMod("foo").WithFile("info.json", test.infoJson)

			ctx := context.Background()
			ctx = instanceBuilder.BuildContext(ctx)
			ctx = NewMetaDataContext(ctx)

			metaData := MetaDataFromContext(ctx)
			metaData.ModVersions = map[string]string{
				"bar": "3.2.1",
			}
			metaData.Numbers["mod"] = 42

			line := Line{
				Content: []byte(test.line),
			}
			thumbnail := icon.Icon{
				Type: "mod",
				Name: "foo",
				FactorioIcon: renderer.FactorioIcon{
					Icon:     "__foo__/thumbnail.png",
					IconSize: 144,
				},
			}

			iconManager := mocks.NewIconManager(t)
			if test.expectThumbnailIcon {
				iconManager.On("Add", ctx, thumbnail).Once()
			}
			iconManager.On("HashForEntity", ctx, "mod", "foo").Maybe().Return("fancy-hash")

			translator := mocks.NewFullTranslator(t)
			if test.expectLoadCore {
				translator.On("LoadMod", ctx, "core").Once().Return(nil)
			}
			if test.expectLoadMod {
				translator.On("LoadMod", ctx, "foo").Once().Return(nil)
			}
			translator.On("TranslateWithFallback", ctx, []any{"mod-name.foo"}, mock.Anything).Maybe().Return(labels)
			translator.On("TranslateWithFallback", ctx, []any{"mod-description.foo"}, mock.Anything).Maybe().Return(descriptions)

			instance := ModProcessor{
				iconManager: iconManager,
				translator:  translator,
				loadedCore:  test.loadedCore,
			}
			result := instance.Process(ctx, line)

			assert.Equal(t, test.expectedResult, result)
			assert.True(t, instance.loadedCore)
			assert.Equal(t, test.expectedMetaDataVersions, metaData.ModVersions)
			assert.Equal(t, test.expectedNumberOfMods, metaData.Numbers["mod"])
		})
	}
}

func TestModProcessor_readModInfo(t *testing.T) {
	tests := map[string]struct {
		line                           string
		seenDumperMod                  bool
		infoJson                       string
		expectedSeenDumperMod          bool
		expectedSeenModsAfterDumperMod bool
		expectedResult                 *instancetest.ModInfo
	}{
		"happy path": {
			line:                           "  0.1337 Checksum of foo: 1337",
			seenDumperMod:                  false,
			infoJson:                       `{"name":"foo","title":"Mod of Foo"}`,
			expectedSeenDumperMod:          false,
			expectedSeenModsAfterDumperMod: false,
			expectedResult: &instancetest.ModInfo{
				Name:  "foo",
				Title: "Mod of Foo",
			},
		},
		"mod info error": {
			line:                           "  0.1337 Checksum of foo: 1337",
			seenDumperMod:                  false,
			infoJson:                       "",
			expectedSeenDumperMod:          false,
			expectedSeenModsAfterDumperMod: false,
			expectedResult:                 nil,
		},
		"dumper mod": {
			line:                           "  0.1337 Checksum of dumper: 1337",
			seenDumperMod:                  false,
			infoJson:                       "",
			expectedSeenDumperMod:          true,
			expectedSeenModsAfterDumperMod: false,
			expectedResult:                 nil,
		},
		"mod after dumper": {
			line:                           "  0.1337 Checksum of foo: 1337",
			seenDumperMod:                  true,
			infoJson:                       "",
			expectedSeenDumperMod:          true,
			expectedSeenModsAfterDumperMod: true,
			expectedResult:                 nil,
		},
		"without match": {
			line:                           "fancy line without mod",
			seenDumperMod:                  false,
			infoJson:                       "",
			expectedSeenDumperMod:          false,
			expectedSeenModsAfterDumperMod: false,
			expectedResult:                 nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instanceBuilder := instancetest.NewMockedInstanceBuilder()
			mod := instanceBuilder.WithMod("foo")
			if test.infoJson != "" {
				mod.WithFile("info.json", test.infoJson)
			}

			ctx := context.Background()
			ctx = instanceBuilder.BuildContext(ctx)

			line := Line{
				Content: []byte(test.line),
			}

			instance := ModProcessor{
				seenDumperMod: test.seenDumperMod,
			}
			result := instance.readModInfo(ctx, line)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedSeenDumperMod, instance.seenDumperMod)
			assert.Equal(t, test.expectedSeenModsAfterDumperMod, instance.seenModsAfterDumperMod)
		})
	}
}

func TestModProcessor_loadTranslations(t *testing.T) {
	tests := map[string]struct {
		loadModError error
	}{
		"happy path": {
			loadModError: nil,
		},
		"with error": {
			loadModError: fmt.Errorf("test error"),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			modName := "foo"

			translator := mocks.NewFullTranslator(t)
			translator.On("LoadMod", ctx, modName).Once().Return(test.loadModError)

			instance := ModProcessor{
				translator: translator,
			}
			instance.loadTranslations(ctx, modName)
		})
	}
}

func TestModProcessor_createThumbnailIcon(t *testing.T) {
	createImage := func(size int) string {
		buffer := bytes.NewBuffer(nil)
		img := image.NewRGBA(image.Rect(0, 0, size, size))
		_ = png.Encode(buffer, img)
		return buffer.String()
	}

	tests := map[string]struct {
		thumbnail      string
		expectedResult icon.Icon
	}{
		"happy path": {
			thumbnail: createImage(288),
			expectedResult: icon.Icon{
				Type: "mod",
				Name: "foo",
				FactorioIcon: renderer.FactorioIcon{
					Icon:     "__foo__/thumbnail.png",
					IconSize: 288,
				},
			},
		},
		"invalid thumbnail": {
			thumbnail: "invalid",
			expectedResult: icon.Icon{
				Type: "mod",
				Name: "foo",
				FactorioIcon: renderer.FactorioIcon{
					Icon:     "__foo__/thumbnail.png",
					IconSize: 144,
				},
			},
		},
		"without thumbnail": {
			thumbnail: "",
			expectedResult: icon.Icon{
				Type: "mod",
				Name: "foo",
				FactorioIcon: renderer.FactorioIcon{
					Icon:     "__foo__/thumbnail.png",
					IconSize: 144,
				},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instanceBuilder := instancetest.NewMockedInstanceBuilder()
			mod := instanceBuilder.WithMod("foo")
			if test.thumbnail != "" {
				mod.WithFile("thumbnail.png", test.thumbnail)
			}

			modInfo := instancetest.ModInfo{
				Name: "foo",
			}

			ctx := context.Background()
			ctx = instanceBuilder.BuildContext(ctx)

			instance := ModProcessor{}
			result := instance.createThumbnailIcon(ctx, modInfo)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestModProcessor_createExportMod(t *testing.T) {
	labels := exportdata.Translations{"en": "abc"}
	descriptions := exportdata.Translations{"en": "def"}

	tests := map[string]struct {
		bundled        bool
		modInfo        instancetest.ModInfo
		iconHash       string
		expectedResult exportdata.ModV1
	}{
		"bundled": {
			bundled: true,
			modInfo: instancetest.ModInfo{
				Name:        "foo",
				Title:       "Foo Title",
				Description: "Foo Description",
				Author:      "Foo Author",
				Version:     "1.2.3",
			},
			iconHash: "fancy-hash",
			expectedResult: exportdata.ModV1{
				Name:         "foo",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Author:       "Foo Author",
				Version:      "1.2.3",
				Bundled:      true,
			},
		},
		"not bundled": {
			bundled: false,
			modInfo: instancetest.ModInfo{
				Name:        "foo",
				Title:       "Foo Title",
				Description: "Foo Description",
				Author:      "Foo Author",
				Version:     "1.2.3",
			},
			iconHash: "fancy-hash",
			expectedResult: exportdata.ModV1{
				Name:         "foo",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Author:       "Foo Author",
				Version:      "1.2.3",
				Bundled:      false,
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instanceBuilder := instancetest.NewMockedInstanceBuilder()
			if test.bundled {
				instanceBuilder.WithBundledMod("foo", true)
			} else {
				instanceBuilder.WithMod("foo")
			}

			ctx := context.Background()
			ctx = instanceBuilder.BuildContext(ctx)

			iconManager := mocks.NewIconManager(t)
			iconManager.On("HashForEntity", ctx, "mod", "foo").Once().Return(test.iconHash)

			translator := mocks.NewFullTranslator(t)
			translator.On(
				"TranslateWithFallback",
				ctx,
				[]any{"mod-name.foo"},
				exportdata.Translations{"en": "Foo Title"},
			).Once().Return(labels)
			translator.On(
				"TranslateWithFallback",
				ctx,
				[]any{"mod-description.foo"},
				exportdata.Translations{"en": "Foo Description"},
			).Once().Return(descriptions)

			instance := ModProcessor{
				iconManager: iconManager,
				translator:  translator,
			}
			result := instance.createExportMod(ctx, test.modInfo)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestModProcessor_Validate(t *testing.T) {
	tests := map[string]struct {
		seenDumperMod          bool
		seenModsAfterDumperMod bool
		expectedError          error
	}{
		"valid": {
			seenDumperMod:          true,
			seenModsAfterDumperMod: false,
			expectedError:          nil,
		},
		"missing dumper mod": {
			seenDumperMod:          false,
			seenModsAfterDumperMod: false,
			expectedError:          errors.NewModsNotLoadedError([]string{"baz"}),
		},
		"early dumper mod": {
			seenDumperMod:          true,
			seenModsAfterDumperMod: true,
			expectedError:          errors.NewModsNotLoadedError([]string{"baz"}),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			modVersions := map[string]string{
				"foo": "1.2.3",
				"bar": "2.3.4",
			}

			instanceBuilder := instancetest.NewMockedInstanceBuilder()
			instanceBuilder.WithMod("foo")
			instanceBuilder.WithMod("bar")
			instanceBuilder.WithMod("baz")

			ctx := context.Background()
			ctx = instanceBuilder.BuildContext(ctx)

			instance := ModProcessor{
				seenDumperMod:          test.seenDumperMod,
				seenModsAfterDumperMod: test.seenModsAfterDumperMod,
				modVersions:            modVersions,
			}
			err := instance.Validate(ctx)

			assert.Equal(t, test.expectedError, err)
		})
	}
}
