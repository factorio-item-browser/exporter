package dump

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump/mocks"
)

func TestNewSurfaceProcessor(t *testing.T) {
	hashProvider := mocks.NewHashProvider(t)
	translator := mocks.NewTranslator(t)

	instance := NewSurfaceProcessor(hashProvider, translator)

	assert.NotNil(t, instance)
	assert.Same(t, hashProvider, instance.hashProvider)
	assert.Same(t, translator, instance.translator)
}

func TestSurfaceProcessor_Process(t *testing.T) {
	localisedLabel := []any{"label", float64(42)}
	localisedDescription := []any{"description", float64(1337)}
	labels := exportdata.Translations{"en": "abc"}
	descriptions := exportdata.Translations{"en": "def"}

	tests := map[string]struct {
		entity                   any
		expectTranslation        bool
		expectHash               bool
		expectedResult           any
		expectedNumberOfSurfaces uint64
	}{
		"full": {
			entity: Surface{
				Type:                 "super",
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				SurfaceProperties: map[string]float64{
					"foo": 13.37,
					"bar": 4.2,
				},
				Order:  "x[fancy]",
				Hidden: false,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.SurfaceV1{
				Type:         "super",
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Properties: map[string]float64{
					"foo": 13.37,
					"bar": 4.2,
				},
				Order:  "x[fancy]",
				Hidden: false,
			},
			expectedNumberOfSurfaces: 43,
		},
		"minimal": {
			entity: Surface{
				Type:                 "super",
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				SurfaceProperties:    nil,
				Order:                "x[fancy]",
				Hidden:               true,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.SurfaceV1{
				Type:         "super",
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Properties:   map[string]float64{},
				Order:        "x[fancy]",
				Hidden:       true,
			},
			expectedNumberOfSurfaces: 43,
		},
		"no entity": {
			entity:                   nil,
			expectTranslation:        false,
			expectHash:               false,
			expectedResult:           nil,
			expectedNumberOfSurfaces: 42,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = NewMetaDataContext(ctx)

			metaData := MetaDataFromContext(ctx)
			metaData.Numbers["surface"] = 42

			line := Line{
				Content: []byte("fancy content"),
				Entity:  test.entity,
			}

			translator := mocks.NewTranslator(t)
			if test.expectTranslation {
				translator.On("Translate", ctx, localisedLabel).Once().Return(labels)
				translator.On("Translate", ctx, localisedDescription).Once().Return(descriptions)
			}

			hashProvider := mocks.NewHashProvider(t)
			if test.expectHash {
				hashProvider.On("HashForEntity", ctx, "super", "fancy").Once().Return("fancy-hash")
			}

			instance := SurfaceProcessor{
				hashProvider: hashProvider,
				translator:   translator,
			}

			result := instance.Process(ctx, line)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedNumberOfSurfaces, metaData.Numbers["surface"])
		})
	}
}

func TestSurfaceProcessor_Validate(t *testing.T) {
	ctx := context.Background()

	instance := SurfaceProcessor{}
	err := instance.Validate(ctx)

	assert.Nil(t, err)
}
