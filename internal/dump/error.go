package dump

import (
	"bytes"
	"context"
	"fmt"
	"strings"

	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
)

var (
	lineErrorStart = []byte("------------- Error -------------")
	lineErrorEnd   = []byte("---------------------------------")
)

// ErrorProcessor is checking the lines for any errors thrown by the Factorio game. If an error is present, it will
// fail the validation. This processor does not collect any other data.
type ErrorProcessor struct {
	errorMessage strings.Builder
	isCatching   bool
}

// NewErrorProcessor creates a new instance of ErrorProcessor.
func NewErrorProcessor() *ErrorProcessor {
	return &ErrorProcessor{
		isCatching: false,
	}
}

func (p *ErrorProcessor) Process(_ context.Context, line Line) any {
	if bytes.Equal(line.Content, lineErrorStart) {
		p.isCatching = true
		return nil
	}

	if bytes.Equal(line.Content, lineErrorEnd) {
		p.isCatching = false
		return nil
	}

	if p.isCatching {
		p.errorMessage.Write(line.Content)
		p.errorMessage.WriteByte('\n')
		return nil
	}

	return nil
}

func (p *ErrorProcessor) Validate(ctx context.Context) error {
	// Check if we had an error from Factorio itself.
	if p.errorMessage.Len() > 0 {
		return errors.NewFactorioExecutionError(p.errorMessage.String())
	}

	// Check whether the exit code was non-zero.
	exitCode := MetaDataFromContext(ctx).ExitCode
	if exitCode != 0 {
		return errors.NewFactorioExecutionError(
			fmt.Sprintf("process exited with code %d, unknown error", exitCode),
		)
	}

	// All is fine.
	return nil
}
