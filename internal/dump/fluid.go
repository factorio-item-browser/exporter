package dump

import (
	"context"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
)

type Fluid struct {
	Name                 string  `json:"name"`
	LocalisedName        any     `json:"localised_name"`
	LocalisedDescription any     `json:"localised_description"`
	FuelValue            float64 `json:"fuel_value"`
	Order                string  `json:"order"`
	Hidden               bool    `json:"hidden"`
}

type FluidProcessor struct {
	hashProvider hashProvider
	translator   translator
}

func NewFluidProcessor(hashProvider hashProvider, translator translator) *FluidProcessor {
	return &FluidProcessor{
		hashProvider: hashProvider,
		translator:   translator,
	}
}

func (p *FluidProcessor) Process(ctx context.Context, line Line) any {
	fluid, ok := line.Entity.(Fluid)
	if !ok {
		return nil
	}

	exportFluid := exportdata.FluidV1{
		Name:         fluid.Name,
		Labels:       p.translator.Translate(ctx, fluid.LocalisedName),
		Descriptions: p.translator.Translate(ctx, fluid.LocalisedDescription),
		Icon:         p.hashProvider.HashForEntity(ctx, "fluid", fluid.Name),
		Order:        fluid.Order,
		Hidden:       fluid.Hidden,
	}

	if fluid.FuelValue > 0 {
		exportFluid.Burns = &exportdata.FluidV1Burn{
			Value: roundFloat(fluid.FuelValue),
		}
	}

	MetaDataFromContext(ctx).IncreaseNumber("fluid")
	return exportFluid
}

func (p *FluidProcessor) Validate(_ context.Context) error {
	return nil
}
