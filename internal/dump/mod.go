package dump

import (
	"context"
	"fmt"
	"image/png"
	"log/slog"
	"regexp"
	"slices"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/icon"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	renderer "gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
)

var (
	regexpLineChecksum = regexp.MustCompile(`^\s*[0-9.]+ Checksum of (.*): \d+$`)
)

type iconManager interface {
	hashProvider
	iconAdder
}

type fullTranslator interface {
	LoadMod(ctx context.Context, modName string) error
	TranslateWithFallback(ctx context.Context, localisedString any, fallback exportdata.Translations) exportdata.Translations
}

type ModProcessor struct {
	iconManager            iconManager
	translator             fullTranslator
	loadedCore             bool
	seenDumperMod          bool
	seenModsAfterDumperMod bool
	modVersions            map[string]string
}

func NewModProcessor(iconManager iconManager, translator fullTranslator) *ModProcessor {
	return &ModProcessor{
		iconManager: iconManager,
		translator:  translator,
		modVersions: make(map[string]string),
	}
}

func (p *ModProcessor) Process(ctx context.Context, line Line) any {
	if !p.loadedCore {
		p.loadTranslations(ctx, "core")
		p.loadedCore = true
	}

	modInfo := p.readModInfo(ctx, line)
	if modInfo == nil {
		return nil
	}

	p.loadTranslations(ctx, modInfo.Name)
	p.iconManager.Add(ctx, p.createThumbnailIcon(ctx, *modInfo))

	metaData := MetaDataFromContext(ctx)
	metaData.IncreaseNumber("mod")
	metaData.SetModVersion(modInfo.Name, modInfo.Version)

	return p.createExportMod(ctx, *modInfo)
}

// readModInfo will try to read the ModInfo from the provided line. If no mod is present in the line, nil is returned.
func (p *ModProcessor) readModInfo(ctx context.Context, line Line) *instance.ModInfo {
	matches := regexpLineChecksum.FindSubmatch(line.Content)
	if matches == nil {
		return nil
	}

	modName := string(matches[1])
	if modName == "dumper" {
		p.seenDumperMod = true
		return nil
	}

	if p.seenDumperMod {
		p.seenModsAfterDumperMod = true
		return nil
	}

	fi := instance.FromContext(ctx)
	modInfo, err := fi.ModInfo(modName)
	if err != nil {
		log.FromContext(ctx).Error(
			"mod info load error",
			slog.String("modName", modName),
			slog.String("error", err.Error()),
		)
		return nil
	}

	return &modInfo
}

// loadTranslations will load the translations of the specified mod into the translator.
func (p *ModProcessor) loadTranslations(ctx context.Context, modName string) {
	err := p.translator.LoadMod(ctx, modName)
	if err != nil {
		log.FromContext(ctx).Error(
			"mod translation load error",
			slog.String("modName", modName),
			slog.String("error", err.Error()),
		)
	}
}

// createThumbnailIcon creates the icon which is representing the thumbnail of the mod specified by its name.
func (p *ModProcessor) createThumbnailIcon(ctx context.Context, modInfo instance.ModInfo) icon.Icon {
	result := icon.Icon{
		Type: "mod",
		Name: modInfo.Name,
		FactorioIcon: renderer.FactorioIcon{
			Icon:     fmt.Sprintf("__%s__/thumbnail.png", modInfo.Name),
			IconSize: 144,
		},
	}

	fi := instance.FromContext(ctx)
	thumbnail, err := fi.ReadModFile(modInfo.Name, "thumbnail.png")
	if err != nil {
		return result
	}

	img, err := png.Decode(thumbnail)
	if err != nil {
		return result
	}

	thumbnailSize := img.Bounds().Dx()
	if thumbnailSize > 0 {
		result.FactorioIcon.IconSize = int16(thumbnailSize)
	}

	return result
}

// createExportMod creates the export mod structure from the provided mod info.
func (p *ModProcessor) createExportMod(ctx context.Context, modInfo instance.ModInfo) exportdata.ModV1 {
	fi := instance.FromContext(ctx)

	exportMod := exportdata.ModV1{
		Name: modInfo.Name,
		Labels: p.translator.TranslateWithFallback(
			ctx,
			[]any{"mod-name." + modInfo.Name},
			exportdata.Translations{"en": modInfo.Title},
		),
		Descriptions: p.translator.TranslateWithFallback(
			ctx,
			[]any{"mod-description." + modInfo.Name},
			exportdata.Translations{"en": modInfo.Description},
		),
		Icon:    p.iconManager.HashForEntity(ctx, "mod", modInfo.Name),
		Author:  modInfo.Author,
		Version: modInfo.Version,
		Bundled: slices.Contains(fi.BundledModNames(), modInfo.Name),
	}

	return exportMod
}

func (p *ModProcessor) Validate(ctx context.Context) error {
	if !p.seenDumperMod || p.seenModsAfterDumperMod {
		fi := instance.FromContext(ctx)

		missingModNames := make([]string, 0)
		for _, modName := range fi.ModNames() {
			if _, ok := p.modVersions[modName]; !ok {
				missingModNames = append(missingModNames, modName)
			}
		}

		return errors.NewModsNotLoadedError(missingModNames)
	}

	return nil
}
