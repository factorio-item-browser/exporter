package dump

import (
	"context"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
)

type Machine struct {
	Name                 string  `json:"name"`
	LocalisedName        any     `json:"localised_name"`
	LocalisedDescription any     `json:"localised_description"`
	ModuleInventorySize  int64   `json:"module_inventory_size"`
	EnergyUsage          float64 `json:"energy_usage"`
	Order                string  `json:"order"`
	Hidden               bool    `json:"hidden"`

	CraftingCategories map[string]bool   `json:"crafting_categories"`
	CraftingSpeed      float64           `json:"crafting_speed"`
	IngredientCount    int64             `json:"ingredient_count"`
	FluidBoxes         []MachineFluidBox `json:"fluid_boxes"`

	ResourceCategories map[string]bool `json:"resource_categories"`
	MiningSpeed        float64         `json:"mining_speed"`

	FuelCategories map[string]bool `json:"fuel_categories"`
}

type MachineFluidBox struct {
	ProductionType string `json:"production_type"`
}

type MachineProcessor struct {
	hashProvider hashProvider
	translator   translator
}

func NewMachineProcessor(hashProvider hashProvider, translator translator) *MachineProcessor {
	return &MachineProcessor{
		hashProvider: hashProvider,
		translator:   translator,
	}
}

func (p *MachineProcessor) Process(ctx context.Context, line Line) any {
	machine, ok := line.Entity.(Machine)
	if !ok {
		return nil
	}

	exportMachine := exportdata.MachineV1{
		Name:         machine.Name,
		Labels:       p.translator.Translate(ctx, machine.LocalisedName),
		Descriptions: p.translator.Translate(ctx, machine.LocalisedDescription),
		Icon:         p.hashProvider.HashForEntity(ctx, "machine", machine.Name),
		ModuleSlots:  machine.ModuleInventorySize,
		EnergyUsage:  roundFloat(machine.EnergyUsage * 60), // Energy usage is provided per tick.
		Order:        machine.Order,
		Hidden:       machine.Hidden,
	}

	if len(machine.CraftingCategories) > 0 {
		exportMachine.Crafting = &exportdata.MachineV1Crafting{
			Categories: extractCategories(machine.CraftingCategories),
			Speed:      roundFloat(machine.CraftingSpeed),
			ItemSlots:  machine.IngredientCount,
		}

		for _, box := range machine.FluidBoxes {
			switch box.ProductionType {
			case "input":
				exportMachine.Crafting.FluidInputSlots++
			case "output":
				exportMachine.Crafting.FluidOutputSlots++
			case "input-output":
				exportMachine.Crafting.FluidInputSlots++
				exportMachine.Crafting.FluidOutputSlots++
			}
		}
	}

	if len(machine.ResourceCategories) > 0 {
		exportMachine.Mining = &exportdata.MachineV1Mining{
			Categories: extractCategories(machine.ResourceCategories),
			Speed:      roundFloat(machine.MiningSpeed),
		}
	}

	if len(machine.FuelCategories) > 0 {
		exportMachine.Burning = &exportdata.MachineV1Burning{
			Categories: extractCategories(machine.FuelCategories),
		}
	}

	if machine.Name == "character" && exportMachine.Crafting != nil {
		exportMachine.Crafting.Speed = 1
		exportMachine.Crafting.ItemSlots = 255
	}

	MetaDataFromContext(ctx).IncreaseNumber("machine")
	return exportMachine
}

func extractCategories(categories map[string]bool) []string {
	result := make([]string, 0, len(categories))
	for category, flag := range categories {
		if flag {
			result = append(result, category)
		}
	}
	return result
}

func (p *MachineProcessor) Validate(_ context.Context) error {
	return nil
}
