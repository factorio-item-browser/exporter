package dump

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump/mocks"
)

func TestNewResourceProcessor(t *testing.T) {
	hashProvider := mocks.NewHashProvider(t)
	translator := mocks.NewTranslator(t)

	instance := NewResourceProcessor(hashProvider, translator)

	assert.NotNil(t, instance)
	assert.Same(t, hashProvider, instance.hashProvider)
	assert.Same(t, translator, instance.translator)
}

func TestResourceProcessor_Process(t *testing.T) {
	localisedLabel := []any{"label", float64(42)}
	localisedDescription := []any{"description", float64(1337)}
	labels := exportdata.Translations{"en": "abc"}
	descriptions := exportdata.Translations{"en": "def"}

	tests := map[string]struct {
		entity                    any
		expectTranslation         bool
		expectHash                bool
		expectedResult            any
		expectedNumberOfResources uint64
	}{
		"full resource": {
			entity: Resource{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				ResourceCategory:     "fancy-resource",
				MiningTime:           21,
				RequiredFluid:        "fancy-fluid",
				FluidAmount:          1337,
				Products: []ResourceProduct{
					{
						Name:        "with-amount",
						Amount:      10,
						Probability: 0.75,
					},
					{
						Name:        "with-min-max",
						AmountMin:   2,
						AmountMax:   5,
						Probability: 0.25,
					},
				},
				Order:  "x[fancy]",
				Hidden: false,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.ResourceV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Category:     "fancy-resource",
				Time:         21,
				Fluid: &exportdata.ResourceV1Fluid{
					Name:   "fancy-fluid",
					Amount: 1337,
				},
				Products: []exportdata.ResourceV1Product{
					{
						Name:        "with-amount",
						AmountMin:   10,
						AmountMax:   10,
						Probability: 0.75,
					},
					{
						Name:        "with-min-max",
						AmountMin:   2,
						AmountMax:   5,
						Probability: 0.25,
					},
				},
				Order:  "x[fancy]",
				Hidden: false,
			},
			expectedNumberOfResources: 43,
		},
		"minimal resource": {
			entity: Resource{
				Name:                 "fancy",
				LocalisedName:        localisedLabel,
				LocalisedDescription: localisedDescription,
				ResourceCategory:     "fancy-resource",
				MiningTime:           21,
				Order:                "x[fancy]",
				Hidden:               false,
			},
			expectTranslation: true,
			expectHash:        true,
			expectedResult: exportdata.ResourceV1{
				Name:         "fancy",
				Labels:       labels,
				Descriptions: descriptions,
				Icon:         "fancy-hash",
				Category:     "fancy-resource",
				Time:         21,
				Fluid:        nil,
				Products:     []exportdata.ResourceV1Product{},
				Order:        "x[fancy]",
				Hidden:       false,
			},
			expectedNumberOfResources: 43,
		},
		"no entity": {
			entity:                    nil,
			expectTranslation:         false,
			expectHash:                false,
			expectedResult:            nil,
			expectedNumberOfResources: 42,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = NewMetaDataContext(ctx)

			metaData := MetaDataFromContext(ctx)
			metaData.Numbers["resource"] = 42

			line := Line{
				Content: []byte("fancy content"),
				Entity:  test.entity,
			}

			translator := mocks.NewTranslator(t)
			if test.expectTranslation {
				translator.On("Translate", ctx, localisedLabel).Once().Return(labels)
				translator.On("Translate", ctx, localisedDescription).Once().Return(descriptions)
			}

			hashProvider := mocks.NewHashProvider(t)
			if test.expectHash {
				hashProvider.On("HashForEntity", ctx, "resource", "fancy").Once().Return("fancy-hash")
			}

			instance := ResourceProcessor{
				hashProvider: hashProvider,
				translator:   translator,
			}

			result := instance.Process(ctx, line)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedNumberOfResources, metaData.Numbers["resource"])
		})
	}
}

func TestResourceProcessor_Validate(t *testing.T) {
	ctx := context.Background()

	instance := ResourceProcessor{}
	err := instance.Validate(ctx)

	assert.Nil(t, err)
}
