package dump

import (
	"context"
	"sync"
)

type MetaData struct {
	lock sync.Mutex

	ExitCode    int
	ModVersions map[string]string
	Numbers     map[string]uint64
	Files       map[string]FileMetaData
}

type FileMetaData struct {
	Size           uint64
	CompressedSize uint64
}

func NewMetaData() *MetaData {
	return &MetaData{
		ModVersions: make(map[string]string),
		Numbers:     make(map[string]uint64),
		Files:       make(map[string]FileMetaData),
	}
}

func (d *MetaData) SetExitCode(exitCode int) {
	d.lock.Lock()
	defer d.lock.Unlock()

	d.ExitCode = exitCode
}

func (d *MetaData) SetModVersion(name string, version string) {
	d.lock.Lock()
	defer d.lock.Unlock()

	d.ModVersions[name] = version
}

func (d *MetaData) IncreaseNumber(name string) {
	d.lock.Lock()
	defer d.lock.Unlock()

	d.Numbers[name] = d.Numbers[name] + 1
}

func (d *MetaData) AddFile(name string, data FileMetaData) {
	d.lock.Lock()
	defer d.lock.Unlock()

	d.Files[name] = data
}

type contextKey string

const keyMetaData contextKey = "metaData"

// NewMetaDataContext creates a new context from the provided one, with empty metadata set to them.
func NewMetaDataContext(ctx context.Context) context.Context {
	return context.WithValue(ctx, keyMetaData, NewMetaData())
}

// MetaDataFromContext returns the metadata from the provided context.
func MetaDataFromContext(ctx context.Context) *MetaData {
	return ctx.Value(keyMetaData).(*MetaData)
}
