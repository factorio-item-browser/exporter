package dump

import (
	"context"
	"math"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
)

// Processor is the interface for all the export processors.
type Processor interface {
	Process(ctx context.Context, line Line) any
	Validate(ctx context.Context) error
}

type translator interface {
	Translate(ctx context.Context, localisedString any) exportdata.Translations
}

type hashProvider interface {
	HashForEntity(ctx context.Context, entityType string, entityName string) string
}

// roundFloat will round the provided float value to 3 digits after the decimal point.
func roundFloat(f float64) float64 {
	return math.Round(f*1000) / 1000
}
