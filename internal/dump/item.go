package dump

import (
	"context"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
)

type Item struct {
	Name                 string                `json:"name"`
	LocalisedName        any                   `json:"localised_name"`
	LocalisedDescription any                   `json:"localised_description"`
	StackSize            int64                 `json:"stack_size"`
	Weight               float64               `json:"weight"`
	FuelCategory         string                `json:"fuel_category"`
	FuelValue            float64               `json:"fuel_value"`
	BurntResultName      string                `json:"burnt_result_name"`
	SpoilResultName      string                `json:"spoil_result_name"`
	SpoilTicks           int64                 `json:"spoil_ticks"`
	RocketLaunchProducts []RocketLaunchProduct `json:"rocket_launch_products"`
	Order                string                `json:"order"`
	Hidden               bool                  `json:"hidden"`
}

type RocketLaunchProduct struct {
	Name        string  `json:"name"`
	Amount      float64 `json:"amount"`
	AmountMin   float64 `json:"amount_min"`
	AmountMax   float64 `json:"amount_max"`
	Probability float64 `json:"probability"`
}

type ItemProcessor struct {
	hashProvider hashProvider
	translator   translator
}

func NewItemProcessor(hashProvider hashProvider, translator translator) *ItemProcessor {
	return &ItemProcessor{
		hashProvider: hashProvider,
		translator:   translator,
	}
}

func (p *ItemProcessor) Process(ctx context.Context, line Line) any {
	item, ok := line.Entity.(Item)
	if !ok {
		return nil
	}

	exportItem := exportdata.ItemV1{
		Name:         item.Name,
		Labels:       p.translator.Translate(ctx, item.LocalisedName),
		Descriptions: p.translator.Translate(ctx, item.LocalisedDescription),
		Icon:         p.hashProvider.HashForEntity(ctx, "item", item.Name),
		StackSize:    item.StackSize,
		Weight:       roundFloat(item.Weight / 1000),
		Order:        item.Order,
		Hidden:       item.Hidden,
	}

	if item.FuelValue > 0 {
		exportItem.Burns = &exportdata.ItemV1Burn{
			Category:   item.FuelCategory,
			Value:      roundFloat(item.FuelValue),
			ResultName: item.BurntResultName,
		}
	}

	if item.SpoilResultName != "" {
		exportItem.Spoils = &exportdata.ItemV1Spoil{
			ResultName: item.SpoilResultName,
			Time:       roundFloat(float64(item.SpoilTicks) / 60),
		}
	}

	exportItem.RocketLaunchProducts = make([]exportdata.ItemV1RocketLaunchProduct, 0, len(item.RocketLaunchProducts))
	for _, product := range item.RocketLaunchProducts {
		mappedProduct := exportdata.ItemV1RocketLaunchProduct{
			Name:        product.Name,
			AmountMin:   roundFloat(product.AmountMin),
			AmountMax:   roundFloat(product.AmountMax),
			Probability: roundFloat(product.Probability),
		}
		if product.Amount > 0 {
			mappedProduct.AmountMin = roundFloat(product.Amount)
			mappedProduct.AmountMax = roundFloat(product.Amount)
		}
		exportItem.RocketLaunchProducts = append(exportItem.RocketLaunchProducts, mappedProduct)
	}

	MetaDataFromContext(ctx).IncreaseNumber("item")
	return exportItem
}

func (p *ItemProcessor) Validate(_ context.Context) error {
	return nil
}
