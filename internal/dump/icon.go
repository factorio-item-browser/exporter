package dump

import (
	"context"

	"gitlab.com/factorio-item-browser/exporter.git/internal/icon"
)

type iconAdder interface {
	Add(ctx context.Context, icon icon.Icon)
}

// IconProcessor is checking all lines for icons and adding them to the icon manager.
// It does not add any data to the export on its own.
type IconProcessor struct {
	iconAdder iconAdder
}

// NewIconProcessor creates a new instance of IconProcessor.
func NewIconProcessor(iconAdder iconAdder) *IconProcessor {
	return &IconProcessor{
		iconAdder: iconAdder,
	}
}

func (p *IconProcessor) Process(ctx context.Context, line Line) any {
	dumpIcon, ok := line.Entity.(icon.Icon)
	if !ok {
		return nil
	}

	p.iconAdder.Add(ctx, dumpIcon)
	return nil
}

func (*IconProcessor) Validate(_ context.Context) error {
	return nil
}
