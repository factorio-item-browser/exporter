package dump

import (
	"bufio"
	"bytes"
	"encoding/json"
	"io"
	"regexp"

	"gitlab.com/factorio-item-browser/exporter.git/internal/icon"
)

// Line represents a line from the Factorio output. It also contains the unmarshalled dump entity, if the line contains
// one.
type Line struct {
	Content []byte
	Entity  any
}

// LineReader is the reader for the Factorio output, which will provide each line for further processing. It checks
// for any dump data in the lines, and will already unmarshal it to the corresponding dump structure.
type LineReader struct {
	reader io.Reader
}

// NewLineReader creates a new instance of LineReader.
func NewLineReader(reader io.Reader) *LineReader {
	return &LineReader{
		reader: reader,
	}
}

var (
	prefixDumpLine = []byte(">DUMP>")
	regexpLineDump = regexp.MustCompile(`^>DUMP>(.*)>(.*)<$`)
	readLine       = (*LineReader).readLine
)

// Read will from the provided reader, and process each line checking for any dump data. The processed lines are
// returned as an iterator.
func (r *LineReader) Read() <-chan Line {
	result := make(chan Line)

	go func() {
		defer close(result)

		scanner := bufio.NewScanner(r.reader)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			result <- readLine(r, scanner.Bytes())
		}
	}()

	return result
}

// readLine will read the provided content and create a Line object representing it. If the content contains dump data,
// it will be unmarshalled into a corresponding entity.
func (r *LineReader) readLine(content []byte) Line {
	line := Line{
		Content: content,
	}

	if !bytes.HasPrefix(content, prefixDumpLine) {
		return line
	}

	matches := regexpLineDump.FindSubmatch(content)
	if matches == nil {
		// We are not interested in the current line.
		return line
	}

	switch string(matches[1]) {
	case "fluid":
		line.Entity = parseData[Fluid](matches[2])
	case "icon":
		line.Entity = parseData[icon.Icon](matches[2])
	case "item":
		line.Entity = parseData[Item](matches[2])
	case "machine":
		line.Entity = parseData[Machine](matches[2])
	case "recipe":
		line.Entity = parseData[Recipe](matches[2])
	case "resource":
		line.Entity = parseData[Resource](matches[2])
	case "surface":
		line.Entity = parseData[Surface](matches[2])
	case "technology":
		line.Entity = parseData[Technology](matches[2])
	}

	return line
}

// parseData will parse the data into an entity of the provided type T.
func parseData[T any](data []byte) T {
	var entity T
	_ = json.Unmarshal(data, &entity)
	return entity
}
