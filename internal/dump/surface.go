package dump

import (
	"context"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
)

type Surface struct {
	Type                 string             `json:"type"`
	Name                 string             `json:"name"`
	LocalisedName        any                `json:"localised_name"`
	LocalisedDescription any                `json:"localised_description"`
	SurfaceProperties    map[string]float64 `json:"surface_properties"`
	Order                string             `json:"order"`
	Hidden               bool               `json:"hidden"`
}

type SurfaceProcessor struct {
	hashProvider hashProvider
	translator   translator
}

func NewSurfaceProcessor(hashProvider hashProvider, translator translator) *SurfaceProcessor {
	return &SurfaceProcessor{
		hashProvider: hashProvider,
		translator:   translator,
	}
}

func (p *SurfaceProcessor) Process(ctx context.Context, line Line) any {
	surface, ok := line.Entity.(Surface)
	if !ok {
		return nil
	}

	exportSurface := exportdata.SurfaceV1{
		Type:         surface.Type,
		Name:         surface.Name,
		Labels:       p.translator.Translate(ctx, surface.LocalisedName),
		Descriptions: p.translator.Translate(ctx, surface.LocalisedDescription),
		Icon:         p.hashProvider.HashForEntity(ctx, surface.Type, surface.Name),
		Properties:   make(map[string]float64, len(surface.SurfaceProperties)),
		Order:        surface.Order,
		Hidden:       surface.Hidden,
	}

	for name, value := range surface.SurfaceProperties {
		exportSurface.Properties[name] = roundFloat(value)
	}

	MetaDataFromContext(ctx).IncreaseNumber("surface")
	return exportSurface
}

func (p *SurfaceProcessor) Validate(_ context.Context) error {
	return nil
}
