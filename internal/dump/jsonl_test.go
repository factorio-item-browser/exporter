package dump

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewJsonLinesWriter(t *testing.T) {
	writer := bytes.NewBuffer(nil)

	instance := NewJsonLinesWriter(writer)

	assert.NotNil(t, instance)
	assert.Same(t, writer, instance.writer)
}

func TestJsonLinesWriter_WriteData(t *testing.T) {
	type testData struct {
		Foo any `json:"foo"`
	}

	tests := map[string]struct {
		data              any
		expectedErrorType error
		expectedContent   string
	}{
		"happy path": {
			data: testData{
				Foo: "bar",
			},
			expectedErrorType: nil,
			expectedContent:   "{\"foo\":\"bar\"}\n",
		},
		"marshal error": {
			data: testData{
				Foo: make(chan string),
			},
			expectedErrorType: &json.UnsupportedTypeError{},
			expectedContent:   "",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			buffer := bytes.NewBuffer(nil)

			instance := JsonLinesWriter{
				writer: buffer,
			}
			err := instance.WriteData(test.data)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedContent, buffer.String())
		})
	}
}
