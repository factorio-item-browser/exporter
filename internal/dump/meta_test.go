package dump

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewMetaData(t *testing.T) {
	instance := NewMetaData()

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.ModVersions)
	assert.NotNil(t, instance.Numbers)
	assert.NotNil(t, instance.Files)
}

func TestMetaData_SetExitCode(t *testing.T) {
	exitCode := 42

	instance := MetaData{}
	instance.SetExitCode(exitCode)

	assert.Equal(t, exitCode, instance.ExitCode)
}

func TestMetaData_SetModVersion(t *testing.T) {
	modName := "foo"
	version := "1.2.3"
	expectedModVersions := map[string]string{
		"bar": "3.2.1",
		"foo": "1.2.3",
	}

	instance := MetaData{
		ModVersions: map[string]string{
			"bar": "3.2.1",
		},
	}
	instance.SetModVersion(modName, version)

	assert.Equal(t, expectedModVersions, instance.ModVersions)
}

func TestMetaData_IncreaseNumber(t *testing.T) {
	tests := map[string]struct {
		numbers         map[string]uint64
		name            string
		expectedNumbers map[string]uint64
	}{
		"existing": {
			numbers: map[string]uint64{
				"foo": 42,
				"bar": 21,
			},
			name: "foo",
			expectedNumbers: map[string]uint64{
				"foo": 43,
				"bar": 21,
			},
		},
		"new": {
			numbers: map[string]uint64{
				"bar": 21,
			},
			name: "foo",
			expectedNumbers: map[string]uint64{
				"foo": 1,
				"bar": 21,
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := MetaData{
				Numbers: test.numbers,
			}
			instance.IncreaseNumber(test.name)

			assert.Equal(t, test.expectedNumbers, instance.Numbers)
		})
	}
}

func TestMetaData_AddFile(t *testing.T) {
	name := "foo"
	data := FileMetaData{
		Size:           7331,
		CompressedSize: 1337,
	}
	expectedFiles := map[string]FileMetaData{
		"bar": {
			Size:           21,
			CompressedSize: 42,
		},
		"foo": data,
	}

	instance := MetaData{
		Files: map[string]FileMetaData{
			"bar": {
				Size:           21,
				CompressedSize: 42,
			},
		},
	}
	instance.AddFile(name, data)

	assert.Equal(t, expectedFiles, instance.Files)
}

func TestMetaData_Context(t *testing.T) {
	ctx := context.Background()
	ctx = NewMetaDataContext(ctx)

	result := MetaDataFromContext(ctx)
	assert.IsType(t, &MetaData{}, result)

	// Changes will be returned from future context calls
	result.ExitCode = 42
	result2 := MetaDataFromContext(ctx)
	assert.Equal(t, 42, result2.ExitCode)
}
