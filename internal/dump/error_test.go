package dump

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
)

func TestNewErrorProcessor(t *testing.T) {
	instance := NewErrorProcessor()

	assert.NotNil(t, instance)
	assert.False(t, instance.isCatching)
}

func TestErrorProcessor_Process(t *testing.T) {
	tests := map[string]struct {
		lines                []string
		expectedErrorMessage string
	}{
		"with error message": {
			lines: []string{
				"abc",
				"def",
				"------------- Error -------------",
				"ghi",
				"jkl",
				"---------------------------------",
				"mno",
				"pqr",
			},
			expectedErrorMessage: "ghi\njkl\n",
		},
		"without error message": {
			lines: []string{
				"abc",
				"def",
				"ghi",
				"jkl",
				"---------------------------------",
				"mno",
				"pqr",
			},
			expectedErrorMessage: "",
		},
		"missing end mark": {
			lines: []string{
				"abc",
				"def",
				"------------- Error -------------",
				"ghi",
				"jkl",
				"mno",
				"pqr",
			},
			expectedErrorMessage: "ghi\njkl\nmno\npqr\n",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			instance := ErrorProcessor{}

			for _, content := range test.lines {
				line := Line{
					Content: []byte(content),
				}
				result := instance.Process(ctx, line)
				assert.Nil(t, result)
			}

			assert.Equal(t, test.expectedErrorMessage, instance.errorMessage.String())
		})
	}
}

func TestErrorProcessor_Validate(t *testing.T) {
	tests := map[string]struct {
		errorMessage      string
		exitCode          int
		expectedErrorType error
	}{
		"with error": {
			errorMessage:      "abc\ndef\n",
			exitCode:          0,
			expectedErrorType: &errors.FactorioExecutionError{},
		},
		"with exit error": {
			errorMessage:      "",
			exitCode:          42,
			expectedErrorType: &errors.FactorioExecutionError{},
		},
		"without error": {
			errorMessage:      "",
			exitCode:          0,
			expectedErrorType: nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			ctx = NewMetaDataContext(ctx)
			metaData := MetaDataFromContext(ctx)
			metaData.ExitCode = test.exitCode

			var errorMessage strings.Builder
			errorMessage.WriteString(test.errorMessage)

			instance := ErrorProcessor{
				errorMessage: errorMessage,
			}
			err := instance.Validate(ctx)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}
