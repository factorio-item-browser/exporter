package dump

import (
	"encoding/json"
	"io"
)

// JsonLinesWriter is a writer which takes in any structured data, and writes it out in JSONL format to the provided
// writer.
type JsonLinesWriter struct {
	writer io.Writer
}

// NewJsonLinesWriter creates a new instance of JsonLinesWriter.
func NewJsonLinesWriter(writer io.Writer) *JsonLinesWriter {
	return &JsonLinesWriter{
		writer: writer,
	}
}

// WriteData will marshal the provided data as JSON, and if successful, write it out as a new line into the writer.
func (w *JsonLinesWriter) WriteData(data any) error {
	encodedData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, _ = w.writer.Write(encodedData)
	_, _ = w.writer.Write([]byte("\n"))

	return nil
}
