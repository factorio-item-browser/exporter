package errors

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFactorioExecutionError(t *testing.T) {
	message := "foo"

	instance := NewFactorioExecutionError(message)

	assert.Equal(t, TypeFactorioExecution, instance.Type())
	assert.Equal(t, "factorio execution error: foo", instance.Error())
}

func TestFactorioVersionError(t *testing.T) {
	originalError := fmt.Errorf("test error")

	instance := NewFactorioVersionError(originalError)

	assert.Equal(t, TypeFactorioVersion, instance.Type())
	assert.Equal(t, "detecting Factorio version error: test error", instance.Error())
	assert.Same(t, originalError, instance.Unwrap())
}

func TestModsNotLoadedError(t *testing.T) {
	missingModNames := []string{"foo", "bar"}

	instance := NewModsNotLoadedError(missingModNames)

	assert.Equal(t, TypeModsNotLoaded, instance.Type())
	assert.Equal(t, "mods not loaded: foo, bar", instance.Error())
}
