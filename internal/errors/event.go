package errors

import "fmt"

// PublishEventError is returned when publishing an event has failed.
type PublishEventError struct {
	originalError error
}

func NewPublishEventError(originalError error) *PublishEventError {
	return &PublishEventError{
		originalError: originalError,
	}
}

func (e *PublishEventError) Type() ErrorType {
	return TypePublishEvent
}

func (e *PublishEventError) Error() string {
	return fmt.Sprintf("publish event error: %s", e.originalError)
}

func (e *PublishEventError) Unwrap() error {
	return e.originalError
}
