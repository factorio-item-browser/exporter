package errors

import (
	"fmt"
)

// ModNotFoundError is returned when a requested mod was not found on the mod portal.
type ModNotFoundError struct {
	modName string
}

func NewModNotFoundError(modName string) *ModNotFoundError {
	return &ModNotFoundError{
		modName: modName,
	}
}

func (e *ModNotFoundError) Type() ErrorType {
	return TypeModNotFound
}

func (e *ModNotFoundError) Error() string {
	return fmt.Sprintf(`mod "%s" not found on mod portal`, e.modName)
}

// NoValidReleaseError is returned when a mod does not have a valid release on the Factorio mod portal to be used.
type NoValidReleaseError struct {
	modName string
}

func NewNoValidReleaseError(modName string) *NoValidReleaseError {
	return &NoValidReleaseError{
		modName: modName,
	}
}

func (e *NoValidReleaseError) Type() ErrorType {
	return TypeNoValidRelease
}

func (e *NoValidReleaseError) Error() string {
	return fmt.Sprintf(`no valid release found for mod "%s"`, e.modName)
}

// ModPortalRequestError is returned when a request to the Factorio Mod Portal has failed.
type ModPortalRequestError struct {
	originalError error
}

func NewModPortalRequestError(originalError error) *ModPortalRequestError {
	return &ModPortalRequestError{
		originalError: originalError,
	}
}

func (e *ModPortalRequestError) Type() ErrorType {
	return TypeModPortalRequest
}

func (e *ModPortalRequestError) Error() string {
	return fmt.Sprintf("mod portal request error: %s", e.originalError)
}

func (e *ModPortalRequestError) Unwrap() error {
	return e.originalError
}
