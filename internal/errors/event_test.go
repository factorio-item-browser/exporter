package errors

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPublishEventError(t *testing.T) {
	originalError := fmt.Errorf("test error")

	expectedType := TypePublishEvent
	expectedError := "publish event error: test error"

	instance := NewPublishEventError(originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedType, instance.Type())
	assert.Equal(t, expectedError, instance.Error())
	assert.Equal(t, originalError, instance.Unwrap())
}
