package errors

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnknownModError(t *testing.T) {
	modName := "foo"

	expectedType := TypeUnknownMod
	expectedError := `mod "foo" not known in the current instance`

	instance := NewUnknownModError(modName)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedType, instance.Type())
	assert.Equal(t, expectedError, instance.Error())
}

func TestReadModFileError(t *testing.T) {
	modName := "foo"
	fileName := "bar"
	originalError := fmt.Errorf("test error")

	expectedType := TypeReadModFile
	expectedError := `read file "bar" from mod "foo" error: test error`

	instance := NewReadModFileError(modName, fileName, originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedType, instance.Type())
	assert.Equal(t, expectedError, instance.Error())
	assert.Equal(t, originalError, instance.Unwrap())
}
