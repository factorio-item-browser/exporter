package errors

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestModNotFoundError(t *testing.T) {
	modName := "test"

	expectedType := TypeModNotFound
	expectedError := `mod "test" not found on mod portal`

	instance := NewModNotFoundError(modName)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedType, instance.Type())
	assert.Equal(t, expectedError, instance.Error())
}

func TestNoValidReleaseError(t *testing.T) {
	modName := "test"

	expectedType := TypeNoValidRelease
	expectedError := `no valid release found for mod "test"`

	instance := NewNoValidReleaseError(modName)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedType, instance.Type())
	assert.Equal(t, expectedError, instance.Error())
}

func TestModPortalRequestError(t *testing.T) {
	originalError := fmt.Errorf("test error")

	expectedType := TypeModPortalRequest
	expectedError := "mod portal request error: test error"

	instance := NewModPortalRequestError(originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedType, instance.Type())
	assert.Equal(t, expectedError, instance.Error())
	assert.Equal(t, originalError, instance.Unwrap())
}
