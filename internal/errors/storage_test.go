package errors

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStorageError(t *testing.T) {
	message := "abc"
	originalError := fmt.Errorf("test error")
	expectedType := TypeStorage
	expectedMessage := "abc: test error"

	instance := NewStorageError(message, originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedMessage, instance.Error())
	assert.Equal(t, expectedType, instance.Type())
	assert.Same(t, originalError, instance.Unwrap())
}
