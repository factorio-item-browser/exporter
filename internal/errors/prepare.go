package errors

import (
	"fmt"
)

// LocalFileSystemError is returned when something with the local filesystem went wrong and directories weren't possible
// to create or files to be copied.
type LocalFileSystemError struct {
	message       string
	originalError error
}

func NewLocalFileSystemError(message string, originalError error) *LocalFileSystemError {
	return &LocalFileSystemError{
		message:       message,
		originalError: originalError,
	}
}

func (e *LocalFileSystemError) Type() ErrorType {
	return TypeLocalFileSystem
}

func (e *LocalFileSystemError) Error() string {
	return fmt.Sprintf("%s: %s", e.message, e.originalError)
}

func (e *LocalFileSystemError) Unwrap() error {
	return e.originalError
}
