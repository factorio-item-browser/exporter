package errors

import (
	"fmt"
	"strings"
)

// FactorioExecutionError is returned when executing Factorio ended in an unexpected error from Factorio itself.
type FactorioExecutionError struct {
	message string
}

func NewFactorioExecutionError(message string) *FactorioExecutionError {
	return &FactorioExecutionError{
		message: message,
	}
}

func (e *FactorioExecutionError) Type() ErrorType {
	return TypeFactorioExecution
}

func (e *FactorioExecutionError) Error() string {
	return fmt.Sprintf("factorio execution error: %s", e.message)
}

type FactorioVersionError struct {
	originalError error
}

func NewFactorioVersionError(originalError error) *FactorioVersionError {
	return &FactorioVersionError{
		originalError: originalError,
	}
}

func (e *FactorioVersionError) Type() ErrorType {
	return TypeFactorioVersion
}

func (e *FactorioVersionError) Error() string {
	return fmt.Sprintf("detecting Factorio version error: %s", e.originalError)
}

func (e *FactorioVersionError) Unwrap() error {
	return e.originalError
}

type ModsNotLoadedError struct {
	missingModNames []string
}

func NewModsNotLoadedError(missingModNames []string) *ModsNotLoadedError {
	return &ModsNotLoadedError{
		missingModNames: missingModNames,
	}
}

func (e *ModsNotLoadedError) Type() ErrorType {
	return TypeModsNotLoaded
}

func (e *ModsNotLoadedError) Error() string {
	return fmt.Sprintf("mods not loaded: %s", strings.Join(e.missingModNames, ", "))
}
