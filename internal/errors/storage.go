package errors

import "fmt"

// StorageError is returned when the storage is encountering any problem while reading from or writing to it.
type StorageError struct {
	message       string
	originalError error
}

func NewStorageError(message string, originalError error) *StorageError {
	return &StorageError{
		message:       message,
		originalError: originalError,
	}
}

func (e *StorageError) Error() string {
	return fmt.Sprintf("%s: %s", e.message, e.originalError)
}

func (e *StorageError) Type() ErrorType {
	return TypeStorage
}

func (e *StorageError) Unwrap() error {
	return e.originalError
}
