package errors

import "fmt"

// UnknownModError is returned when a mod was attempted to being accessed, which was not included in the current
// instance of Factorio.
type UnknownModError struct {
	modName string
}

func NewUnknownModError(modName string) *UnknownModError {
	return &UnknownModError{
		modName: modName,
	}
}

func (e *UnknownModError) Type() ErrorType {
	return TypeUnknownMod
}

func (e *UnknownModError) Error() string {
	return fmt.Sprintf(`mod "%s" not known in the current instance`, e.modName)
}

// ReadModFileError is returned when a problem with reading a file from a mod has been encountered.
type ReadModFileError struct {
	modName       string
	fileName      string
	originalError error
}

func NewReadModFileError(modName string, fileName string, originalError error) *ReadModFileError {
	return &ReadModFileError{
		modName:       modName,
		fileName:      fileName,
		originalError: originalError,
	}
}

func (e *ReadModFileError) Type() ErrorType {
	return TypeReadModFile
}

func (e *ReadModFileError) Error() string {
	return fmt.Sprintf(`read file "%s" from mod "%s" error: %s`, e.fileName, e.modName, e.originalError)
}

func (e *ReadModFileError) Unwrap() error {
	return e.originalError
}
