package errors

import "errors"

type ErrorType string

const (
	TypeFactorioExecution ErrorType = "factorio-execution"
	TypeFactorioVersion   ErrorType = "factorio-version"
	TypeLocalFileSystem   ErrorType = "local-filesystem"
	TypeModNotFound       ErrorType = "mod-not-found"
	TypeModPortalRequest  ErrorType = "mod-portal-request"
	TypeModsNotLoaded     ErrorType = "mods-not-loaded"
	TypeNoValidRelease    ErrorType = "no-valid-release"
	TypePublishEvent      ErrorType = "publish-event"
	TypeReadModFile       ErrorType = "read-mod-file"
	TypeStorage           ErrorType = "storage"
	TypeUnknown           ErrorType = "unknown"
	TypeUnknownMod        ErrorType = "unknown-mod"
)

var As = errors.As
