package errors

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLocalFileSystemError(t *testing.T) {
	message := "foo"
	originalError := fmt.Errorf("test error")

	expectedType := TypeLocalFileSystem
	expectedError := "foo: test error"

	instance := NewLocalFileSystemError(message, originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedType, instance.Type())
	assert.Equal(t, expectedError, instance.Error())
	assert.Equal(t, originalError, instance.Unwrap())
}
