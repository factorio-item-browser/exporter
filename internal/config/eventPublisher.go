package config

import "os"

type EventPublisher struct {
	SourceFormat string
}

func ForEventPublisher() EventPublisher {
	return EventPublisher{
		SourceFormat: os.Getenv("EVENT_PUBLISHER_SOURCE_FORMAT"),
	}
}
