package config

import (
	"os"
	"strconv"
)

type IconRenderer struct {
	WorkerCount int
}

func ForIconRenderer() IconRenderer {
	workerCount, _ := strconv.ParseInt(os.Getenv("ICON_RENDERER_WORKER_COUNT"), 10, 64)

	return IconRenderer{
		WorkerCount: int(workerCount),
	}
}
