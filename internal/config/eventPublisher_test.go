package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForEventPublisher(t *testing.T) {
	env := map[string]string{
		"EVENT_PUBLISHER_SOURCE_FORMAT": "foo",
	}
	expectedResult := EventPublisher{
		SourceFormat: "foo",
	}

	currentEnv := make(map[string]string, len(env))
	for k := range env {
		currentEnv[k] = os.Getenv(k)
	}
	defer func(env map[string]string) {
		for k, v := range env {
			_ = os.Setenv(k, v)
		}
	}(currentEnv)
	for k, v := range env {
		_ = os.Setenv(k, v)
	}

	result := ForEventPublisher()
	assert.Equal(t, expectedResult, result)
}
