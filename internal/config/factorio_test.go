package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForFactorio(t *testing.T) {
	env := map[string]string{
		"FACTORIO_MOD_DOWNLOAD_WORKER_COUNT": "42",
		"FACTORIO_CREDENTIALS_USERNAME":      "secret-user",
		"FACTORIO_CREDENTIALS_TOKEN":         "secret-token",
	}
	expectedResult := Factorio{
		CredentialsUsername:    "secret-user",
		CredentialsToken:       "secret-token",
		ModDownloadWorkerCount: 42,
	}

	currentEnv := make(map[string]string, len(env))
	for k := range env {
		currentEnv[k] = os.Getenv(k)
	}
	defer func(env map[string]string) {
		for k, v := range env {
			_ = os.Setenv(k, v)
		}
	}(currentEnv)
	for k, v := range env {
		_ = os.Setenv(k, v)
	}

	result := ForFactorio()
	assert.Equal(t, expectedResult, result)
}
