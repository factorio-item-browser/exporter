package config

import (
	"os"
	"strconv"
)

type Mod struct {
	IncludeInModList     bool
	AutoloadTranslations bool
}

type Factorio struct {
	CredentialsUsername    string
	CredentialsToken       string
	ModDownloadWorkerCount int
}

func ForFactorio() Factorio {
	downloadWorkerCount, _ := strconv.ParseInt(os.Getenv("FACTORIO_MOD_DOWNLOAD_WORKER_COUNT"), 10, 64)

	return Factorio{
		CredentialsUsername:    os.Getenv("FACTORIO_CREDENTIALS_USERNAME"),
		CredentialsToken:       os.Getenv("FACTORIO_CREDENTIALS_TOKEN"),
		ModDownloadWorkerCount: int(downloadWorkerCount),
	}
}
