package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForStorage(t *testing.T) {
	env := map[string]string{
		"STORAGE_DATA_BUCKET_NAME": "foo",
		"STORAGE_MODS_BUCKET_NAME": "bar",
	}
	expectedResult := Storage{
		DataBucketName: "foo",
		ModsBucketName: "bar",
	}

	currentEnv := make(map[string]string, len(env))
	for k := range env {
		currentEnv[k] = os.Getenv(k)
	}
	defer func(env map[string]string) {
		for k, v := range env {
			_ = os.Setenv(k, v)
		}
	}(currentEnv)
	for k, v := range env {
		_ = os.Setenv(k, v)
	}

	result := ForStorage()
	assert.Equal(t, expectedResult, result)
}
