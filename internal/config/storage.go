package config

import "os"

type Storage struct {
	DataBucketName string
	ModsBucketName string
}

func ForStorage() Storage {
	return Storage{
		DataBucketName: os.Getenv("STORAGE_DATA_BUCKET_NAME"),
		ModsBucketName: os.Getenv("STORAGE_MODS_BUCKET_NAME"),
	}
}
