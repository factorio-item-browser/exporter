package config

import "os"

type Logger struct {
	Format string
	Level  string
}

func ForLogger() Logger {
	return Logger{
		Format: os.Getenv("LOGGER_FORMAT"),
		Level:  os.Getenv("LOGGER_LEVEL"),
	}
}
