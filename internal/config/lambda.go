package config

import "os"

type Lambda struct {
	IsLambda bool
}

func ForLambda() Lambda {
	return Lambda{
		IsLambda: os.Getenv("AWS_LAMBDA_FUNCTION_NAME") != "",
	}
}
