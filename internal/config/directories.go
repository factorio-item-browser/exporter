package config

import "os"

type Directories struct {
	Exporter  string
	Factorio  string
	Instances string
}

func ForDirectories() Directories {
	return Directories{
		Exporter:  os.Getenv("DIRECTORY_EXPORTER"),
		Factorio:  os.Getenv("DIRECTORY_FACTORIO"),
		Instances: os.Getenv("DIRECTORY_INSTANCES"),
	}
}
