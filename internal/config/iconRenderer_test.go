package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForIconRenderer(t *testing.T) {
	env := map[string]string{
		"ICON_RENDERER_WORKER_COUNT": "42",
	}
	expectedResult := IconRenderer{
		WorkerCount: 42,
	}

	currentEnv := make(map[string]string, len(env))
	for k := range env {
		currentEnv[k] = os.Getenv(k)
	}
	defer func(env map[string]string) {
		for k, v := range env {
			_ = os.Setenv(k, v)
		}
	}(currentEnv)
	for k, v := range env {
		_ = os.Setenv(k, v)
	}

	result := ForIconRenderer()
	assert.Equal(t, expectedResult, result)
}
