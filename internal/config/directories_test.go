package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForDirectories(t *testing.T) {
	env := map[string]string{
		"DIRECTORY_EXPORTER":  "foo",
		"DIRECTORY_FACTORIO":  "bar",
		"DIRECTORY_INSTANCES": "baz",
	}
	expectedResult := Directories{
		Exporter:  "foo",
		Factorio:  "bar",
		Instances: "baz",
	}

	currentEnv := make(map[string]string, len(env))
	for k := range env {
		currentEnv[k] = os.Getenv(k)
	}
	defer func(env map[string]string) {
		for k, v := range env {
			_ = os.Setenv(k, v)
		}
	}(currentEnv)
	for k, v := range env {
		_ = os.Setenv(k, v)
	}

	result := ForDirectories()
	assert.Equal(t, expectedResult, result)
}
