package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForLogger(t *testing.T) {
	env := map[string]string{
		"LOGGER_FORMAT": "foo",
		"LOGGER_LEVEL":  "bar",
	}
	expectedResult := Logger{
		Format: "foo",
		Level:  "bar",
	}

	currentEnv := make(map[string]string, len(env))
	for k := range env {
		currentEnv[k] = os.Getenv(k)
	}
	defer func(env map[string]string) {
		for k, v := range env {
			_ = os.Setenv(k, v)
		}
	}(currentEnv)
	for k, v := range env {
		_ = os.Setenv(k, v)
	}

	result := ForLogger()
	assert.Equal(t, expectedResult, result)
}
