package config

import "github.com/subosito/gotenv"

func init() {
	_ = gotenv.Load()
}
