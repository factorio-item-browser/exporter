package storage

import (
	"context"
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/storage/mocks"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func TestNewModStorage(t *testing.T) {
	bucketName := "fancy-bucket"
	cfg := config.Storage{
		ModsBucketName: bucketName,
	}

	instance := NewModStorage(cfg, aws.Config{})

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.bucket)
	assert.Equal(t, bucketName, instance.bucketName)
}

func TestModStorage_Version(t *testing.T) {
	tests := map[string]struct {
		headResult        *s3.HeadObjectOutput
		headError         error
		expectedErrorType error
		expectedResult    modportalclient.Version
	}{
		"happy path": {
			headResult: &s3.HeadObjectOutput{
				Metadata: map[string]string{
					metaDataVersion: "1.2.3",
				},
			},
			headError:         nil,
			expectedErrorType: nil,
			expectedResult:    modportalclient.NewVersion("1.2.3"),
		},
		"not found": {
			headResult:        nil,
			headError:         &types.NotFound{},
			expectedErrorType: nil,
			expectedResult:    modportalclient.Version{},
		},
		"api error": {
			headResult:        nil,
			headError:         fmt.Errorf("test error"),
			expectedErrorType: &errors.StorageError{},
			expectedResult:    modportalclient.Version{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			bucketName := "abc"
			modName := "foo"

			expectedInput := s3.HeadObjectInput{
				Bucket: aws.String("abc"),
				Key:    aws.String("foo.zip"),
			}

			bucket := mocks.NewBucket(t)
			bucket.On("HeadObject", ctx, &expectedInput).Once().Return(test.headResult, test.headError)

			instance := ModStorage{
				bucket:     bucket,
				bucketName: bucketName,
			}
			result, err := instance.Version(ctx, modName)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestModStorage_Persist(t *testing.T) {
	tests := map[string]struct {
		putError          error
		expectedErrorType error
	}{
		"happy path": {
			putError:          nil,
			expectedErrorType: nil,
		},
		"put error": {
			putError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.StorageError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			bucketName := "abc"

			modName := "foo"
			version := modportalclient.NewVersion("1.2.3")
			content := strings.NewReader("bar")

			expectedInput := s3.PutObjectInput{
				Bucket: aws.String("abc"),
				Key:    aws.String("foo.zip"),
				Body:   content,
				Metadata: map[string]string{
					metaDataVersion: "1.2.3",
				},
			}

			bucket := mocks.NewBucket(t)
			bucket.On("PutObject", ctx, &expectedInput).Once().Return(nil, test.putError)

			instance := ModStorage{
				bucket:     bucket,
				bucketName: bucketName,
			}
			err := instance.Persist(ctx, modName, version, content)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}

func TestModStorage_Fetch(t *testing.T) {
	tests := map[string]struct {
		getResult         *s3.GetObjectOutput
		getError          error
		expectedErrorType error
		expectedContent   string
		expectedVersion   modportalclient.Version
	}{
		"happy path": {
			getResult: &s3.GetObjectOutput{
				Body: io.NopCloser(strings.NewReader("abc")),
				Metadata: map[string]string{
					metaDataVersion: "1.2.3",
				},
			},
			getError:          nil,
			expectedErrorType: nil,
			expectedContent:   "abc",
			expectedVersion:   modportalclient.NewVersion("1.2.3"),
		},
		"get error": {
			getResult:         nil,
			getError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.StorageError{},
			expectedContent:   "",
			expectedVersion:   modportalclient.Version{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			bucketName := "abc"
			modName := "foo"

			expectedInput := s3.GetObjectInput{
				Bucket: aws.String(bucketName),
				Key:    aws.String("foo.zip"),
			}

			bucket := mocks.NewBucket(t)
			bucket.On("GetObject", ctx, &expectedInput).Once().Return(test.getResult, test.getError)

			instance := ModStorage{
				bucket:     bucket,
				bucketName: bucketName,
			}
			result, version, err := instance.Fetch(ctx, modName)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedVersion, version)
			if test.expectedContent != "" {
				content, _ := io.ReadAll(result)
				assert.Equal(t, test.expectedContent, string(content))
			}
		})
	}
}
