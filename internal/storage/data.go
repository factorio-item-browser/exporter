package storage

import (
	"bytes"
	"context"
	"fmt"
	"io"

	"github.com/andybalholm/brotli"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
)

const metaDataRawSize = "raw-size"

var (
	encodeContent = (*DataStorage).encodeContent
	uploadFile    = (*DataStorage).uploadFile
)

type DataStorage struct {
	bucket     bucket
	bucketName string
}

func NewDataStorage(cfg config.Storage, awsConfig aws.Config) *DataStorage {
	return &DataStorage{
		bucket:     s3.NewFromConfig(awsConfig),
		bucketName: cfg.DataBucketName,
	}
}

// UploadData will upload the main JSON data to the storage.
func (s *DataStorage) UploadData(
	ctx context.Context,
	combinationId string,
	sourceFileName string,
) (dump.FileMetaData, error) {
	return uploadFile(s, ctx, sourceFileName, fmt.Sprintf("data/%s.jsonl", combinationId), "application/jsonl")
}

// UploadStylesheet will upload the stylesheet to the storage.
func (s *DataStorage) UploadStylesheet(
	ctx context.Context,
	combinationId string,
	sourceFileName string,
) (dump.FileMetaData, error) {
	return uploadFile(s, ctx, sourceFileName, fmt.Sprintf("stylesheet/%s.css", combinationId), "text/css")
}

// UploadOutput will upload the Factorio output to the storage.
func (s *DataStorage) UploadOutput(
	ctx context.Context,
	combinationId string,
	sourceFileName string,
) (dump.FileMetaData, error) {
	return uploadFile(s, ctx, sourceFileName, fmt.Sprintf("output/%s.txt", combinationId), "text/plain")
}

// uploadFile will upload the provided file to the storage.
func (s *DataStorage) uploadFile(
	ctx context.Context,
	sourceFileName string,
	destinationKey string,
	contentType string,
) (dump.FileMetaData, error) {
	body, fileData, err := encodeContent(s, ctx, sourceFileName)
	if err != nil {
		return fileData, errors.NewStorageError("read file error: %s", err)
	}

	input := s3.PutObjectInput{
		Bucket:          aws.String(s.bucketName),
		Key:             aws.String(destinationKey),
		Body:            body,
		ContentEncoding: aws.String("br"),
		ContentType:     aws.String(contentType),
		Metadata: map[string]string{
			metaDataRawSize: fmt.Sprintf("%d", fileData.Size),
		},
	}

	_, err = s.bucket.PutObject(ctx, &input)
	if err != nil {
		return fileData, errors.NewStorageError("file put error", err)
	}
	return fileData, nil
}

// encodeContent will read the provided file and return its content already encoded for the storage.
func (s *DataStorage) encodeContent(_ context.Context, fileName string) (io.Reader, dump.FileMetaData, error) {
	fp, err := os.FileSystem.Open(fileName)
	if err != nil {
		return nil, dump.FileMetaData{}, err
	}
	defer func() {
		_ = fp.Close()
	}()

	stat, err := fp.Stat()
	if err != nil {
		return nil, dump.FileMetaData{}, err
	}

	buffer := new(bytes.Buffer)
	writer := brotli.NewWriterLevel(buffer, brotli.BestCompression)
	_, _ = io.Copy(writer, fp)
	_ = writer.Close()

	data := dump.FileMetaData{
		Size:           uint64(stat.Size()),
		CompressedSize: uint64(buffer.Len()),
	}

	return buffer, data, nil
}
