package storage

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/fs"
	"strings"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/dump"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/exporter.git/internal/storage/mocks"
)

func TestNewDataStorage(t *testing.T) {
	bucketName := "fancy-bucket"
	cfg := config.Storage{
		DataBucketName: bucketName,
	}

	instance := NewDataStorage(cfg, aws.Config{})

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.bucket)
	assert.Equal(t, bucketName, instance.bucketName)
}

func TestDataStorage_UploadData(t *testing.T) {
	defer func(ref1 func(*DataStorage, context.Context, string, string, string) (dump.FileMetaData, error)) {
		uploadFile = ref1
	}(uploadFile)

	ctx := context.Background()
	combinationId := "fancy-id"
	sourceFileName := "fancy-file"
	fileData := dump.FileMetaData{
		Size:           1337,
		CompressedSize: 42,
	}
	uploadError := fmt.Errorf("test error")
	expectedKey := "data/fancy-id.jsonl"
	expectedContentType := "application/jsonl"

	instance := DataStorage{}

	uploadFile = func(i *DataStorage, c context.Context, f string, k string, ct string) (dump.FileMetaData, error) {
		assert.Same(t, &instance, i)
		assert.Equal(t, ctx, c)
		assert.Equal(t, sourceFileName, f)
		assert.Equal(t, expectedKey, k)
		assert.Equal(t, expectedContentType, ct)

		return fileData, uploadError
	}

	result, err := instance.UploadData(ctx, combinationId, sourceFileName)

	assert.Equal(t, uploadError, err)
	assert.Equal(t, fileData, result)
}

func TestDataStorage_UploadStylesheet(t *testing.T) {
	defer func(ref1 func(*DataStorage, context.Context, string, string, string) (dump.FileMetaData, error)) {
		uploadFile = ref1
	}(uploadFile)

	ctx := context.Background()
	combinationId := "fancy-id"
	sourceFileName := "fancy-file"
	fileData := dump.FileMetaData{
		Size:           1337,
		CompressedSize: 42,
	}
	uploadError := fmt.Errorf("test error")
	expectedKey := "stylesheet/fancy-id.css"
	expectedContentType := "text/css"

	instance := DataStorage{}

	uploadFile = func(i *DataStorage, c context.Context, f string, k string, ct string) (dump.FileMetaData, error) {
		assert.Same(t, &instance, i)
		assert.Equal(t, ctx, c)
		assert.Equal(t, sourceFileName, f)
		assert.Equal(t, expectedKey, k)
		assert.Equal(t, expectedContentType, ct)

		return fileData, uploadError
	}

	result, err := instance.UploadStylesheet(ctx, combinationId, sourceFileName)

	assert.Equal(t, uploadError, err)
	assert.Equal(t, fileData, result)
}

func TestDataStorage_UploadOutput(t *testing.T) {
	defer func(ref1 func(*DataStorage, context.Context, string, string, string) (dump.FileMetaData, error)) {
		uploadFile = ref1
	}(uploadFile)

	ctx := context.Background()
	combinationId := "fancy-id"
	sourceFileName := "fancy-file"
	fileData := dump.FileMetaData{
		Size:           1337,
		CompressedSize: 42,
	}
	uploadError := fmt.Errorf("test error")
	expectedKey := "output/fancy-id.txt"
	expectedContentType := "text/plain"

	instance := DataStorage{}

	uploadFile = func(i *DataStorage, c context.Context, f string, k string, ct string) (dump.FileMetaData, error) {
		assert.Same(t, &instance, i)
		assert.Equal(t, ctx, c)
		assert.Equal(t, sourceFileName, f)
		assert.Equal(t, expectedKey, k)
		assert.Equal(t, expectedContentType, ct)

		return fileData, uploadError
	}

	result, err := instance.UploadOutput(ctx, combinationId, sourceFileName)

	assert.Equal(t, uploadError, err)
	assert.Equal(t, fileData, result)
}

func TestDataStorage_uploadFile(t *testing.T) {
	tests := map[string]struct {
		contentError      error
		expectPut         bool
		putError          error
		expectedErrorType error
	}{
		"happy path": {
			contentError:      nil,
			expectPut:         true,
			putError:          nil,
			expectedErrorType: nil,
		},
		"content error": {
			contentError:      fmt.Errorf("test error"),
			expectPut:         false,
			putError:          nil,
			expectedErrorType: &errors.StorageError{},
		},
		"put error": {
			contentError:      nil,
			expectPut:         true,
			putError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.StorageError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 func(*DataStorage, context.Context, string) (io.Reader, dump.FileMetaData, error)) {
				encodeContent = ref1
			}(encodeContent)

			ctx := context.Background()
			bucketName := "fancy-bucket"
			key := "fancy-key"
			contentType := "application/test"
			fileName := "fancy-file"
			body := strings.NewReader("foo")

			fileData := dump.FileMetaData{
				Size:           1337,
				CompressedSize: 42,
			}

			expectedInput := s3.PutObjectInput{
				Bucket:          aws.String(bucketName),
				Key:             aws.String(key),
				Body:            body,
				ContentEncoding: aws.String("br"),
				ContentType:     aws.String(contentType),
				Metadata: map[string]string{
					metaDataRawSize: "1337",
				},
			}

			bucket := mocks.NewBucket(t)
			if test.expectPut {
				bucket.On("PutObject", ctx, &expectedInput).Once().Return(nil, test.putError)
			}

			instance := DataStorage{
				bucket:     bucket,
				bucketName: bucketName,
			}

			encodeContent = func(i *DataStorage, c context.Context, f string) (io.Reader, dump.FileMetaData, error) {
				assert.Same(t, &instance, i)
				assert.Equal(t, ctx, c)
				assert.Equal(t, fileName, f)

				return body, fileData, test.contentError
			}

			result, err := instance.uploadFile(ctx, fileName, key, contentType)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, fileData, result)
		})
	}
}

func TestDataStorage_encodeContent(t *testing.T) {
	tests := map[string]struct {
		fileName          string
		expectedErrorType error
		expectedMeta      dump.FileMetaData
		expectedContent   []byte
	}{
		"happy path": {
			fileName:          "/root/test.txt",
			expectedErrorType: nil,
			expectedMeta: dump.FileMetaData{
				Size:           8,
				CompressedSize: 12,
			},
			expectedContent: []byte{0x8b, 0x3, 0x80, 0x66, 0x6f, 0x6f, 0xa, 0x62, 0x61, 0x72, 0xa, 0x3},
		},
		"missing file": {
			fileName:          "/root/missing.txt",
			expectedErrorType: &fs.PathError{},
			expectedMeta:      dump.FileMetaData{},
			expectedContent:   nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 afero.Fs) {
				os.FileSystem = ref1
			}(os.FileSystem)
			os.FileSystem = afero.NewMemMapFs()

			_ = afero.WriteFile(os.FileSystem, "/root/test.txt", []byte("foo\nbar\n"), 0644)

			ctx := context.Background()

			instance := DataStorage{}

			result, meta, err := instance.encodeContent(ctx, test.fileName)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedMeta, meta)
			if test.expectedContent != nil {
				content := bytes.NewBuffer(nil)
				_, _ = content.ReadFrom(result)
				assert.Equal(t, test.expectedContent, content.Bytes())
			}
		})
	}
}
