package storage

import (
	"context"
	"fmt"
	"io"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/smithy-go"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

const (
	metaDataVersion   = "version"
	keyMod            = "%s.zip"
	errorCodeNotFound = "NotFound"
)

type bucket interface {
	HeadObject(context.Context, *s3.HeadObjectInput, ...func(*s3.Options)) (*s3.HeadObjectOutput, error)
	GetObject(context.Context, *s3.GetObjectInput, ...func(*s3.Options)) (*s3.GetObjectOutput, error)
	PutObject(context.Context, *s3.PutObjectInput, ...func(*s3.Options)) (*s3.PutObjectOutput, error)
}

type ModStorage struct {
	bucket     bucket
	bucketName string
}

func NewModStorage(cfg config.Storage, awsConfig aws.Config) *ModStorage {
	return &ModStorage{
		bucket:     s3.NewFromConfig(awsConfig),
		bucketName: cfg.ModsBucketName,
	}
}

// Version retrieves the latest version of the provided mod, which is currently available in the mod storage.
func (s *ModStorage) Version(ctx context.Context, modName string) (modportalclient.Version, error) {
	input := s3.HeadObjectInput{
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(fmt.Sprintf(keyMod, modName)),
	}

	output, err := s.bucket.HeadObject(ctx, &input)
	if err != nil {
		var apiError smithy.APIError
		if errors.As(err, &apiError) && apiError.ErrorCode() == errorCodeNotFound {
			return modportalclient.Version{}, nil
		}
		return modportalclient.Version{}, errors.NewStorageError("mod head error", err)
	}

	return modportalclient.NewVersion(output.Metadata[metaDataVersion]), nil
}

// Persist writes the provided mod file to the mod storage.
func (s *ModStorage) Persist(ctx context.Context, modName string, version modportalclient.Version, content io.ReadSeeker) error {
	input := s3.PutObjectInput{
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(fmt.Sprintf(keyMod, modName)),
		Body:   content,
		Metadata: map[string]string{
			metaDataVersion: version.String(),
		},
	}

	_, err := s.bucket.PutObject(ctx, &input)
	if err != nil {
		return errors.NewStorageError("mod persist error", err)
	}
	return nil
}

// Fetch will fetch the provided mod from the storage, providing its contents and its version.
func (s *ModStorage) Fetch(ctx context.Context, modName string) (io.ReadCloser, modportalclient.Version, error) {
	input := s3.GetObjectInput{
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(fmt.Sprintf(keyMod, modName)),
	}

	output, err := s.bucket.GetObject(ctx, &input)
	if err != nil {
		return nil, modportalclient.Version{}, errors.NewStorageError("mod fetch error", err)
	}

	return output.Body, modportalclient.NewVersion(output.Metadata[metaDataVersion]), nil
}
