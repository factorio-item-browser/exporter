package factorio

import (
	"context"
	"net/http"

	"gitlab.com/blue-psyduck/soushinki.git/pkg/client"
	soushinki "gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

// ModPortal is the service helping with loading data from the Factorio Mod Portal.
type ModPortal struct {
	modPortalClient *client.Client
}

// NewModPortal creates a new instance of the ModPortal.
func NewModPortal(cfg config.Factorio) *ModPortal {
	modPortalClient := modportalclient.New(
		modportalclient.WithFactorioCredentials(cfg.CredentialsUsername, cfg.CredentialsToken),
	)

	return &ModPortal{
		modPortalClient: modPortalClient,
	}
}

// Mod fetches the mod with the provided name from the Factorio Mod Portal and returns its details.
func (p *ModPortal) Mod(ctx context.Context, modName string) (modportalclient.Mod, error) {
	request := modportalclient.ModRequest{
		ModName: modName,
	}
	response, err := request.CastResponse(p.modPortalClient.Send(ctx, &request))
	if err != nil {
		var re *soushinki.ResponseStatusError
		if errors.As(err, &re) && re.StatusCode() == http.StatusNotFound {
			return modportalclient.Mod{}, errors.NewModNotFoundError(modName)
		}

		return modportalclient.Mod{}, errors.NewModPortalRequestError(err)
	}

	return response.Mod, nil
}

// DownloadRelease downloads the provided release from the Factorio Mod Portal, and returns its contents as byte array.
func (p *ModPortal) DownloadRelease(
	ctx context.Context,
	release modportalclient.Release,
) ([]byte, error) {
	request := modportalclient.DownloadModRequest{
		DownloadUrl: release.DownloadUrl,
	}
	response, err := request.CastResponse(p.modPortalClient.Send(ctx, &request))
	if err != nil {
		return nil, errors.NewModPortalRequestError(err)
	}

	return response.Content, nil
}
