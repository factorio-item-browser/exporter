// Code generated by mockery. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	modportalclient "gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

// Versioner is an autogenerated mock type for the versioner type
type Versioner struct {
	mock.Mock
}

// Version provides a mock function with given fields: ctx
func (_m *Versioner) Version(ctx context.Context) (modportalclient.Version, error) {
	ret := _m.Called(ctx)

	if len(ret) == 0 {
		panic("no return value specified for Version")
	}

	var r0 modportalclient.Version
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context) (modportalclient.Version, error)); ok {
		return rf(ctx)
	}
	if rf, ok := ret.Get(0).(func(context.Context) modportalclient.Version); ok {
		r0 = rf(ctx)
	} else {
		r0 = ret.Get(0).(modportalclient.Version)
	}

	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// NewVersioner creates a new instance of Versioner. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewVersioner(t interface {
	mock.TestingT
	Cleanup(func())
}) *Versioner {
	mock := &Versioner{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
