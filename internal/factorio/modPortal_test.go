package factorio

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/clienttest"
	soushinki "gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/response"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func TestNewModPortal(t *testing.T) {
	cfg := config.Factorio{
		CredentialsUsername: "foo",
		CredentialsToken:    "bar",
	}

	instance := NewModPortal(cfg)

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.modPortalClient)
}

func TestModPortal_Mod(t *testing.T) {
	mod := modportalclient.Mod{
		Name:  "foo",
		Owner: "bar",
	}

	tests := map[string]struct {
		apiResponse       *modportalclient.ModResponse
		apiError          error
		expectedErrorType error
		expectedResult    modportalclient.Mod
	}{
		"happy path": {
			apiResponse: &modportalclient.ModResponse{
				Mod: mod,
			},
			apiError:          nil,
			expectedErrorType: nil,
			expectedResult:    mod,
		},
		"not found error": {
			apiResponse:       nil,
			apiError:          soushinki.NewResponseStatusError(http.StatusNotFound, nil, nil, nil),
			expectedErrorType: &errors.ModNotFoundError{},
			expectedResult:    modportalclient.Mod{},
		},
		"api error": {
			apiResponse:       nil,
			apiError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.ModPortalRequestError{},
			expectedResult:    modportalclient.Mod{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			modName := "foo"

			expectedRequest := modportalclient.ModRequest{
				ModName: modName,
			}

			builder := clienttest.NewMockedClientBuilder()
			builder.On(&expectedRequest).Once().Return(test.apiResponse, test.apiError)

			instance := ModPortal{
				modPortalClient: builder.Client(),
			}
			result, err := instance.Mod(ctx, modName)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestModPortal_DownloadRelease(t *testing.T) {
	tests := map[string]struct {
		apiResponse       *response.RawResponse
		apiError          error
		expectedErrorType error
		expectedResult    []byte
	}{
		"happy path": {
			apiResponse: &response.RawResponse{
				Content: []byte("fancy content"),
			},
			apiError:          nil,
			expectedErrorType: nil,
			expectedResult:    []byte("fancy content"),
		},
		"api error": {
			apiResponse:       nil,
			apiError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.ModPortalRequestError{},
			expectedResult:    nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			release := modportalclient.Release{
				DownloadUrl: "foo/bar",
			}

			expectedRequest := modportalclient.DownloadModRequest{
				DownloadUrl: "foo/bar",
			}

			builder := clienttest.NewMockedClientBuilder()
			builder.On(&expectedRequest).Once().Return(test.apiResponse, test.apiError)

			instance := ModPortal{
				modPortalClient: builder.Client(),
			}
			result, err := instance.DownloadRelease(ctx, release)

			assert.IsType(t, test.expectedErrorType, err)

			if test.expectedResult != nil {
				assert.Equal(t, test.expectedResult, result)
			}
		})
	}
}
