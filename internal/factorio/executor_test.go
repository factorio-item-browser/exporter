package factorio

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/fs"
	realos "os"
	"os/exec"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func TestNewExecutor(t *testing.T) {
	instance := NewExecutor()

	assert.NotNil(t, instance)
}

func TestExecutor_Version(t *testing.T) {
	tests := map[string]struct {
		output            string
		commandError      error
		expectedErrorType error
		expectedResult    modportalclient.Version
	}{
		"happy path": {
			output: `Version: 1.1.69 (build 12345, linux64, full)
Binary version: 64
Map input version: 0.18.0-0
Map output version: 1.1.69-0
`,
			commandError:      nil,
			expectedErrorType: nil,
			expectedResult:    modportalclient.NewVersion("1.1.69"),
		},
		"command error": {
			output:            "",
			commandError:      &exec.ExitError{},
			expectedErrorType: &errors.FactorioVersionError{},
			expectedResult:    modportalclient.Version{},
		},
		"regex mismatch": {
			output:            "unexpected output",
			commandError:      nil,
			expectedErrorType: &errors.FactorioVersionError{},
			expectedResult:    modportalclient.Version{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 func(*Executor, context.Context, string, []string, io.Writer) error) {
				execute = ref1
			}(execute)

			ctx := context.Background()
			ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

			expectedArgs := []string{"--version"}

			instance := Executor{}

			execute = func(i *Executor, c context.Context, d string, a []string, w io.Writer) error {
				assert.Same(t, &instance, i)
				assert.Equal(t, ctx, c)
				assert.Equal(t, "/root/instances/test/factorio", d)
				assert.Equal(t, expectedArgs, a)

				_, _ = w.Write([]byte(test.output))

				return test.commandError
			}

			result, err := instance.Version(ctx)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestExecutor_Run(t *testing.T) {
	tests := map[string]struct {
		commandError   error
		expectedResult int
	}{
		"happy path": {
			commandError:   nil,
			expectedResult: 0,
		},
		"exit code": {
			commandError:   &exec.ExitError{},
			expectedResult: -1,
		},
		"unknown error": {
			commandError:   fmt.Errorf("test error"),
			expectedResult: 1,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 func(*Executor, context.Context, string, []string, io.Writer) error) {
				execute = ref1
			}(execute)

			ctx := context.Background()
			ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

			buffer := bytes.NewBuffer(nil)
			output := "Lorem ipsum dolor sit amet"
			expectedArgs := []string{"--no-log-rotation", "--create=dump"}

			instance := Executor{}

			execute = func(i *Executor, c context.Context, d string, a []string, w io.Writer) error {
				assert.Same(t, &instance, i)
				assert.Equal(t, ctx, c)
				assert.Equal(t, "/root/instances/test/factorio", d)
				assert.Equal(t, expectedArgs, a)

				_, _ = w.Write([]byte(output))

				return test.commandError
			}

			result := instance.Run(ctx, buffer)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, output, buffer.String())
		})
	}
}

func TestExecutor_execute(t *testing.T) {
	tests := map[string]struct {
		workingDirectory  string
		exitCode          int
		expectedErrorType error
		expectOutput      bool
	}{
		"happy path": {
			workingDirectory:  "/",
			exitCode:          0,
			expectedErrorType: nil,
			expectOutput:      true,
		},
		"exit code": {
			workingDirectory:  "/",
			exitCode:          42,
			expectedErrorType: &exec.ExitError{},
			expectOutput:      true,
		},
		"start error": {
			workingDirectory:  "/not/existing",
			exitCode:          0,
			expectedErrorType: &fs.PathError{},
			expectOutput:      false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 func(string, ...string) *exec.Cmd) {
				execCommand = ref1
			}(execCommand)

			ctx := context.Background()
			ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

			args := []string{"--foo", "--bar"}
			buffer := bytes.NewBuffer(nil)
			expectedCommand := "bin/x64/factorio"

			execCommand = func(command string, a ...string) *exec.Cmd {
				fmt.Printf("Mocked the thing! %s | %+v\n", command, a)
				assert.Equal(t, expectedCommand, command)
				assert.Equal(t, args, a)

				fakeArgs := []string{"-test.run=TestExecutor_Command", "--", command}
				fakeArgs = append(fakeArgs, args...)

				cmd := exec.Command(realos.Args[0], fakeArgs...)
				cmd.Env = []string{fmt.Sprintf("GO_TEST_PROCESS=%d", test.exitCode)}
				return cmd
			}

			instance := Executor{}
			err := instance.execute(ctx, test.workingDirectory, args, buffer)

			assert.IsType(t, test.expectedErrorType, err)

			if test.expectOutput {
				assert.Equal(t, "Lorem ipsum dolor sit amet.\n", buffer.String())
			}
		})
	}
}
func TestExecutor_Command(t *testing.T) {
	if realos.Getenv("GO_TEST_PROCESS") == "" {
		return
	}

	fmt.Println("Lorem ipsum dolor sit amet.")
	exitCode, _ := strconv.ParseInt(realos.Getenv("GO_TEST_PROCESS"), 10, 64)
	realos.Exit(int(exitCode))
}
