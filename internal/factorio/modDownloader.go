package factorio

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log/slog"
	"slices"

	"github.com/spf13/afero"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

type versioner interface {
	Version(ctx context.Context) (modportalclient.Version, error)
}

type modPortalClient interface {
	Mod(ctx context.Context, modName string) (modportalclient.Mod, error)
	DownloadRelease(ctx context.Context, release modportalclient.Release) ([]byte, error)
}

type modStorage interface {
	Version(ctx context.Context, modName string) (modportalclient.Version, error)
	Persist(ctx context.Context, modName string, version modportalclient.Version, content io.ReadSeeker) error
	Fetch(ctx context.Context, modName string) (io.ReadCloser, modportalclient.Version, error)
}

// ModDownloader is able to download any mod from the Factorio Mod Portal, and store it locally in the Factorio
// instance of the provided context. The mod downloader also uses an S3 bucket as cache to make downloads of the same
// mod faster for future executions.
type ModDownloader struct {
	modPortalClient modPortalClient
	modStorage      modStorage
	versioner       versioner
	blacklistedMods []string
}

func NewModDownloader(modPortalClient modPortalClient, modStorage modStorage, versioner versioner) *ModDownloader {
	return &ModDownloader{
		modPortalClient: modPortalClient,
		modStorage:      modStorage,
		versioner:       versioner,
		blacklistedMods: []string{"core", "base"},
	}
}

func (d *ModDownloader) DownloadMod(ctx context.Context, modName string) error {
	fi := instance.FromContext(ctx)

	if slices.Contains(fi.BundledModNames(), modName) {
		return nil
	}

	factorioVersion, err := d.versioner.Version(ctx)
	if err != nil {
		return err
	}

	mod, err := d.modPortalClient.Mod(ctx, modName)
	if err != nil {
		return err
	}

	release := mod.SelectLatestRelease(&factorioVersion)
	if release == nil {
		return errors.NewNoValidReleaseError(modName)
	}

	currentVersion, err := d.modStorage.Version(ctx, modName)
	if err != nil {
		return err
	}

	modsDirectory := instance.FromContext(ctx).Path(instance.ModsDirectory)
	fileName := fmt.Sprintf("%s/%s_%s.zip", modsDirectory, modName, release.Version.String())

	ctx = log.NewContextWithValues(
		ctx,
		slog.String("modName", modName),
		slog.String("cachedVersion", currentVersion.String()),
		slog.String("latestVersion", release.Version.String()),
	)

	if release.Version.CompareTo(currentVersion) <= 0 {
		log.FromContext(ctx).Info("cached mod already up-to-date")
		content, _, err := d.modStorage.Fetch(ctx, modName)
		if err != nil {
			return err
		}

		return afero.WriteReader(os.FileSystem, fileName, content)
	}

	log.FromContext(ctx).Info("download new version from the mod portal")
	content, err := d.modPortalClient.DownloadRelease(ctx, *release)
	if err != nil {
		return err
	}

	err = afero.WriteReader(os.FileSystem, fileName, bytes.NewReader(content))
	if err != nil {
		return err
	}

	return d.modStorage.Persist(ctx, modName, release.Version, bytes.NewReader(content))
}
