package factorio

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os/exec"
	"regexp"

	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

var (
	execCommand = exec.Command
	execute     = (*Executor).execute

	regexpVersion = regexp.MustCompile(`Version: (\d+\.\d+\.\d+)`)
)

type Executor struct {
}

func NewExecutor() *Executor {
	return &Executor{}
}

// Version executes Factorio to retrieve its current version. If an error occurs during execution, or the version
// cannot be found in the output, an error will be returned.
func (e *Executor) Version(ctx context.Context) (modportalclient.Version, error) {
	fi := instance.FromContext(ctx)

	output := bytes.NewBuffer(nil)
	err := execute(e, ctx, fi.Path(instance.FactorioDirectory), []string{"--version"}, output)
	if err != nil {
		return modportalclient.Version{}, errors.NewFactorioVersionError(err)
	}

	match := regexpVersion.FindStringSubmatch(output.String())
	if match == nil {
		return modportalclient.Version{}, errors.NewFactorioVersionError(fmt.Errorf("version regexp mismatch"))
	}
	return modportalclient.NewVersion(match[1]), nil
}

// Run will actually run the Factorio instance, creating a save game file, thus loading all the mods.
// The exit code of the process is returned. If an error occurs during execution, this exit code will be non-zero.
// The error itself is not returned. It is expected that the output is analyzed to get the actual error.
func (e *Executor) Run(ctx context.Context, writer io.Writer) int {
	fi := instance.FromContext(ctx)
	args := []string{"--no-log-rotation", "--create=dump"}

	err := execute(e, ctx, fi.Path(instance.FactorioDirectory), args, writer)
	if err != nil {
		var exitErr *exec.ExitError
		if errors.As(err, &exitErr) {
			return exitErr.ExitCode()
		}

		return 1 // We had an error but no exit error, so assume 1 as exit code.
	}

	return 0
}

// execute will execute the Factorio binary passing the provided params. The output will be written to the provided
// writer. If Factorio exits with an error code, an error is returned.
func (e *Executor) execute(_ context.Context, workingDirectory string, args []string, writer io.Writer) error {
	cmd := execCommand("bin/x64/factorio", args...)
	cmd.Dir = workingDirectory
	cmd.Stdout = writer
	return cmd.Run()
}
