package factorio

import (
	"bytes"
	"context"
	"io"
	"strings"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/exporter.git/internal/errors"
	"gitlab.com/factorio-item-browser/exporter.git/internal/factorio/mocks"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/os"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func TestNewModDownloader(t *testing.T) {
	modPortalClient := mocks.NewModPortalClient(t)
	modStorage := mocks.NewModStorage(t)
	versioner := mocks.NewVersioner(t)

	instance := NewModDownloader(modPortalClient, modStorage, versioner)

	assert.NotNil(t, instance)
	assert.Same(t, modPortalClient, instance.modPortalClient)
	assert.Same(t, modStorage, instance.modStorage)
	assert.Same(t, versioner, instance.versioner)
}

func TestModDownloader_DownloadMod(t *testing.T) {
	factorioVersion := modportalclient.NewVersion("1.0.0")
	release := modportalclient.Release{
		Version: modportalclient.NewVersion("1.2.3"),
		InfoJson: modportalclient.InfoJson{
			FactorioVersion: &factorioVersion,
		},
	}
	mod := modportalclient.Mod{
		Releases: []modportalclient.Release{
			release,
		},
	}

	tests := map[string]struct {
		modName               string
		expectFactorioVersion bool
		factorioVersion       modportalclient.Version
		factorioVersionError  error
		expectMod             bool
		mod                   modportalclient.Mod
		modError              error
		expectModVersion      bool
		currentVersion        modportalclient.Version
		modVersionError       error
		expectFetch           bool
		fetchResult           io.ReadCloser
		fetchError            error
		expectDownload        bool
		expectedRelease       modportalclient.Release
		downloadContent       []byte
		downloadError         error
		expectPersist         bool
		persistError          error
		expectedErrorType     error
		expectedFileContent   string
	}{
		"already current version": {
			modName:               "foo",
			expectFactorioVersion: true,
			factorioVersion:       factorioVersion,
			factorioVersionError:  nil,
			expectMod:             true,
			mod:                   mod,
			modError:              nil,
			expectModVersion:      true,
			currentVersion:        modportalclient.NewVersion("1.2.3"),
			modVersionError:       nil,
			expectFetch:           true,
			fetchResult:           io.NopCloser(strings.NewReader("fancy-content")),
			fetchError:            nil,
			expectDownload:        false,
			expectedRelease:       modportalclient.Release{},
			downloadContent:       nil,
			downloadError:         nil,
			expectPersist:         false,
			persistError:          nil,
			expectedErrorType:     nil,
			expectedFileContent:   "fancy-content",
		},
		"download mod": {
			modName:               "foo",
			expectFactorioVersion: true,
			factorioVersion:       factorioVersion,
			factorioVersionError:  nil,
			expectMod:             true,
			mod:                   mod,
			modError:              nil,
			expectModVersion:      true,
			currentVersion:        modportalclient.NewVersion("1.2.0"),
			modVersionError:       nil,
			expectFetch:           false,
			fetchResult:           nil,
			fetchError:            nil,
			expectDownload:        true,
			expectedRelease:       release,
			downloadContent:       []byte("fancy-content"),
			downloadError:         nil,
			expectPersist:         true,
			persistError:          nil,
			expectedErrorType:     nil,
			expectedFileContent:   "fancy-content",
		},
		"blacklisted mod": {
			modName:               "base",
			expectFactorioVersion: false,
			expectMod:             false,
			expectModVersion:      false,
			expectFetch:           false,
			expectDownload:        false,
			expectPersist:         false,
			expectedErrorType:     nil,
			expectedFileContent:   "",
		},
		"factorio version error": {
			modName:               "foo",
			expectFactorioVersion: true,
			factorioVersion:       modportalclient.Version{},
			factorioVersionError:  &errors.FactorioVersionError{},
			expectMod:             false,
			expectModVersion:      false,
			expectFetch:           false,
			expectDownload:        false,
			expectPersist:         false,
			expectedErrorType:     &errors.FactorioVersionError{},
			expectedFileContent:   "",
		},
		"mod error": {
			modName:               "foo",
			expectFactorioVersion: true,
			factorioVersion:       factorioVersion,
			factorioVersionError:  nil,
			expectMod:             true,
			mod:                   modportalclient.Mod{},
			modError:              &errors.ModNotFoundError{},
			expectModVersion:      false,
			expectFetch:           false,
			expectDownload:        false,
			expectPersist:         false,
			expectedErrorType:     &errors.ModNotFoundError{},
			expectedFileContent:   "",
		},
		"no valid release error": {
			modName:               "foo",
			expectFactorioVersion: true,
			factorioVersion:       modportalclient.NewVersion("2.0.0"),
			factorioVersionError:  nil,
			expectMod:             true,
			mod:                   mod,
			modError:              nil,
			expectModVersion:      false,
			expectFetch:           false,
			expectDownload:        false,
			expectPersist:         false,
			expectedErrorType:     &errors.NoValidReleaseError{},
			expectedFileContent:   "",
		},
		"mod version error": {
			modName:               "foo",
			expectFactorioVersion: true,
			factorioVersion:       factorioVersion,
			factorioVersionError:  nil,
			expectMod:             true,
			mod:                   mod,
			modError:              nil,
			expectModVersion:      true,
			currentVersion:        modportalclient.Version{},
			modVersionError:       &errors.StorageError{},
			expectFetch:           false,
			expectDownload:        false,
			expectPersist:         false,
			expectedErrorType:     &errors.StorageError{},
			expectedFileContent:   "",
		},
		"fetch error": {
			modName:               "foo",
			expectFactorioVersion: true,
			factorioVersion:       factorioVersion,
			factorioVersionError:  nil,
			expectMod:             true,
			mod:                   mod,
			modError:              nil,
			expectModVersion:      true,
			currentVersion:        modportalclient.NewVersion("1.2.3"),
			modVersionError:       nil,
			expectFetch:           true,
			fetchResult:           nil,
			fetchError:            &errors.StorageError{},
			expectDownload:        false,
			expectPersist:         false,
			expectedErrorType:     &errors.StorageError{},
			expectedFileContent:   "",
		},
		"download error": {
			modName:               "foo",
			expectFactorioVersion: true,
			factorioVersion:       factorioVersion,
			factorioVersionError:  nil,
			expectMod:             true,
			mod:                   mod,
			modError:              nil,
			expectModVersion:      true,
			currentVersion:        modportalclient.NewVersion("1.2.0"),
			modVersionError:       nil,
			expectFetch:           false,
			expectDownload:        true,
			expectedRelease:       release,
			downloadContent:       nil,
			downloadError:         &errors.ModPortalRequestError{},
			expectPersist:         false,
			expectedErrorType:     &errors.ModPortalRequestError{},
			expectedFileContent:   "",
		},
		"persist error": {
			modName:               "foo",
			expectFactorioVersion: true,
			factorioVersion:       factorioVersion,
			factorioVersionError:  nil,
			expectMod:             true,
			mod:                   mod,
			modError:              nil,
			expectModVersion:      true,
			currentVersion:        modportalclient.NewVersion("1.2.0"),
			modVersionError:       nil,
			expectFetch:           false,
			expectDownload:        true,
			expectedRelease:       release,
			downloadContent:       nil,
			downloadError:         nil,
			expectPersist:         true,
			persistError:          &errors.StorageError{},
			expectedErrorType:     &errors.StorageError{},
			expectedFileContent:   "",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			defer func(ref1 afero.Fs) {
				os.FileSystem = ref1
			}(os.FileSystem)
			os.FileSystem = afero.NewMemMapFs()

			instanceBuilder := instance2.NewMockedInstanceBuilder()
			instanceBuilder.WithBundledMod("base", true)
			instanceBuilder.WithMod("foo")
			instanceBuilder.WithMod("bar")

			ctx := context.Background()
			ctx = instanceBuilder.BuildContext(ctx)

			versioner := mocks.NewVersioner(t)
			if test.expectFactorioVersion {
				versioner.On("Version", ctx).Once().Return(test.factorioVersion, test.factorioVersionError)
			}

			modPortalClient := mocks.NewModPortalClient(t)
			if test.expectMod {
				modPortalClient.On("Mod", ctx, test.modName).Once().Return(test.mod, test.modError)
			}
			if test.expectDownload {
				modPortalClient.
					On("DownloadRelease", mock.Anything, test.expectedRelease).
					Once().
					Return(test.downloadContent, test.downloadError)
			}

			modStorage := mocks.NewModStorage(t)
			if test.expectModVersion {
				modStorage.On("Version", ctx, test.modName).Once().Return(test.currentVersion, test.modVersionError)
			}
			if test.expectFetch {
				modStorage.
					On("Fetch", mock.Anything, test.modName).
					Once().
					Return(test.fetchResult, test.currentVersion, test.fetchError)
			}
			if test.expectPersist {
				modStorage.
					On("Persist", mock.Anything, test.modName, test.expectedRelease.Version, mock.IsType(&bytes.Reader{})).
					Once().
					Return(test.persistError)
			}

			instance := ModDownloader{
				modPortalClient: modPortalClient,
				modStorage:      modStorage,
				versioner:       versioner,
				blacklistedMods: []string{"core", "base"},
			}
			err := instance.DownloadMod(ctx, test.modName)

			assert.IsType(t, test.expectedErrorType, err)

			if test.expectedFileContent != "" {
				content, _ := afero.ReadFile(os.FileSystem, "/root/instances/test/factorio/mods/foo_1.2.3.zip")
				assert.Equal(t, test.expectedFileContent, string(content))
			}
		})
	}
}
