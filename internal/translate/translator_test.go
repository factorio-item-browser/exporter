package translate

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/translate/mocks"
)

func TestNew(t *testing.T) {
	instance := NewTranslator()

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.factorioTranslator)
}

func TestProcessControlPlaceholder(t *testing.T) {
	locale := "de"
	controlName := "build"
	version := 42
	translatedControl := "Bauen"
	expectedLocalisedString := []any{"controls.build"}
	expectedResult := "[Bauen]"

	factorioTranslator := mocks.NewProcessorTranslator(t)
	factorioTranslator.On("TranslateWithFallback", locale, expectedLocalisedString).Once().Return(translatedControl)

	result, ok := processControlPlaceholder(factorioTranslator, locale, controlName, version)

	assert.True(t, ok)
	assert.Equal(t, expectedResult, result)
}

func TestEraseRichText(t *testing.T) {
	tests := map[string]string{
		"abc def":             "abc def",
		"abc[item=fail]def":   "abcdef",
		"abc [item=fail] def": "abc def",
		"abc [color=red] def [font=bold]ghi[/font] jkl [/color] mno": "abc def ghi jkl mno",
		"abc [item=fail], def":  "abc, def",
		"abc [item=fail] (def)": "abc (def)",
	}

	for value, expectedResult := range tests {
		t.Run(value, func(t *testing.T) {
			result := eraseRichText(nil, "test", value, nil)

			assert.Equal(t, expectedResult, result)
		})
	}
}

func TestTranslator_LoadMod(t *testing.T) {
	modName := "foo"

	instanceBuilder := instance2.NewMockedInstanceBuilder()
	instanceBuilder.WithMod(modName)

	ctx := context.Background()
	ctx = instanceBuilder.BuildContext(ctx)

	expectedModPath := "/root/instances/test/factorio/mods/foo"

	factorioTranslator := mocks.NewFactorioTranslator(t)
	factorioTranslator.On("LoadMod", expectedModPath).Once().Return(nil)

	instance := Translator{
		factorioTranslator: factorioTranslator,
	}
	err := instance.LoadMod(ctx, modName)

	assert.Nil(t, err)
}

func TestTranslator_LoadMod_WithUnknownMod(t *testing.T) {
	modName := "foo"

	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

	factorioTranslator := mocks.NewFactorioTranslator(t)

	instance := Translator{
		factorioTranslator: factorioTranslator,
	}
	err := instance.LoadMod(ctx, modName)

	assert.NotNil(t, err)
}

func TestTranslator_Translate(t *testing.T) {
	localisedString := []any{"foo", 42}
	locales := []string{"de", "en", "fr"}
	expectedResult := exportdata.Translations{
		"de": "abc",
		"en": "def",
	}

	factorioTranslator := mocks.NewFactorioTranslator(t)
	factorioTranslator.On("Locales").Maybe().Return(locales)
	factorioTranslator.On("Translate", "de", localisedString).Once().Return("abc")
	factorioTranslator.On("Translate", "en", localisedString).Once().Return("def")
	factorioTranslator.On("Translate", "fr", localisedString).Once().Return("")

	instance := Translator{
		factorioTranslator: factorioTranslator,
	}
	result := instance.Translate(context.Background(), localisedString)

	assert.Equal(t, expectedResult, result)
}

func TestTranslator_TranslateWithFallback(t *testing.T) {
	type call struct {
		locale string
		result string
	}

	tests := map[string]struct {
		localisedString any
		locales         []string
		fallback        exportdata.Translations
		translatorCalls []call
		expectedResult  exportdata.Translations
	}{
		"happy path": {
			localisedString: []any{"foo", 42},
			locales:         []string{"de", "en", "fr"},
			fallback:        exportdata.Translations{},
			translatorCalls: []call{
				{locale: "de", result: "abc"},
				{locale: "en", result: "def"},
				{locale: "fr", result: "ghi"},
			},
			expectedResult: exportdata.Translations{
				"de": "abc",
				"en": "def",
				"fr": "ghi",
			},
		},
		"some empty": {
			localisedString: []any{"foo", 42},
			locales:         []string{"de", "en", "fr"},
			fallback: exportdata.Translations{
				"en": "fed",
				"fr": "ihg",
			},
			translatorCalls: []call{
				{locale: "de", result: ""},
				{locale: "en", result: "def"},
				{locale: "fr", result: ""},
			},
			expectedResult: exportdata.Translations{
				"en": "def",
				"fr": "ihg",
			},
		},
		"all empty": {
			localisedString: []any{"foo", 42},
			locales:         []string{"de", "en", "fr"},
			fallback: exportdata.Translations{
				"en": "",
				"de": "",
			},
			translatorCalls: []call{
				{locale: "de", result: ""},
				{locale: "en", result: ""},
				{locale: "fr", result: ""},
			},
			expectedResult: nil,
		},
		"nil": {
			localisedString: nil,
			locales:         []string{"de", "en", "fr"},
			fallback:        exportdata.Translations{},
			translatorCalls: nil,
			expectedResult:  nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			factorioTranslator := mocks.NewFactorioTranslator(t)
			factorioTranslator.On("Locales").Maybe().Return(test.locales)
			for _, call := range test.translatorCalls {
				factorioTranslator.On("Translate", call.locale, test.localisedString).Once().Return(call.result)
			}

			instance := Translator{
				factorioTranslator: factorioTranslator,
			}
			result := instance.TranslateWithFallback(context.Background(), test.localisedString, test.fallback)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
