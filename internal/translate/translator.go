package translate

import (
	"context"
	"fmt"
	"regexp"

	"gitlab.com/factorio-item-browser/export-data.git/go/exportdata"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/factorio-translator.git/pkg/processor"
	"gitlab.com/factorio-item-browser/factorio-translator.git/pkg/translator"
)

type factorioTranslator interface {
	LoadMod(source string) error
	Locales() []string
	Translate(locale string, localisedString any) string
}

type Translator struct {
	factorioTranslator factorioTranslator
}

func NewTranslator() *Translator {
	return &Translator{
		factorioTranslator: createFactorioTranslator(),
	}
}

var (
	regexpErase = regexp.MustCompile(`\[([a-z-]+?=.+?|[./][a-z-]+?)]`)
	regexpFix   = regexp.MustCompile(` +([ .:,;])`)
)

func createFactorioTranslator() factorioTranslator {
	return translator.New(
		translator.WithDefaultLoaders(),
		translator.WithCommonProcessors(),
		translator.WithProcessor(processor.NewControlPlaceholderProcessor(processControlPlaceholder)),
		translator.WithProcessor(eraseRichText),
	)
}

func processControlPlaceholder(translator processor.Translator, locale string, controlName string, _ int) (string, bool) {
	control := translator.TranslateWithFallback(locale, []any{fmt.Sprintf("controls.%s", controlName)})
	return fmt.Sprintf("[%s]", control), true
}

func eraseRichText(_ processor.Translator, _ string, value string, _ []any) string {
	value = regexpErase.ReplaceAllString(value, "")
	value = regexpFix.ReplaceAllString(value, "$1")
	return value
}

// LoadMod will add provided mod to the translator, using the context to resolve the path to it.
func (t *Translator) LoadMod(ctx context.Context, modName string) error {
	fi := instance.FromContext(ctx)
	modPath, err := fi.ModPath(modName)
	if err != nil {
		return err
	}

	return t.factorioTranslator.LoadMod(modPath)
}

// Translate will translate the provided localised string into all known languages.
func (t *Translator) Translate(ctx context.Context, localisedString any) exportdata.Translations {
	return t.TranslateWithFallback(ctx, localisedString, exportdata.Translations{})
}

// TranslateWithFallback will translate the provided localised string into all known languages, applying the provided
// fallback where applicable.
func (t *Translator) TranslateWithFallback(
	_ context.Context,
	localisedString any,
	fallbacks exportdata.Translations,
) exportdata.Translations {
	if localisedString == nil {
		return nil
	}

	locales := t.factorioTranslator.Locales()
	result := make(exportdata.Translations, len(locales))
	for _, locale := range locales {
		translatedString := t.factorioTranslator.Translate(locale, localisedString)
		if translatedString != "" {
			result[locale] = translatedString
		} else if fallbacks[locale] != "" {
			result[locale] = fallbacks[locale]
		}
	}

	if len(result) == 0 {
		return nil
	}

	return result
}
