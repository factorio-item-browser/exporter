// Code generated by mockery. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"

// FactorioTranslator is an autogenerated mock type for the factorioTranslator type
type FactorioTranslator struct {
	mock.Mock
}

// LoadMod provides a mock function with given fields: source
func (_m *FactorioTranslator) LoadMod(source string) error {
	ret := _m.Called(source)

	if len(ret) == 0 {
		panic("no return value specified for LoadMod")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(source)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Locales provides a mock function with given fields:
func (_m *FactorioTranslator) Locales() []string {
	ret := _m.Called()

	if len(ret) == 0 {
		panic("no return value specified for Locales")
	}

	var r0 []string
	if rf, ok := ret.Get(0).(func() []string); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	return r0
}

// Translate provides a mock function with given fields: locale, localisedString
func (_m *FactorioTranslator) Translate(locale string, localisedString any) string {
	ret := _m.Called(locale, localisedString)

	if len(ret) == 0 {
		panic("no return value specified for Translate")
	}

	var r0 string
	if rf, ok := ret.Get(0).(func(string, any) string); ok {
		r0 = rf(locale, localisedString)
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// NewFactorioTranslator creates a new instance of FactorioTranslator. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewFactorioTranslator(t interface {
	mock.TestingT
	Cleanup(func())
}) *FactorioTranslator {
	mock := &FactorioTranslator{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
