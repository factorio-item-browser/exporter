package icon

import (
	"context"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"slices"

	"github.com/google/uuid"
)

var simpleIconTypes = []string{
	"fluid",
	"item",
	"mod",
	"planet",
	"recipe",
	"resource",
	"space-location",
	"surface",
	"technology",
}

type Manager struct {
	icons      map[string]Icon
	hashes     map[string]string
	usedHashes map[string]struct{}
}

// NewManager will create a new instance of Manager.
func NewManager() *Manager {
	return &Manager{
		icons:      make(map[string]Icon),
		hashes:     make(map[string]string),
		usedHashes: make(map[string]struct{}),
	}
}

// Add will add the provided icon to the manager.
func (m *Manager) Add(_ context.Context, icon Icon) {
	hash := calculateIconHash(icon)
	icon.Hash = hash
	m.icons[hash] = icon

	if slices.Contains(simpleIconTypes, icon.Type) {
		m.hashes[buildKey(icon.Type, icon.Name)] = hash
		return
	}
	
	keys := []string{
		buildKey("item", icon.Name),
		buildKey("machine", icon.Name),
	}
	for _, key := range keys {
		if _, ok := m.hashes[key]; !ok {
			m.hashes[key] = hash
		}
	}
}

// buildKey builds the key used for the entity in the maps.
func buildKey(entityType string, entityName string) string {
	return fmt.Sprintf("%s|%s", entityType, entityName)
}

// calculateIconHash calculates a hash representing the provided icon to detect duplicates.
func calculateIconHash(icon Icon) string {
	dataString, _ := json.Marshal(icon.FactorioIcon)
	hash := md5.Sum(dataString)
	id, _ := uuid.FromBytes(hash[:])
	return id.String()
}

// HashForEntity returns the hash for the entity of the provided type and name. If the entity is not known, an empty
// string is returned.
func (m *Manager) HashForEntity(_ context.Context, entityType string, entityName string) string {
	result, ok := m.hashes[buildKey(entityType, entityName)]
	if !ok {
		return ""
	}

	m.usedHashes[result] = struct{}{}
	return result
}

// UsedIcons will return a channel where all icons which have actually been used will be pushed.
// This method is non-blocking, and the channel will be closed once done adding all the used icons.
func (m *Manager) UsedIcons(_ context.Context) <-chan Icon {
	result := make(chan Icon)

	go func() {
		defer close(result)

		for hash := range m.usedHashes {
			result <- m.icons[hash]
		}
	}()

	return result
}
