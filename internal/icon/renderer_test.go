package icon

import (
	"context"
	"fmt"
	"image"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/icon/mocks"
	instance2 "gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
)

func TestNewRenderer(t *testing.T) {
	workerCount := 42

	instance := NewRenderer(config.IconRenderer{WorkerCount: workerCount})

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.iconRenderer)
	assert.Equal(t, workerCount, instance.workerCount)
}

func TestRenderer_RenderIcons(t *testing.T) {
	defer func(ref1 func(*Renderer, context.Context, Icon, ImageWriter)) {
		renderIcon = ref1
	}(renderIcon)

	icon1 := Icon{
		Type: "item",
		Name: "electronic-circuit",
	}
	icon2 := Icon{
		Type: "fluid",
		Name: "crude-oil",
	}
	icon3 := Icon{
		Type: "mod",
		Name: "base",
	}
	calledIcons := map[string]Icon{
		icon1.Type: icon1,
		icon2.Type: icon2,
		icon3.Type: icon3,
	}

	icons := make(chan Icon)
	go func() {
		icons <- icon1
		icons <- icon2
		icons <- icon3
		close(icons)
	}()

	ctx := context.Background()
	ctx = instance2.NewMockedInstanceBuilder().BuildContext(ctx)

	iconRenderer := mocks.NewIconRenderer(t)
	iconRenderer.On("AddModsDirectory", "/root/instances/test/factorio/data").Once()
	iconRenderer.On("AddModsDirectory", "/root/instances/test/factorio/mods").Once()

	writer := mocks.NewImageWriter(t)

	instance := Renderer{
		iconRenderer: iconRenderer,
		workerCount:  2,
	}

	var lock sync.Mutex
	renderIcon = func(i *Renderer, c context.Context, ic Icon, w ImageWriter) {
		assert.Same(t, &instance, i)
		assert.Equal(t, ctx, c)
		assert.Same(t, writer, w)

		lock.Lock()
		defer lock.Unlock()

		icon, ok := calledIcons[ic.Type]
		if !ok {
			assert.Fail(t, fmt.Sprintf("unexpected call of renderIcon with icon %+v", icon))
		} else {
			assert.Equal(t, icon, ic)
			delete(calledIcons, ic.Type)
		}
	}

	instance.RenderIcons(ctx, icons, writer)

	assert.Len(t, calledIcons, 0)
}

func TestRenderer_renderIcon(t *testing.T) {
	img := image.RGBA{
		Pix: []uint8{
			0xFF, 0x00, 0x00, 0xFF, 0x80, 0x00, 0x00, 0xFF,
			0xFF, 0x00, 0x00, 0x80, 0x80, 0x00, 0xFF, 0x80,
		},
		Stride: 8,
		Rect:   image.Rect(0, 0, 2, 2),
	}

	tests := map[string]struct {
		icon              Icon
		expectedSize      int
		renderResult      image.Image
		renderError       error
		expectedWriteHash string
		writeError        error
	}{
		"happy path": {
			icon: Icon{
				Type: "item",
				Name: "electronic-circuit",
				Hash: "fancy-hash",
			},
			expectedSize:      64,
			renderResult:      &img,
			renderError:       nil,
			expectedWriteHash: "fancy-hash",
			writeError:        nil,
		},
		"mod": {
			icon: Icon{
				Type: "mod",
				Name: "base",
				Hash: "fancy-hash",
			},
			expectedSize:      144,
			renderResult:      &img,
			renderError:       nil,
			expectedWriteHash: "fancy-hash",
			writeError:        nil,
		},
		//"technology": {
		//	icon: Icon{
		//		Type: "technology",
		//		Name: "automation",
		//		Hash: "fancy-hash",
		//	},
		//	expectedSize:      256,
		//	renderResult:      &img,
		//	renderError:       nil,
		//	expectedWriteHash: "fancy-hash",
		//	writeError:        nil,
		//},
		"render error": {
			icon: Icon{
				Type: "item",
				Name: "electronic-circuit",
				Hash: "fancy-hash",
			},
			expectedSize:      64,
			renderResult:      nil,
			renderError:       fmt.Errorf("test error"),
			expectedWriteHash: "",
			writeError:        nil,
		},
		"write error": {
			icon: Icon{
				Type: "item",
				Name: "electronic-circuit",
				Hash: "fancy-hash",
			},
			expectedSize:      64,
			renderResult:      &img,
			renderError:       nil,
			expectedWriteHash: "fancy-hash",
			writeError:        fmt.Errorf("test error"),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			iconRenderer := mocks.NewIconRenderer(t)
			iconRenderer.On("Render", mock.MatchedBy(func(icon transfer.RenderIcon) bool {
				return assert.Equal(t, test.expectedSize, icon.OutputSize)
			})).Once().Return(test.renderResult, test.renderError)

			writer := mocks.NewImageWriter(t)
			if test.expectedWriteHash != "" {
				writer.On("WriteImage", test.expectedWriteHash, test.renderResult).Once().Return(test.writeError)
			}

			instance := Renderer{
				iconRenderer: iconRenderer,
			}
			instance.renderIcon(ctx, test.icon, writer)
		})
	}
}
