package icon

import "gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"

type Icon struct {
	transfer.FactorioIcon

	Type string `json:"type"`
	Name string `json:"name"`
	Hash string `json:"-"`
}
