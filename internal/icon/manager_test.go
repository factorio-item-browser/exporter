package icon

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	renderer "gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
)

func TestNewManager(t *testing.T) {
	instance := NewManager()

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.icons)
	assert.NotNil(t, instance.hashes)
	assert.NotNil(t, instance.usedHashes)
}

func TestManager_Add(t *testing.T) {
	tests := map[string]struct {
		icons          map[string]Icon
		hashes         map[string]string
		icon           Icon
		expectedIcons  map[string]Icon
		expectedHashes map[string]string
	}{
		"known type": {
			icons: map[string]Icon{
				"foo": {},
			},
			hashes: map[string]string{
				"foo": "bar",
			},
			icon: Icon{
				Type: "mod",
				Name: "test",
				FactorioIcon: renderer.FactorioIcon{
					Icon: "abc",
				},
			},
			expectedIcons: map[string]Icon{
				"foo": {},
				"78664bcc-1362-99af-70fb-07943d8f9194": {
					Type: "mod",
					Name: "test",
					FactorioIcon: renderer.FactorioIcon{
						Icon: "abc",
					},
					Hash: "78664bcc-1362-99af-70fb-07943d8f9194",
				},
			},
			expectedHashes: map[string]string{
				"foo":      "bar",
				"mod|test": "78664bcc-1362-99af-70fb-07943d8f9194",
			},
		},
		"known type existing": {
			icons: map[string]Icon{
				"foo": {},
				"78664bcc-1362-99af-70fb-07943d8f9194": {
					Name: "old",
				},
			},
			hashes: map[string]string{
				"foo":      "bar",
				"mod|test": "old",
			},
			icon: Icon{
				Type: "mod",
				Name: "test",
				FactorioIcon: renderer.FactorioIcon{
					Icon: "abc",
				},
			},
			expectedIcons: map[string]Icon{
				"foo": {},
				"78664bcc-1362-99af-70fb-07943d8f9194": {
					Type: "mod",
					Name: "test",
					FactorioIcon: renderer.FactorioIcon{
						Icon: "abc",
					},
					Hash: "78664bcc-1362-99af-70fb-07943d8f9194",
				},
			},
			expectedHashes: map[string]string{
				"foo":      "bar",
				"mod|test": "78664bcc-1362-99af-70fb-07943d8f9194",
			},
		},
		"unknown entity": {
			icons: map[string]Icon{
				"foo": {},
			},
			hashes: map[string]string{
				"foo": "bar",
			},
			icon: Icon{
				Type: "fancy",
				Name: "test",
				FactorioIcon: renderer.FactorioIcon{
					Icon: "def",
				},
			},
			expectedIcons: map[string]Icon{
				"foo": {},
				"076e06ad-e3c9-a210-0c28-419326eb6155": {
					Type: "fancy",
					Name: "test",
					FactorioIcon: renderer.FactorioIcon{
						Icon: "def",
					},
					Hash: "076e06ad-e3c9-a210-0c28-419326eb6155",
				},
			},
			expectedHashes: map[string]string{
				"foo":          "bar",
				"item|test":    "076e06ad-e3c9-a210-0c28-419326eb6155",
				"machine|test": "076e06ad-e3c9-a210-0c28-419326eb6155",
			},
		},
		"unknown entity existing": {
			icons: map[string]Icon{
				"foo": {},
				"076e06ad-e3c9-a210-0c28-419326eb6155": {
					Type: "fancy",
					Name: "test",
					FactorioIcon: renderer.FactorioIcon{
						Icon: "def",
					},
					Hash: "076e06ad-e3c9-a210-0c28-419326eb6155",
				},
			},
			hashes: map[string]string{
				"foo":       "bar",
				"item|test": "076e06ad-e3c9-a210-0c28-419326eb6155",
			},
			icon: Icon{
				Type: "fancy",
				Name: "test",
				FactorioIcon: renderer.FactorioIcon{
					Icon: "ghi",
				},
			},
			expectedIcons: map[string]Icon{
				"foo": {},
				"076e06ad-e3c9-a210-0c28-419326eb6155": {
					Type: "fancy",
					Name: "test",
					FactorioIcon: renderer.FactorioIcon{
						Icon: "def",
					},
					Hash: "076e06ad-e3c9-a210-0c28-419326eb6155",
				},
				"eee468d8-4a5e-2c77-4be7-af1616d644f4": {
					Type: "fancy",
					Name: "test",
					FactorioIcon: renderer.FactorioIcon{
						Icon: "ghi",
					},
					Hash: "eee468d8-4a5e-2c77-4be7-af1616d644f4",
				},
			},
			expectedHashes: map[string]string{
				"foo":          "bar",
				"item|test":    "076e06ad-e3c9-a210-0c28-419326eb6155",
				"machine|test": "eee468d8-4a5e-2c77-4be7-af1616d644f4",
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := Manager{
				icons:  test.icons,
				hashes: test.hashes,
			}
			instance.Add(context.Background(), test.icon)

			assert.Equal(t, test.expectedIcons, instance.icons)
			assert.Equal(t, test.expectedHashes, instance.hashes)
		})
	}
}

func TestManager_HashForEntity(t *testing.T) {
	tests := map[string]struct {
		hashes             map[string]string
		usedHashes         map[string]struct{}
		entityType         string
		entityName         string
		expectedResult     string
		expectedUsedHashes map[string]struct{}
	}{
		"known hash": {
			hashes: map[string]string{
				"abc|def": "foo",
				"ghi|jkl": "bar",
			},
			usedHashes: map[string]struct{}{
				"foo": {},
			},
			entityType:     "ghi",
			entityName:     "jkl",
			expectedResult: "bar",
			expectedUsedHashes: map[string]struct{}{
				"foo": {},
				"bar": {},
			},
		},
		"known used hash": {
			hashes: map[string]string{
				"abc|def": "foo",
				"ghi|jkl": "bar",
			},
			usedHashes: map[string]struct{}{
				"foo": {},
			},
			entityType:     "abc",
			entityName:     "def",
			expectedResult: "foo",
			expectedUsedHashes: map[string]struct{}{
				"foo": {},
			},
		},
		"unknown hash": {
			hashes: map[string]string{
				"abc|def": "foo",
				"ghi|jkl": "bar",
			},
			usedHashes: map[string]struct{}{
				"foo": {},
			},
			entityType:     "not",
			entityName:     "known",
			expectedResult: "",
			expectedUsedHashes: map[string]struct{}{
				"foo": {},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := Manager{
				hashes:     test.hashes,
				usedHashes: test.usedHashes,
			}
			result := instance.HashForEntity(context.Background(), test.entityType, test.entityName)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedUsedHashes, instance.usedHashes)
		})
	}
}

func TestManager_UsedIcons(t *testing.T) {
	icon1 := Icon{
		Name: "abc",
	}
	icon2 := Icon{
		Name: "def",
	}
	icon3 := Icon{
		Name: "ghi",
	}

	icons := map[string]Icon{
		"abc": icon1,
		"def": icon2,
		"ghi": icon3,
	}
	usedHashes := map[string]struct{}{
		"abc": {},
		"ghi": {},
	}
	expectedResult := []Icon{
		icon1,
		icon3,
	}

	instance := Manager{
		icons:      icons,
		usedHashes: usedHashes,
	}
	result := instance.UsedIcons(context.Background())

	resultIcons := make([]Icon, 0)
	for r := range result {
		resultIcons = append(resultIcons, r)
	}

	assert.ElementsMatch(t, expectedResult, resultIcons)
}
