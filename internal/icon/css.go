package icon

import (
	"bytes"
	"encoding/base64"
	"image"
	"image/png"
	"io"
	"sync"
)

// CssWriter will write images and their hashes to generate a CSS file.
type CssWriter struct {
	writer io.Writer
	lock   sync.Mutex
}

// NewCssWriter creates a new instance of CssWriter.
func NewCssWriter(writer io.Writer) *CssWriter {
	return &CssWriter{
		writer: writer,
	}
}

// WriteImage will encode the provided image, and write it out together with the provided hash.
// This method is thread-safe.
func (w *CssWriter) WriteImage(hash string, img image.Image) error {
	buffer := bytes.NewBuffer(nil)
	buffer.WriteString(".icon-")
	buffer.WriteString(hash)
	buffer.WriteString("{background-image:url(data:image/png;base64,")

	encoder := base64.NewEncoder(base64.StdEncoding, buffer)
	err := png.Encode(encoder, img)
	if err != nil {
		return err
	}

	buffer.WriteString(")}\n")

	w.lock.Lock()
	defer w.lock.Unlock()
	_, _ = buffer.WriteTo(w.writer)

	return nil
}
