package icon

import (
	"context"
	"image"
	"log/slog"
	"sync"

	"gitlab.com/factorio-item-browser/exporter.git/internal/config"
	"gitlab.com/factorio-item-browser/exporter.git/internal/instance"
	"gitlab.com/factorio-item-browser/exporter.git/internal/log"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/renderer"
	"gitlab.com/factorio-item-browser/factorio-icon-renderer.git/pkg/transfer"
)

const defaultIconSize = 64
const defaultScaleBase = transfer.ScaleBaseDefault

var iconSizesPerType = map[string]int{
	"mod": 144,
}
var scaleBasePerType = map[string]transfer.ScaleBase{
	"item":   transfer.ScaleBaseItemOrRecipe,
	"recipe": transfer.ScaleBaseItemOrRecipe,
}

var renderIcon = (*Renderer).renderIcon

type iconRenderer interface {
	AddModsDirectory(modsDirectory string)
	Render(icon transfer.RenderIcon) (image.Image, error)
}

type ImageWriter interface {
	WriteImage(hash string, img image.Image) error
}

type Renderer struct {
	iconRenderer iconRenderer
	workerCount  int
}

func NewRenderer(cfg config.IconRenderer) *Renderer {
	return &Renderer{
		iconRenderer: renderer.New(),
		workerCount:  cfg.WorkerCount,
	}
}

// RenderIcons will render all icons provided in the icons channel, and write them to the provided image writer.
func (r *Renderer) RenderIcons(ctx context.Context, icons <-chan Icon, writer ImageWriter) {
	fi := instance.FromContext(ctx)

	r.iconRenderer.AddModsDirectory(fi.Path(instance.FactorioDataDirectory))
	r.iconRenderer.AddModsDirectory(fi.Path(instance.ModsDirectory))

	var wg sync.WaitGroup
	wg.Add(r.workerCount)
	for i := 0; i < r.workerCount; i++ {
		go func() {
			defer wg.Done()

			for icon := range icons {
				renderIcon(r, ctx, icon, writer)
			}
		}()
	}
	wg.Wait()
}

// renderIcon will render the provided icon and write it to the provided image writer. If an error occurs, it will get
// written to the log, and no icon will be written.
func (r *Renderer) renderIcon(ctx context.Context, icon Icon, writer ImageWriter) {
	ctx = log.NewContextWithValues(ctx, slog.String("hash", icon.Hash))

	iconSize := defaultIconSize
	if size, ok := iconSizesPerType[icon.Type]; ok {
		iconSize = size
	}

	scaleBase := defaultScaleBase
	if sb, ok := scaleBasePerType[icon.Type]; ok {
		scaleBase = sb
	}

	img, err := r.iconRenderer.Render(transfer.NewRenderIcon(icon.FactorioIcon, scaleBase, iconSize))
	if err != nil {
		log.FromContext(ctx).Error("render icon error", slog.String("error", err.Error()))
		return
	}

	err = writer.WriteImage(icon.Hash, img)
	if err != nil {
		log.FromContext(ctx).Error("write icon error", slog.String("error", err.Error()))
	}
}
