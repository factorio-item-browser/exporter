package icon

import (
	"bytes"
	"image"
	"image/png"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCssWriter(t *testing.T) {
	writer := bytes.NewBuffer(nil)

	instance := NewCssWriter(writer)

	assert.NotNil(t, instance)
	assert.Same(t, writer, instance.writer)
}

func TestCssWriter_WriteImage(t *testing.T) {
	tests := map[string]struct {
		hash          string
		img           image.Image
		expectedWrite string
		expectedError error
	}{
		"happy path": {
			hash: "fancy-hash",
			img: &image.RGBA{
				Pix: []uint8{
					0xFF, 0x00, 0x00, 0xFF, 0x80, 0x00, 0x00, 0xFF,
					0xFF, 0x00, 0x00, 0x80, 0x80, 0x00, 0xFF, 0x80,
				},
				Stride: 8,
				Rect:   image.Rect(0, 0, 2, 2),
			},
			expectedWrite: ".icon-fancy-hash{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAH0lEQVR4nGL5z8Dwv5GBgYHxLwNDAxPDXwZAAAAA//8yOwUB9VCx7AAAAABJRU5ErkJg)}\n",
			expectedError: nil,
		},
		"with error": {
			hash: "fancy-hash",
			img: &image.RGBA{
				Pix:    []uint8{},
				Stride: 8,
				Rect:   image.Rect(0, 0, 0, 0),
			},
			expectedWrite: "",
			expectedError: png.FormatError("test error"),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			buffer := bytes.NewBuffer(nil)

			instance := CssWriter{
				writer: buffer,
			}
			err := instance.WriteImage(test.hash, test.img)

			assert.IsType(t, test.expectedError, err)
			assert.Equal(t, test.expectedWrite, buffer.String())
		})
	}
}
